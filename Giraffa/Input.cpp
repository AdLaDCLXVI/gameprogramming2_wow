#include "Input.h"

Input::Input()
{
	for (int i = 0; i < sf::Keyboard::KeyCount; i++)
	{
		keyHold_[i] = false;
		keyPressed_[i] = false;
	}

	for (int i = 0; i < sf::Mouse::Button::ButtonCount; i++)
	{
		mouseHold_[i] = false;
		mousePressed_[i] = false;
	}
}

void Input::update(sf::RenderWindow* window)
{
	for (int i = 0; i < sf::Keyboard::KeyCount; i++)
		keyPressed_[i] = false;
	for (int i = 0; i < sf::Mouse::Button::ButtonCount; i++)
		mousePressed_[i] = false;

	while (window->pollEvent(events_))
	{
		switch (events_.type)
		{
		case sf::Event::MouseMoved:
			mousePosition_ = sf::Mouse::getPosition(*window);
			break;

		case sf::Event::MouseButtonPressed:
			mouseHold_[events_.mouseButton.button] = true;
			mousePressed_[events_.mouseButton.button] = true;
			break;

		case sf::Event::MouseButtonReleased:
			mouseHold_[events_.mouseButton.button] = false;
			break;

		case sf::Event::KeyPressed:
			keyPressed_[0] = true;
			keyHold_[events_.key.code] = true;
			keyPressed_[events_.key.code] = true;
			break;

		case sf::Event::TextEntered:
			currentChar_ = static_cast<char>(events_.text.unicode);
			break;

		case sf::Event::KeyReleased:
			keyHold_[events_.key.code] = false;
			//mKeyReleased[mEvents.key.code] = true;
			break;

		default:
			break;
		}
	}
}

bool Input::getKey(sf::Keyboard::Key key)
{
	return keyHold_[key];
}

bool Input::getKeyPressed(sf::Keyboard::Key key)
{
	return keyPressed_[key];
}

bool Input::getAnyKeyPressed()
{
	return keyPressed_[0];
}
char Input::getCurrentChar()
{
	return currentChar_;
}
int Input::getMouseX() { return mousePosition_.x; }

int Input::getMouseY() { return mousePosition_.y; }

const sf::Vector2i& Input::getMousePos() const
{
	return mousePosition_;
}

bool Input::getMouse(sf::Mouse::Button button)
{
	return mouseHold_[button];
}

bool Input::getMousePressed(sf::Mouse::Button button)
{
	return mousePressed_[button];
}