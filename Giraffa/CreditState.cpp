#include "Global.h"
#include "Render.h"
#include "Input.h"
#include "AudioManager.h"
#include "MenuState.h"
#include "CreditState.h"
#include "OptionState.h"
#include "CreditState.h"

CreditState::CreditState(Render* render, Input* input, AudioManager* audio)
{
	render_ = render;
	input_ = input;
	audio_ = audio;

	returnState = 0;

	imageTexture_ = render_->addTexture("assets/textures/spr_buttons.png");
	Image_.setTexture(*imageTexture_);
	Image_.setTextureRect({});
	Image_.setScale(0.5f, 0.5f);
	Image_.setPosition(450.0f, 100.0f);
}

void CreditState::enter()
{

	creditTexture_ = render_->addTexture("assets/textures/spr_soundCredits.png");
	credImage_.setTexture(*creditTexture_);
	credImage_.setScale(1.0f, 1.0f);
	credImage_.setPosition(0.0f, 0.0f);

	BackToMenuButton_ = Button({ 650, 650 }, render_->getButtonTexture(), { 611.0f, 641.0f, 390.0f, 61.0f });
}

bool CreditState::update(float deltaTime)
{
	anykey_.update(deltaTime);


	BackToMenuButton_.update((sf::Vector2f)input_->getMousePos());
	if (input_->getMousePressed(sf::Mouse::Left))
	{

		BackToMenuButton_.setPressed((sf::Vector2f)input_->getMousePos());
	}
	if (BackToMenuButton_.reset())
	{
		audio_->PlayMenuSound();
		return false;
	}
	return true;
}

void CreditState::draw()
{
	render_->renderToWindow()->draw(*BackToMenuButton_.getSprite());
	render_->renderToWindow()->draw(credImage_);
}

States CreditState::getState()
{
	return STATE_CREDITSTATE;
}

State* CreditState::getNextState()
{
	return new MenuState(render_, input_, audio_);
}

void CreditState::exit()
{
}