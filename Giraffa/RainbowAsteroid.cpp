#include "Global.h"
#include "NewMath.h"
#include "RainbowAsteriod.h"

RainbowAsteroid::RainbowAsteroid()
{
	alive_ = 0x2;
	angle_ = 0.2f;
}

void RainbowAsteroid::init(const sf::Vector2f& pos, const sf::Vector2f& vel)
{
	posistion_ = pos;
	velocity_ = vel;
	alive_ = 0x0;
	angle_ = NewMath::randNormal() * 360.0f;
	active_ = false;
}

void RainbowAsteroid::update(float deltaTime_)
{
	posistion_ += velocity_ * deltaTime_;
	
	{
		if (posistion_.x >= -50.0f && posistion_.x <= 50.0f + WIDTH && posistion_.y >= -50.0f && posistion_.y <= 50.0f + HEIGHT)
		{
			if (alive_ == 0x0)
				alive_ = 0x1;
		}
		else
		{
			if (alive_ == 0x1)
				alive_ = 0x2;
			else
			{
				if (posistion_.x < -50.0f)
					posistion_.x += WIDTH + 100.0f;
				if (posistion_.x > WIDTH + 50.0f)
					posistion_.x -= WIDTH + 100.0f;
				if (posistion_.y < -50.0f)
					posistion_.y += HEIGHT + 100.0f;
				if (posistion_.y > HEIGHT + 50.0f)
					posistion_.y -= HEIGHT + 100.0f;
			}
		}
	}
}

const sf::Vector2f& RainbowAsteroid::getPosition() const
{
	return posistion_;
}

float RainbowAsteroid::getAngle()
{
	return angle_;
}

bool RainbowAsteroid::isAlive()
{
	return alive_ != 0x2;
}

void RainbowAsteroid::kill()
{
	if (!active_)
	{
	active_ = true;
	}
	else
	{
		alive_ = 0x2;
	}
}

bool RainbowAsteroid::activated()
{
	return active_;
}