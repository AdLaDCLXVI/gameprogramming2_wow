#ifndef BACKGROUND_H
#define BACKGROUND_H
#include <SFML\Graphics.hpp>

class Background
{
public:
	Background();
	~Background() {};
	void update(float deltaTime, float speed);
	const sf::Sprite& getSprite() const;
	const sf::Sprite& getSprite2() const;
	const sf::RenderStates& getState() const;
private:
	sf::RenderStates state_;
	sf::Texture backgroundTexture_;
	sf::Texture starsTexture_;
	sf::Sprite backgroundSprite_;
	sf::Sprite starsSprite_;
	sf::Shader backgroundShader_;

	float backgroundScroll_;
	float starScroll_;
};
#endif