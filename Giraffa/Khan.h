#pragma once
#include <SFML\Graphics.hpp>
enum BossState {
	notReady,
	Start,
	Passive,
	Nr1,
	Nr2,
	Nr3,
	Pre3,
	Dead
};
class EntityManager;
class AudioManager;
class Shape;
class Animation;
class Render;
class Khan
{
public:
	Khan();
	~Khan();

	void init(const sf::Vector2f& pos, const sf::Vector2f& vel);
	void Update(float);
	BossState GetState();
	void Walking();
	void Idle();
	void NrOne();
	void NrTwo();
	void NrThree();
	void Death();
	void NewState();
	void HitBoss(float dmg);
	void CreateAnimation(Render*);
	Shape* GetShape();
	const sf::Vector2f& getPosition() const;
	void setTexture(sf::Texture* texture);
	sf::Sprite* getSprite();
	sf::Sprite* getTentacleSprite();
	sf::Sprite getEyesSprite();
	bool isAlive();
	void kill();
	bool shootRequest();
	float GetTargetOne() { return target1;};
	float GetTargetTwo() { return target2;};
	bool Ready();
private:
	sf::Vector2f position_;
	sf::Vector2f velocity_;

	bool alive_;
	int Time;
	float HP;
	float target1, target2;
	BossState State;
	Shape* collisionShape_;
	bool requestShoot;
	float EyeTime;
	float DeltaTime;
	sf::Sprite Eyes;
	Animation* IdleSprite;

	Animation* attackOne;
	Animation* attackTwo;
	Animation* attackThree;
	Animation* death;
	Animation* Tentacles;
	Animation* enemySprite_;
	
	sf::Texture* texture;
};