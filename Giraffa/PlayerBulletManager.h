#ifndef PLAYERBULLETMANAGER_H
#define PLAYERBULLETMANAGER_H
#include <SFML\Graphics.hpp>
#include "Render.h"
#include "PlayerBullet.h"
#include "EnemyManager.h"
#include "PowerupStates.h"
#include "Lazer.h"
#include "Circle.h"
#include "HitInfo.h"

class AsteroidManager;

class PlayerBulletManager
{
public:
	PlayerBulletManager(Render* render);
	~PlayerBulletManager();
	void update(float deltaTime, EnemyManager* manager1, AsteroidManager* manager2);
	void addBullet(const sf::Vector2f& pos, const sf::Vector2f& vel);
	void setPowerup(PowerupStates powerup);
	void draw();
	void clean();
private:
	bool findEmptySlot();
	void addBullet(const sf::Vector2f& pos, const sf::Vector2f& vel, bool electric);

	Render* render_;
	PlayerBullet playerBulletList_[20];
	Lazer powerupLazer_;
	sf::Texture* powerupLazerTexture_;
	sf::Sprite playerBulletSprite_;
	sf::Sprite powerupLazerSprite_;
	Circle collisionShape_;

	int lastDeleted_;
	PowerupStates powerupState_;
	float powerupTimer_;
	bool powerupLazerActive_;
};
#endif