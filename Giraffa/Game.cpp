#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include "Input.h"
#include "Render.h"
#include "StateManager.h"
#include "Game.h"

bool Game::begin()
{
	sf::ContextSettings context(24, 8, 0, 4, 1);

	
	window_.create(sf::VideoMode(WIDTH, HEIGHT), "Flight of The Giraffa", sf::Style::Titlebar, context);
	if (!window_.isOpen())
		return false;
	window_.setFramerateLimit(60);
	window_.setKeyRepeatEnabled(false);
	window_.setMouseCursorVisible(false);

	input_ = new Input();
	render_ = new Render(&window_, input_);
	stateManager_ = new StateManager(render_, input_);

	isRunning_ = true;
	return true;
}

void Game::run()
{
	

	
	sf::Time timer = clock_.restart();
	double elapsed;
	srand(timer.asSeconds());
	

	while (isRunning_)
	{
		timer = clock_.restart();
		elapsed = timer.asMilliseconds();

		//if (window_.hasFocus())
		{

			render_->update((float)(elapsed * MS_PER_UPDATE));
			isRunning_  = stateManager_->update((float)(elapsed * MS_PER_UPDATE));

			handleInput();
		}

		window_.clear();
		window_.setView(*render_->getView());

		render_->drawBackground();
		stateManager_->draw();
		render_->draw();
		stateManager_->drawFront();

		

		window_.display();
	}
}

void Game::end()
{
	delete render_;
	delete input_;
	window_.close();
}

void Game::handleInput()
{
	input_->update(&window_);

	if (input_->getKeyPressed(sf::Keyboard::F12))
		isRunning_ = false;
}