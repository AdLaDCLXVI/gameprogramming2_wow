#include "Global.h"
#include "NewMath.h"
#include "Input.h"
#include "Render.h"
#include "Player.h"
#include "PickupManager.h"
#include "AsteroidManager.h"
#include "Shield.h"
#include "GameState.h"

GameState::GameState(Render* render, Input* input, int level)
{
	render_ = render;
	input_ = input;
	currentLevel_ = level;
}

void GameState::enter()
{
	render_->scrollBackground(true);

	player_ = new Player(render_, input_);
	player_->setPosition(WIDTH / 3.0f, (HEIGHT + HEIGHT_HUD) / 2.0f);

	playerBulletManager_ = new PlayerBulletManager(render_);

	pickupManager_ = new PickupManager(render_);
	pickupManager_->addPickup((sf::Vector2f)input_->getMousePos());

	enemyManager_ = new EnemyManager(render_, pickupManager_);
	setLevel(currentLevel_);

	asteroidManager_ = new AsteroidManager(render_);

	shield_ = new Shield(render_);

	hudTexture_ = render_->addTexture("assets/textures/spr_gameHud.png");
	hudSprite_.setTexture(*hudTexture_);
}

bool GameState::update(float deltaTime)
{
	player_->update(deltaTime);

	pickupManager_->update(deltaTime);

	pickupManager_->collidePlayer(player_);

	if (input_->getMousePressed(sf::Mouse::Right))
	{
		PowerupStates state = pickupManager_->getCurrentPickup();
		if (state != POWERUP_SHIELD)
			playerBulletManager_->setPowerup(state);
		else
			shield_->activate();

		pickupManager_->empty();
	}

	if (input_->getKeyPressed(sf::Keyboard::Q))
		shield_->activate();

	if (player_->shootRequest())
	{
		sf::Vector2f pos = NewMath::rotateVec({ 34.0f, -55.0f }, player_->getAngle()) + player_->getPosition();
		sf::Vector2f angle = NewMath::angle(pos, (sf::Vector2f)input_->getMousePos());
		playerBulletManager_->addBullet(pos, angle);
	}

	playerBulletManager_->update(deltaTime, enemyManager_, asteroidManager_);
	enemyManager_->update(deltaTime, player_);
	asteroidManager_->update(deltaTime, player_);

	shield_->update(deltaTime, player_->getPosition(),
		enemyManager_,
		asteroidManager_);

	return true;
}

void GameState::draw()
{
	player_->draw();
	shield_->draw();
	enemyManager_->draw();
	asteroidManager_->draw();
	playerBulletManager_->draw();
	pickupManager_->draw();
}

void GameState::drawFront()
{
	render_->renderToWindow()->draw(hudSprite_);
}

States GameState::getState()
{
	return STATE_GAME;
}

State* GameState::getNextState()
{
	return nullptr;
}

void GameState::exit()
{
	render_->deleteTexture(hudTexture_);
	delete shield_;
	delete pickupManager_;
	delete asteroidManager_;
	delete enemyManager_;
	delete playerBulletManager_;
	delete player_;
}

void GameState::setLevel(int level)
{
	switch (level)
	{
	case 0 :
		enemyManager_->init("assets/lvl_1.lvl");
		return;
	case 1:
		enemyManager_->init("assets/lvl_2.lvl");
		return;
	case 2:
		enemyManager_->init("assets/lvl_3.lvl");
		return;
	default:
		return;
	}
}
