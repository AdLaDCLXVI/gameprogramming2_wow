#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include "Input.h"
#include "Render.h"
#include "StateManager.h"
#include "Game.h"

bool Game::begin()
{
	sf::ContextSettings context(24, 8, 0, 4, 1);

	window_.create(sf::VideoMode(WIDTH, HEIGHT), "Flight of The Giraffa", sf::Style::Titlebar, context);
	if (!window_.isOpen())
		return false;
	window_.setFramerateLimit(60);
	window_.setKeyRepeatEnabled(false);
	window_.setMouseCursorVisible(false);

	input_ = new Input();
	render_ = new Render(&window_, input_);
	stateManager_ = new StateManager(render_, input_);

	isRunning_ = true;
	return true;
}

void Game::run()
{
	sf::Font font;
	font.loadFromFile("assets/massive retaliation.ttf");
	sf::Text fpsCounter("", font, 40);
	fpsCounter.setColor(sf::Color(255, 255, 255));

	clock_.restart();
	sf::Time timer;
	double elapsed;

	while (isRunning_)
	{
		timer = clock_.restart();
		elapsed = timer.asMilliseconds();

		if (window_.hasFocus())
		{
			handleInput();

			render_->update((float)(elapsed * MS_PER_UPDATE));
			stateManager_->update((float)(elapsed * MS_PER_UPDATE));
		}

		window_.clear();
		window_.setView(*render_->getView());

		render_->drawBackground();
		stateManager_->draw();
		render_->draw();
		stateManager_->drawFront();

		std::ostringstream fpsString;
		fpsString << (int)(1000 / elapsed);
		fpsCounter.setString(fpsString.str());
		window_.draw(fpsCounter);

		window_.display();
	}
	window_.close();
}

void Game::end()
{
	delete stateManager_;
	delete render_;
	delete input_;
}

void Game::handleInput()
{
	input_->update(&window_);

	if (input_->getKeyPressed(sf::Keyboard::Escape))
		isRunning_ = false;
}