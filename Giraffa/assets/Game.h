#ifndef GAME_H
#define GAME_H
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include "Global.h"

class Input;
class Render;
class StateManager;

class Game
{
public:
	Game() {};
	bool begin();
	void run();
	void end();
private:
	void handleInput();

	Input* input_;
	sf::RenderWindow window_;
	Render* render_;
	sf::Clock clock_;
	StateManager* stateManager_;

	bool isRunning_;
};
#endif