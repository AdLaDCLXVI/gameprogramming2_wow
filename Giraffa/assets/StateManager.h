#ifndef STATEMANAGER_H
#define STATEMANAGER_H
#include <SFML\Graphics.hpp>

class Input;
class Render;
class State;

class StateManager
{
public:
	StateManager(Render* render, Input* input);
	~StateManager();
	bool update(float deltaTime);
	void draw();
	void drawFront();
private:
	void setState(State* state);

	Render* render_;
	Input* input_;
	State* currentState_;
};
#endif