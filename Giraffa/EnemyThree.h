#ifndef ENEMYTHREE_H
#define ENEMYTHREE_H
#include <SFML\Graphics.hpp>

class Animation;

class EnemyThree
{
public:
	EnemyThree();
	~EnemyThree();
	void init(const sf::Vector2f& pos, const sf::Vector2f& vel, sf::Texture* texture);
	void update(float deltaTime);
	const sf::Vector2f& getPosition() const;
	void setTexture(sf::Texture* texture);
	sf::Sprite* getSprite();
	bool isAlive();
	void kill();
	bool shootRequest();
private:
	sf::Vector2f position_;
	sf::Vector2f velocity_;
	Animation* enemySprite_;
	
	bool alive_;
	char states_;
	float timer_;
};
#endif