#include "Global.h"
#include "Render.h"
#include "Input.h"
#include "GameState.h"
#include "OptionState.h"
#include "Input.h"
#include "MenuState.h"
#include "Player.h"
#include "SliderCheck.h"

OptionState::OptionState(Render* render, Input* input, AudioManager* audio)
{
	render_ = render;
	input_ = input;
	audio_ = audio;
}

void OptionState::enter()
{
	volumeSlider_ = SliderCheck(450, 220, render_, 287.0f);
	sfxSlider_ = SliderCheck(450, 280, render_, 287.0f);
	menuTexture_ = render_->addTexture("assets/textures/spr_buttons.png");

	OptionsKeys_.setTexture(*menuTexture_);
	OptionsKeys_.setTextureRect({ 1114, 395, 243, 63 });
	OptionsKeys_.setPosition(510, 50);
	OptionsKeys_.setScale(1.3f, 1.3f);
	
	SoundSettingsKeys_.setTexture(*menuTexture_);
	SoundSettingsKeys_.setTextureRect({ 645, 395, 468, 68 });
	SoundSettingsKeys_.setPosition(550, 160);
	SoundSettingsKeys_.setScale(0.5f, 0.5f);

	MusicKeys_.setTexture(*menuTexture_);
	MusicKeys_.setTextureRect({ 1041, 592, 110, 46 });
	MusicKeys_.setPosition(350, 210);
	MusicKeys_.setScale(0.7f, 0.7f);

	/*MusicBarKeys_.setTexture(*menuTexture_);
	MusicBarKeys_.setTextureRect({ 742, 201, 575, 22 });
	MusicBarKeys_.setPosition(450, 220);
	MusicBarKeys_.setScale(0.8f, 0.8f);
	*/
	SFXKeys_.setTexture(*menuTexture_);
	SFXKeys_.setTextureRect({ 910, 765, 75, 46 });
	SFXKeys_.setPosition(360, 270);
	SFXKeys_.setScale(0.7f, 0.7f);

	/*SFXBarKeys_.setTexture(*menuTexture_);
	SFXBarKeys_.setTextureRect({ 742, 201, 575, 22 });
	SFXBarKeys_.setPosition(450, 280);
	SFXBarKeys_.setScale(0.8f, 0.8f);
	*/
	RightKeys_.setTexture(*menuTexture_);
	RightKeys_.setTextureRect({ 1213, 592, 158, 55 });
	RightKeys_.setPosition(920, 430);
	RightKeys_.setScale(0.65f, 0.65f);

	LeftKeys_.setTexture(*menuTexture_);
	LeftKeys_.setTextureRect({ 970, 853, 144, 55 });
	LeftKeys_.setPosition(250, 430);
	LeftKeys_.setScale(0.65f, 0.65f);

	ControlsKeys_.setTexture(*menuTexture_);
	ControlsKeys_.setTextureRect({ 1338, 93, 295, 68 });
	ControlsKeys_.setPosition(550, 390);
	ControlsKeys_.setScale(0.65f, 0.65f);

	FireRightKeys_.setTexture(*menuTexture_);
	FireRightKeys_.setTextureRect({ 1507, 218, 108, 55 });
	FireRightKeys_.setPosition(1170, 673);
	FireRightKeys_.setScale(0.5f, 0.5f);

	FireLeftKeys_.setTexture(*menuTexture_);
	FireLeftKeys_.setTextureRect({ 1507, 218, 108, 55 });
	FireLeftKeys_.setPosition(50, 673);
	FireLeftKeys_.setScale(0.5f, 0.5f);

	PowerUpRightKeys_.setTexture(*menuTexture_);
	PowerUpRightKeys_.setTextureRect({ 1338, 162, 238, 55 });
	PowerUpRightKeys_.setPosition(730, 673);
	PowerUpRightKeys_.setScale(0.5f, 0.5f);

	PowerUpLeftKeys_.setTexture(*menuTexture_);
	PowerUpLeftKeys_.setTextureRect({ 1338, 162, 238, 55 });
	PowerUpLeftKeys_.setPosition(400, 673);
	PowerUpLeftKeys_.setScale(0.5f, 0.5f);

	MovementRightKeys_.setTexture(*menuTexture_);
	MovementRightKeys_.setTextureRect({ 522, 950, 244, 55 });
	MovementRightKeys_.setPosition(950, 673);
	MovementRightKeys_.setScale(0.5f, 0.5f);

	MovementLeftKeys_.setTexture(*menuTexture_);
	MovementLeftKeys_.setTextureRect({ 522, 950, 244, 55 });
	MovementLeftKeys_.setPosition(200, 673);
	MovementLeftKeys_.setScale(0.5f, 0.5f);

	BackToMenuButton_ = Button({ 70, 30 }, menuTexture_, { 1189.0f, 334.0f, 119.0f, 56.0f });
	RightMenuButton_ = Button({ 970, 570 }, menuTexture_, { 742.0f, 0.0f, 595.0f, 200.0f });
	LeftMenuButton_ = Button({ 310, 570 }, menuTexture_, { 0.0f, 749.0f, 595.0f, 200.0f });
}

bool OptionState::update(float deletaTime)
{
	volumeSlider_.update(input_->getMouseX());
	sfxSlider_.update(input_->getMouseX());

	BackToMenuButton_.update((sf::Vector2f)input_->getMousePos());
	if (input_->getMousePressed(sf::Mouse::Left))
	{
		BackToMenuButton_.setPressed((sf::Vector2f)input_->getMousePos());
		volumeSlider_.pressed((sf::Vector2f)input_->getMousePos());
		sfxSlider_.pressed((sf::Vector2f)input_->getMousePos());
	}

	if (!input_->getMouse(sf::Mouse::Left))
	{
		volumeSlider_.reset();
		sfxSlider_.reset();
	}

	if (BackToMenuButton_.reset())
	{
		audio_->PlayMenuSound();
		return false;
	}

	audio_->setVolume(volumeSlider_.getValue());
	audio_->setSfxVolume(sfxSlider_.getValue());

	return true;
}

void OptionState::draw()
{

	render_->renderToWindow()->draw(OptionsKeys_);
	render_->renderToWindow()->draw(SoundSettingsKeys_);
	render_->renderToWindow()->draw(MusicKeys_);
	// render_->renderToWindow()->draw(MusicBarKeys_);
	volumeSlider_.draw();
	sfxSlider_.draw();
	render_->renderToWindow()->draw(SFXKeys_);
	// render_->renderToWindow()->draw(SFXBarKeys_);
	render_->renderToWindow()->draw(FireRightKeys_);
	render_->renderToWindow()->draw(FireLeftKeys_);
	render_->renderToWindow()->draw(PowerUpRightKeys_);
	render_->renderToWindow()->draw(PowerUpLeftKeys_);
	render_->renderToWindow()->draw(MovementRightKeys_);
	render_->renderToWindow()->draw(MovementLeftKeys_);
	render_->renderToWindow()->draw(ControlsKeys_);
	render_->renderToWindow()->draw(RightKeys_);
	render_->renderToWindow()->draw(LeftKeys_);



	render_->renderToWindow()->draw(*BackToMenuButton_.getSprite());
	render_->renderToWindow()->draw(*RightMenuButton_.getSprite());
	render_->renderToWindow()->draw(*LeftMenuButton_.getSprite());

}

States OptionState::getState()
{
	return STATE_OPTIONSTATE;
}

State* OptionState::getNextState()
{
	return new MenuState(render_, input_, audio_);
}

void OptionState::exit()
{
}