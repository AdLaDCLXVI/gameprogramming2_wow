#pragma once
#include "SFML\Graphics.hpp"
#include "Animation.h"
class DeadAsteroid
{
public:
	DeadAsteroid();
	~DeadAsteroid();
	void init(sf::Texture* texture, sf::Vector2f pos);
	void Update(float deltaTime);
	bool isAlive();
	void kill();
	sf::Sprite* GetSprite();
private:
	sf::Vector2f position_;
	Animation* enemySprite_;

	char alive_;
};