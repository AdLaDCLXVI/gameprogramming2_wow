#ifndef ASTEROIDMANAGER_H
#define ASTEROIDMANAGER_H
#include <SFML\Graphics.hpp>
#include "PlayerBulletManager.h"
#include "Circle.h"
#include "Asteroid.h"
#include "RainbowAsteriod.h"
#include "DeadAsteroid.h"
#include "HitInfo.h"

class Render;
class AudioManager;
class Player;

class AsteroidManager
{
public:
	AsteroidManager(Render* render, AudioManager* pAudioManager);
	~AsteroidManager() {};
	void update(float deltaTime, Player* player);
	void draw();
	bool collideShape(Shape* shape, HitInfo& hitInfo);
	bool collideCircle(Circle* circle, HitInfo& hitInfo);
	bool getClean();
	void stopClean();
	void stop();
	bool getStop();
private:
	int addAsteroid(const sf::Vector2f& pos, const sf::Vector2f& vel);
	bool findEmptySlot();
	bool findEmptyDeadSlot();

	Render* render_;
	AudioManager* audio_;
	Asteroid asteroidList_[6];
	DeadAsteroid deadAsteroidList_[8];
	sf::Sprite asteroidSprite_;
	sf::Texture* texture;
	Animation rainbowAsteroidSprite_;
	Animation blowRainbow_;
	Circle collisionShape_;
	RainbowAsteroid rainbowAsteroid_;

	sf::Sound soundPlayer_;
	soundClip asteroid;
	soundClip rainbow;

	int lastDeleted_;
	int lastDeadDeleted_;
	bool clean_;
	float scale_;
	float rainbowHue_;
	bool stop_;
};
#endif