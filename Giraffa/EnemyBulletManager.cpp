#include "CollisionManager.h"
#include "EnemyBulletManager.h"

EnemyBulletManager::EnemyBulletManager(Render* render)
{
	render_ = render;
	enemyBulletTexture_ = render_->addTexture("assets/textures/spr_enemyBullet.png");
	enemyBulletSprite_.setTexture(*enemyBulletTexture_);
	enemyBulletSprite_.setOrigin({ 27.0f, 23.0f });

	collisionShape_.setRect(24, 10, 0.0f, 0.0f);

	lastDeleted_ = 0;
}

EnemyBulletManager::~EnemyBulletManager()
{
	render_->deleteTexture(enemyBulletTexture_);
}

void EnemyBulletManager::update(float deltaTime)
{
	for (int i = 0; i < 40; i++)
	{
		auto& o = enemyBulletList_[i];
		if (o.isAlive())
			o.update(deltaTime);
	}
}

void EnemyBulletManager::addBullet(const sf::Vector2f& pos, float angle, float speed)
{
	if (findEmptySlot())
		enemyBulletList_[lastDeleted_].init(pos, angle, speed);
}

void EnemyBulletManager::draw()
{
	for (int i = 0; i < 20; i++)
	{
		auto& o = enemyBulletList_[i];
		if (o.isAlive())
		{
			enemyBulletSprite_.setPosition(o.getPosition());
			enemyBulletSprite_.setRotation(o.getDirection());
			render_->renderToWindow()->draw(enemyBulletSprite_, sf::BlendAdd);
		}
	}
}

void EnemyBulletManager::collideCircle(Circle * circle)
{
	for (int i = 0; i < 40; i++)
	{
		auto& o = enemyBulletList_[i];
		if (o.isAlive())
		{
			collisionShape_.setPosition(o.getPosition());
			if (CollisionManager::collideShapeCir(&collisionShape_, circle))
				o.kill();
		}
	}
}

bool EnemyBulletManager::collideShape(Shape * shape)
{
	for (int i = 0; i < 40; i++)
	{
		auto& o = enemyBulletList_[i];
		if (o.isAlive())
		{
			collisionShape_.setPosition(o.getPosition());
			if (CollisionManager::collideShape(&collisionShape_, shape))
			{
				o.kill();
				return true;
			}
		}
	}

	return false;
}

bool EnemyBulletManager::findEmptySlot()
{
	for (int i = lastDeleted_; i < 20; i++)
		if (!enemyBulletList_[i].isAlive())
		{
			lastDeleted_ = i;
			return true;
		}

	for (int i = 0; i < lastDeleted_; i++)
		if (!enemyBulletList_[i].isAlive())
		{
			lastDeleted_ = i;
			return true;
		}

	return false;
}

void EnemyBulletManager::clean()
{
	for (int i = 0; i < 20; i++)
	{
		auto& o = enemyBulletList_[i];
		if (o.isAlive())
		{
			o.kill();
		}
	}
}