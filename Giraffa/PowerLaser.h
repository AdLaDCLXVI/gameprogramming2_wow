#ifndef _PowerLaser_H_
#define _PowerLaser_H_
#include "Entity.h"
#include "Shape.h"

class Render;

class PowerLaser : public Entity
{
public:
	PowerLaser() {};
	PowerLaser(Render* pRenderer, float pX, float pY);
	~PowerLaser() {};
	void Draw();
	ENTITY_TYPES GetType();
	void Update(float DeltaTime) {};
	void Update(float DeltaTime, vec2 pTargetPos, bool setState);
	bool getActive();
	Shape GetShape();

private:
	float mAngle;
	bool isActive;
	Render* mRenderer;
	vec2 mTargetPos;

};
#endif