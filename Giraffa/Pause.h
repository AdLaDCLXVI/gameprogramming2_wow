#pragma once
class Animation;
class Render;
class Pause {
public:
	Pause();
	~Pause();
	void Init(Render* pRender);
	void Update(float DetlaTime);
	void Draw();
private:
	sf::Sprite PauseText;
	sf::Texture* texture_;
	Render* mRender_;
};