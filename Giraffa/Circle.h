#ifndef CIRCLE_H
#define CIRCLE_H
#include <SFML\System\Vector2.hpp>

class Circle
{
public:
	Circle();
	Circle(const sf::Vector2f& pos, float radius);
	~Circle() {};
	const sf::Vector2f& getPosition() const;
	void setPosition(const sf::Vector2f& pos);
	float getRadius();
	void setRadius(float radius);
private:
	sf::Vector2f position_;
	float radius_;
};
#endif