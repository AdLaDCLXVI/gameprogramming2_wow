#include "Global.h"
#include "NewMath.h"
#include "Input.h"
#include "Render.h"
#include "Player.h"
#include "PickupManager.h"
#include <sstream>
#include "WinScreen.h"
#include "MenuState.h"
#include "OptionState.h"
#include "Pause.h"
#include "CheckPointManager.h"
#include "AsteroidManager.h"
#include "HighScore.h"
#include "Shield.h"
#include "GameState.h"
#include "ExitState.h"

GameState::GameState(Render* render, Input* input, int level, AudioManager* audio)
{
	render_ = render;
	input_ = input;
	audio_ = audio;
	currentLevel_ = level;
	pauseScreen_.Init(render_);
	pause_ = false;
	returnState = 0;
}

void GameState::enter()
{

	player_ = new Player(render_, input_);
	player_->setPosition({ WIDTH / 3.0f, (HEIGHT + HEIGHT_HUD) / 2.0f });

	playerBulletManager_ = new PlayerBulletManager(render_);
	pickupManager_ = new PickupManager(render_);
	checkPointManager_ = new CheckPointManager(render_, currentLevel_);

	enemyManager_ = new EnemyManager(render_, pickupManager_, audio_);
	setLevel(currentLevel_);

	asteroidManager_ = new AsteroidManager(render_, audio_);

	hudTexture_ = render_->addTexture("assets/textures/family.png");
	familySprite_.setTexture(*hudTexture_);

	hudTexture_ = render_->addTexture("assets/textures/spr_gameHud.png");
	hudSprite_.setTexture(*hudTexture_);

	playerShoot_ = audio_->AddSound("assets/sound/GiraffaAttack.wav");
	powerLight_ = audio_->AddSound("assets/sound/electricPowerup.ogg");
	powerUp_ = audio_->AddSound("assets/sound/powerupPickup.ogg");
	soundPlayer_.setVolume(audio_->sfxVolume_);

	shield_ = new Shield(render_);

	ResumeButton_ = Button({ 640, 350 }, render_->getButtonTexture(), { 1065.0f, 528.0f, 235.0f, 63.0f });
	BackToLevelSelectButton_ = Button({ 650, 425 }, render_->getButtonTexture(), { 0.0f, 325.0f, 731.0f, 69.0f });
	QuitGameButton_ = Button({ 640, 510 }, render_->getButtonTexture(), { 596.0f, 829.0f, 299.0f, 76.0f });
}

bool GameState::update(float deltaTime)
{
	if (input_->getKeyPressed(sf::Keyboard::Escape))
	{
		if (!pause_){
			pause_ = true;
		}
		else {
			pause_ = false;
		}

		
	}
	if(!pause_)
	{
		if(player_->isAlive())
		{
			render_->updateBackground(deltaTime, 10.0f);
			player_->update(deltaTime);
			pickupManager_->update(deltaTime);

			checkPointManager_->update(deltaTime, player_);
			pickupManager_->collidePlayer(player_->getShape());

			if (enemyManager_->collidePlayer(player_->getShape()) || asteroidManager_->collideShape(player_->getShape(), HitInfo()))
			{
				//player_->kill();
				enemyManager_->KillPlayer(player_->getPosition());
			}

			enemyManager_->update(deltaTime);
			if (enemyManager_->isEmpty())
			{
				if (!checkPointManager_->nextCheckPoint())
					return false;	
			}


			if (asteroidManager_->getClean())
			{
				enemyManager_->clean();
				playerBulletManager_->clean();
			}



			if (input_->getKeyPressed(sf::Keyboard::Space))
			{
				PowerupStates state = pickupManager_->getCurrentPickup();
				if (state != POWERUP_SHIELD)
					playerBulletManager_->setPowerup(state);
				else
					shield_->activate();

				pickupManager_->empty();
			}


			if (input_->getKeyPressed(sf::Keyboard::F1))
				enemyManager_->activateBoss();


	

			if (player_->shootRequest())
			{
				sf::Vector2f pos = NewMath::rotateVec({ 34.0f, -55.0f }, player_->getAngle()) + player_->getPosition();
				playerBulletManager_->addBullet(pos, (sf::Vector2f)input_->getMousePos());
				soundPlayer_.setBuffer(playerShoot_->mClip);
				audio_->PlaySoundRand(&soundPlayer_);
			}

			playerBulletManager_->update(deltaTime, enemyManager_, asteroidManager_);
			enemyManager_->update(deltaTime);
			if (!asteroidManager_->getStop())
				asteroidManager_->update(deltaTime, player_);

			if (checkPointManager_->passedCheckPoint())
				setLevel(checkPointManager_->getCheckPoint());

			shield_->update(deltaTime, player_->getPosition(),
				enemyManager_,
				asteroidManager_);
		}
		if (player_->isAlive() && enemyManager_->GameOver())
		{
			
				if (!family_.isAlive())
				{
					family_.init({ WIDTH / 2, HEIGHT });
					family2_.init({ WIDTH / 2 + 40, HEIGHT });
					family3_.init({ WIDTH / 2 - 30, HEIGHT });
					
				}
				family_.update(deltaTime, player_->getPosition() + sf::Vector2f(NewMath::randRange(-60, 20), NewMath::randRange(-60, 20)));
				family2_.update(deltaTime, player_->getPosition() + sf::Vector2f(NewMath::randRange(-60, 20), NewMath::randRange(-60, 20)));
				family3_.update(deltaTime, player_->getPosition() + sf::Vector2f(NewMath::randRange(-60, 20), NewMath::randRange(-60, 20)));

				if (input_->getAnyKeyPressed())
				{
					if (family_.getPosition().y < player_->getPosition().y + 50 && family_.getPosition().y > player_->getPosition().y - 50)
					{
						return false;
					}
				}
		
			return true;
		}
		else if (!player_->isAlive())
		{
			enemyManager_->update(deltaTime);
			enemyManager_->draw();
			if (input_->getAnyKeyPressed())
				return false;
			return true;
		}
	}
	else
	{
		OptionButton_.update((sf::Vector2f)input_->getMousePos());
		if (input_->getMousePressed(sf::Mouse::Left))
			OptionButton_.setPressed((sf::Vector2f)input_->getMousePos());

		if (OptionButton_.reset())
		{
			audio_->PlayMenuSound();
			returnState = 2;
			return false;
		}
		QuitGameButton_.update((sf::Vector2f)input_->getMousePos());
		if (input_->getMousePressed(sf::Mouse::Left))
			QuitGameButton_.setPressed((sf::Vector2f)input_->getMousePos());

		if (QuitGameButton_.reset())
		{
			audio_->PlayMenuSound();
			returnState = 4;
			return false;
		}
		BackToLevelSelectButton_.update((sf::Vector2f)input_->getMousePos());
		if (input_->getMousePressed(sf::Mouse::Left))
			BackToLevelSelectButton_.setPressed((sf::Vector2f)input_->getMousePos());

		if (BackToLevelSelectButton_.reset())
		{
			audio_->PlayMenuSound();
			returnState = 1;
			return false;
		}
		ResumeButton_.update((sf::Vector2f)input_->getMousePos());
		if (input_->getMousePressed(sf::Mouse::Left))
			ResumeButton_.setPressed((sf::Vector2f)input_->getMousePos());

		if (ResumeButton_.reset())
		{
			pause_ = false;
			ResumeButton_.unPress();
		}
	}

	
	return true;
}
	

void GameState::draw()
{
	checkPointManager_->draw();
	if (player_->isAlive())
	{
		player_->draw();
		shield_->draw();
	}
	if (family_.isAlive())
	{
		familySprite_.setPosition(family_.getPosition());
		familySprite_.setTextureRect({ 267, 47, 111, 132 });
		render_->renderToWindow()->draw(familySprite_);

		familySprite_.setPosition(family2_.getPosition());
		familySprite_.setTextureRect({ 85, 200, 111, 132 });
		render_->renderToWindow()->draw(familySprite_);

		familySprite_.setPosition(family3_.getPosition());
		familySprite_.setTextureRect({ 320, 320, 111, 132 });
		render_->renderToWindow()->draw(familySprite_);
	}
		
	enemyManager_->draw();
	asteroidManager_->draw();
	playerBulletManager_->draw();
	pickupManager_->draw();
	if (pause_)
	{
		pauseScreen_.Draw();
		render_->renderToWindow()->draw(*OptionButton_.getSprite());
		render_->renderToWindow()->draw(*BackToLevelSelectButton_.getSprite());
		render_->renderToWindow()->draw(*QuitGameButton_.getSprite());
		render_->renderToWindow()->draw(*ResumeButton_.getSprite());
		
	}
}

void GameState::drawFront()
{
	render_->renderToWindow()->draw(hudSprite_);
	std::stringstream text;
	text << enemyManager_->GetScore();
	render_->drawText(text.str(), 100, 25);
	pickupManager_->drawHudPickup();
}

States GameState::getState()
{
	return STATE_GAME;
}

State* GameState::getNextState()
{
	if (!pause_)
	{
		if (!player_->isAlive())
		{
			audio_->SetGlobalMusic("assets/music/MenuMusik.wav");
			return new HighScore(render_, input_, audio_, enemyManager_->GetScore());
		}
		else
		{
			return new HighScore(render_, input_, audio_, enemyManager_->GetScore());
		}
	}
	else
	{
		switch (returnState)
		{
		case 4:
			return new ExitState(render_);
		case 2:
			audio_->SetGlobalMusic("assets/music/MenuMusik.wav");
			return new OptionState(render_, input_, audio_);
		default:
			audio_->SetGlobalMusic("assets/music/MenuMusik.wav");
			return new MenuState(render_, input_, audio_);
		}
	}

}

void GameState::exit()
{
	render_->deleteTexture(hudTexture_);
	delete shield_;
	delete checkPointManager_;
	delete pickupManager_;
	delete asteroidManager_;
	delete enemyManager_;
	delete playerBulletManager_;
	delete player_;
	pauseScreen_.~Pause();
}

void GameState::setLevel(int level)
{
	switch (level)
	{
	case 0:
		enemyManager_->init("assets/lvl_1.lvl");
		return;
	case 1:
		enemyManager_->init("assets/lvl_2.lvl");
		return;
	case 2:
		enemyManager_->init("assets/lvl_3.lvl");
		return;
	case 3:
		enemyManager_->activateBoss();
		asteroidManager_->stop();
	default:
		return;
	}
}