#include "DeadEnemyThree.h"

DeadEnemyThree::DeadEnemyThree(sf::Texture* texture, sf::Vector2f pos)
{
	position_ = pos;

	enemySprite_ = new Animation;
	enemySprite_->init(9, 2.0f, 249, 220);
	enemySprite_->getSprite()->setTexture(*texture);
	enemySprite_->getSprite()->setOrigin({ 64, 64 });
	enemySprite_->getSprite()->setPosition(position_);
}

DeadEnemyThree::~DeadEnemyThree()
{
	delete enemySprite_;
}

void DeadEnemyThree::Update(float deltaTime)
{
	enemySprite_->update(deltaTime);
}

sf::Sprite* DeadEnemyThree::GetSprite()
{
	return enemySprite_->getSprite();
}

bool DeadEnemyThree::Destroy()
{
	return enemySprite_->animationEnd();
}