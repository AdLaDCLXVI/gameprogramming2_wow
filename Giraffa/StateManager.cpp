#include "Input.h"
#include "State.h"
#include "Render.h"
#include "CreditState.h"
#include "StateManager.h"
#include "TitleScreen.h"
#include "AudioManager.h"

StateManager::StateManager(Render* render, Input* input)
{
	render_ = render;
	input_ = input;
	currentState_ = nullptr;
	audio_ = new AudioManager();
	audio_->SetGlobalMusic("assets/music/MenuMusik.wav");
	audio_->SetSound();
	setState(new TitleScreen(render_, input_, audio_));
}

StateManager::~StateManager()
{
	if (currentState_ != nullptr)
		delete currentState_;
}

bool StateManager::update(float deltaTime)
{
	if (currentState_ != nullptr)
	{
		audio_->Update();
		if (currentState_->update(deltaTime))
			return true;
		else
		{
			setState(currentState_->getNextState());
			return true;
		}
	}
	return false;
}

void StateManager::draw()
{
	if (currentState_ != nullptr)
		currentState_->draw();
}

void StateManager::drawFront()
{
	if (currentState_ != nullptr)
		currentState_->drawFront();
}

void StateManager::setState(State* state)
{
	if (currentState_ != nullptr)
	{
		currentState_->exit();
		delete currentState_;
	}

	currentState_ = state;
	if (currentState_ != nullptr)
		currentState_->enter();
}