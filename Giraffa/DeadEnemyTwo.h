#pragma once
#include "SFML\Graphics.hpp"
#include "Animation.h"
class DeadEnemyTwo
{
public:
	DeadEnemyTwo(sf::Texture* texture, sf::Vector2f pos);
	~DeadEnemyTwo();
	void Update(float deltaTime);
	bool Destroy();
	sf::Sprite* GetSprite();
private:
	bool IsAlive();
	sf::Vector2f position_;
	Animation* enemySprite_;
};