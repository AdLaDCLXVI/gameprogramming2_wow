#include "TextureManager.h"

TextureManager::~TextureManager()
{
	clearTextures();
}

sf::Texture* TextureManager::createTexture(std::string path)
{
	auto find = textures_.find(path);
	if (find == textures_.end())
	{
		sf::Texture* t = new sf::Texture();
		t->loadFromFile(path);
		t->setSmooth(true);

		textures_.insert(std::pair<std::string, sf::Texture*>(path, t));
		return t;
	}

	return find->second;
}

void TextureManager::deleteTexture(sf::Texture* texture)
{
	auto iterate = textures_.begin();
	while (iterate != textures_.end())
	{
		if (iterate->second == texture)
		{
			delete iterate->second;
			textures_.erase(iterate);
			return;
		}
		iterate++;
	}
}

void TextureManager::clearTextures()
{
	auto iterate = textures_.begin();
	while (iterate != textures_.end())
	{
		delete iterate->second;
		iterate++;
	}
	textures_.clear();
}