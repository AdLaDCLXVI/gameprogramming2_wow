#include "MenuState.h"
#include "GameState.h"
#include "HighScore.h"
#include "Render.h"
#include "Input.h"
#include "AudioManager.h"
#include "Button.h"
#include <fstream>
#include <sstream>
#include <iostream>

HighScore::HighScore(Render* pRenderer, Input* pInput, AudioManager* audio, int playerscore)
{
	mRenderer = pRenderer;
	mInput = pInput;
	audio_ = audio;

	returnState_ = 0;
}

void HighScore::enter()
{
	std::fstream file;
	file.open("assets/HighScore.txt");
	for (int i = 0; i < 5; i++)
	{
		file >> Score[i];
		file >> Name[i];
	}
	file.close();
	// sf::Texture* menuTexture_ = mRenderer->addTexture("assets/textures/spr_buttons.png");
	BackToMenuButton_ = Button({ 650, 650 }, mRenderer->getButtonTexture(), { 732.0f, 325.0f, 456.0f, 69.0f });
	retryButton_ = Button({ 640.0f, 550.0f }, mRenderer->getButtonTexture(), { 1669.0f, 0.0f, 209.0f, 69.0f });
}

void HighScore::exit()
{
	
}

bool HighScore::update(float DeltaTime)
{
	BackToMenuButton_.update((sf::Vector2f)mInput->getMousePos());
	retryButton_.update((sf::Vector2f)mInput->getMousePos());
	if (mInput->getMousePressed(sf::Mouse::Left))
	{
		retryButton_.setPressed((sf::Vector2f)mInput->getMousePos());
		BackToMenuButton_.setPressed((sf::Vector2f)mInput->getMousePos());
	}

	if (BackToMenuButton_.reset())
	{
		audio_->PlayMenuSound();
		returnState_ = 1;
		return false;
	}

	if (retryButton_.reset())
	{
		returnState_ = 2;
		return false;
	}
	return true;

}

void HighScore::draw()
{
	std::string text;
	for (int i = 0; i < 5; i++)
	{	
		std::stringstream strs;
		mRenderer->drawText( Name[i], 500, 50 * i +100);
		strs << Score[i];
		text = strs.str();
		mRenderer->drawText(text.c_str(), 700, 50 * i + 100);

	}

	mRenderer->renderToWindow()->draw(*BackToMenuButton_.getSprite());
	mRenderer->renderToWindow()->draw(*retryButton_.getSprite());
}

State* HighScore::getNextState()
{
	switch (returnState_)
	{
	case 1 :
		return new MenuState(mRenderer, mInput, audio_);
	case 2:
		return new GameState(mRenderer, mInput, 0, audio_);
	default:
		return nullptr;
		break;
	}
}

States HighScore::getState()
{
	return STATE_HIGHSCORE;
}