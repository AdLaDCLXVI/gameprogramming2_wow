#ifndef HITINFO_H
#define HITINFO_H
#include <SFML\System\Vector2.hpp>
struct HitInfo
{
	sf::Vector2f mtvVec_;
	float        mtvMag_;
};
#endif