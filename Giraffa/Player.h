#ifndef PLAYER_H
#define PLAYER_H
#include <SFML\Graphics.hpp>
#include "Animation.h"

class Input;
class Render;
class Shape;

class Player
{
public:
	Player(Render* render, Input* input);
	~Player();
	void update(float deltaTime);
	const sf::Vector2f& getPosition() const;
	const sf::Vector2f& getVelocity() const;
	void setPosition(const sf::Vector2f& pos);
	void setVelocity(const sf::Vector2f& vel);
	void draw();
	const sf::Vector2f& getAngle() const;
	Shape* getShape();
	bool shootRequest();
	bool isAlive();
	void kill();
private:
	Render* render_;
	Input* input_;
	sf::Vector2f position_;
	sf::Vector2f velocity_;
	sf::Vector2f angle_;
	sf::Sprite playerSprite_;
	sf::Sprite forelegSprite_;
	sf::Sprite hindlegSprite_;
	Animation rocketSprite_;
	sf::Texture* rocketTexture_;
	Shape* collisionShape_;

	const float maxSpeed_ = 23.0f;
	bool shootRequest_;
	bool alive_;
};
#endif