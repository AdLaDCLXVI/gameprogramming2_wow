#pragma once
#include "SFML\Graphics.hpp"
#include "Animation.h"
class DeadEnemy
{
public:
	DeadEnemy(sf::Texture* texture, sf::Vector2f pos);
	~DeadEnemy();
	void Update(float deltaTime);
	bool Destroy();
	sf::Sprite* GetSprite();
private:
	sf::Vector2f position_;
	Animation* enemySprite_;
};