#ifndef ENEMYONE_H
#define ENEMYONE_H
#include "SFML\Graphics.hpp"
#include "Animation.h"

class EnemyOne
{
public:
	EnemyOne();
	~EnemyOne();
	void init(sf::Vector2f pos, sf::Vector2f vel, sf::Texture* texture);
	void update(float deltaTime);
	sf::Vector2f getPosition();
	void setPosition(float x, float y);
	void setVelocity(float x, float y);
	void setTexture(sf::Texture* texture);
	bool isAlive();
	void kill();
	Animation* getSprite();
	bool shootRequest();
	bool isInView();
private:
	Animation* enemySprite_;
	sf::Vector2f position_;
	sf::Vector2f velocity_;
	float timer_;
	bool alive_;
	char states_;
	float wave_;
};
#endif