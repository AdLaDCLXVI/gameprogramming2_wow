#ifndef StartState_H
#define StartState_H
#include <SFML\Graphics.hpp>
#include "Button.h"
#include "State.h"
#include "Animation.h"

class Render;
class Input;
class AudioManager;
class Shape;

class StartState : public State
{
public:
	StartState(Render* render, Input* input, AudioManager* audio);
	~StartState() {};
	void enter();
	bool update(float deltaTime);
	void draw();
	float x;
	float y;
	States getState();
	State* getNextState();
	void exit();
private:
	int returnState;
	float wave_;

	Render* render_;
	Input* input_;
	AudioManager* audio_;
	sf::Texture* menuTexture_;
	Button menuButton_;
	Button levelOneButton_;
	Button levelTwoButton_;
	Button levelThreeButton_;
	Button levelFourButton_;
	sf::Sprite Image_;
	sf::Texture* imageTexture_;
	Animation anykey_;
	sf::Texture* anykeyTexture_;
	sf::Sound soundPlayer_;
	soundClip* Level;
	musicClip* music_;
};
#endif