#include "Global.h"
#include "NewMath.h"
#include "EnemyOne.h"

EnemyOne::EnemyOne()
{
	alive_ = false;
	states_ = 0x0;
	enemySprite_ = nullptr;
	wave_ = 0.0f;
	timer_ = 0.0f;
}

EnemyOne::~EnemyOne()
{
	delete enemySprite_;
}

void EnemyOne::init(sf::Vector2f pos, sf::Vector2f vel, sf::Texture* texture)
{
	enemySprite_ = new Animation();
	enemySprite_->init(5, 2.0f, 169, 119);
	enemySprite_->getSprite()->setTexture(*texture);
	enemySprite_->getSprite()->setOrigin({ 84, 60 });
	
	position_ = pos;
	velocity_ = vel;
	enemySprite_->getSprite()->setPosition(position_);
	float tmp = NewMath::randNormal() * 360.0f;
	wave_ = tmp;
	timer_ = tmp;	

	alive_ = true;
}

void EnemyOne::update(float deltaTime)
{
	if (position_.x < -64)
		alive_ = false;

	if (position_.x > -64.0f && position_.x < 64.0f + WIDTH && position_.y > -64.0f && position_.y < 64.0f + HEIGHT)
	{
		position_ += velocity_ * deltaTime;
		states_ = 0x1;
		timer_ += 3 * deltaTime;

		if (timer_ > 100)
			timer_ -= 100;

		if ((int)timer_ == 50)
		{
			states_ = states_ | 0x2;
		}

		wave_ += 10.0f * deltaTime;
		if (wave_ > 360.0f)
			wave_ -= 360.0f;
		velocity_.y = sinf(wave_ * DTR) * 2.0f;

	}
	else
	{
		position_.x -= 5.0f * deltaTime;
		states_ = 0x0;
	}

	enemySprite_->update(deltaTime);
	enemySprite_->getSprite()->setPosition(position_);
}

sf::Vector2f EnemyOne::getPosition()
{
	return position_;
}

void EnemyOne::setPosition(float x, float y)
{
	position_.x = x;
	position_.y = y;
}

void EnemyOne::setVelocity(float x, float y)
{
	velocity_.x = x;
	velocity_.y = y;
}

void EnemyOne::setTexture(sf::Texture* texture)
{
	enemySprite_->getSprite()->setTexture(*texture);
}

bool EnemyOne::isAlive()
{
	return alive_;
}

void EnemyOne::kill()
{
	alive_ = false;
}

Animation* EnemyOne::getSprite()
{
	return enemySprite_;
}

bool EnemyOne::shootRequest()
{
	return states_ >> 0x1 == 0x1;
}

bool EnemyOne::isInView()
{
	return (states_ & 0x1) == 0x1;
}