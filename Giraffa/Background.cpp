#include "Global.h"
#include "Background.h"

Background::Background()
{
	backgroundTexture_.loadFromFile("assets/textures/spr_background.png");
	backgroundTexture_.setRepeated(true);
	backgroundTexture_.setSmooth(true);

	starsTexture_.loadFromFile("assets/textures/spr_stars.png");
	starsTexture_.setRepeated(true);
	starsTexture_.setSmooth(true);

	backgroundSprite_.setTexture(backgroundTexture_);
	starsSprite_.setTexture(starsTexture_);

	backgroundShader_.loadFromFile("assets/vertex.vert", sf::Shader::Vertex);
	backgroundShader_.setParameter("newColor", sf::Color(128, 128, 255));
	state_.shader = &backgroundShader_;

	backgroundScroll_ = 0.0f;
	starScroll_ = 0.0f;
}

void Background::update(float deltaTime, float speed)
{
	backgroundScroll_ += speed * deltaTime;
	if (backgroundScroll_ > WIDTH)
		backgroundScroll_ -= (int)WIDTH;

	starScroll_ += speed * 1.5f * deltaTime;
	if (starScroll_ > WIDTH)
		starScroll_ -= (int)WIDTH;

	backgroundSprite_.setTextureRect(sf::IntRect((int)backgroundScroll_, 0, WIDTH, HEIGHT));
	starsSprite_.setTextureRect(sf::IntRect((int)starScroll_, 0, WIDTH, HEIGHT));
}

const sf::Sprite& Background::getSprite() const
{
	return backgroundSprite_;
}

const sf::Sprite& Background::getSprite2() const
{
	return starsSprite_;
}

const sf::RenderStates& Background::getState() const
{
	return state_;
}