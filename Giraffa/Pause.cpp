#include <SFML\Graphics.hpp>
#include "Render.h"
#include "Pause.h"



Pause::Pause()
{


}
Pause::~Pause()
{

}
void Pause::Init(Render* pRender)
{
	mRender_ = pRender;
	texture_ = pRender->addTexture("assets/textures/Pause.png");
	PauseText.setTexture(*texture_);
	PauseText.setPosition(460, 200);
}
void Pause::Update(float DeltaTime)
{

}
void Pause::Draw()
{
	mRender_->renderToWindow()->draw(PauseText);
}