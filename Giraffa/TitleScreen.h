#ifndef TitleScreen_H
#define TitleScreen_H
#include <SFML\Graphics.hpp>
#include "Button.h"
#include "State.h"
#include "Animation.h"

class Render;
class Input;
class Shape;
class AudioManager;
class TitleScreen : public State
{
public:
	TitleScreen(Render* render, Input* input, AudioManager* audio);
	~TitleScreen() {};
	void enter();
	bool update(float deltaTime);
	void draw();
	float x;
	float y;
	States getState();
	State* getNextState();
	void exit();

private:
	bool active;

	Render* render_;
	Animation anykey_;
	Input* input_;
	AudioManager* audio_;
	sf::Texture* anykeyTexture_;
	sf::Sprite Image_;
	sf::Texture* imageTexture_;
	musicClip* music_;

	float wave_;
};
#endif