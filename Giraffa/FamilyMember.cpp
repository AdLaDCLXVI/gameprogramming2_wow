#include "FamilyMember.h"
FamilyMember::FamilyMember()
{
	alive_ = false;
}

void FamilyMember::init(sf::Vector2f pos)
{
	position_ = pos;
	alive_ = true;
}

void FamilyMember::update(float deltaTime, sf::Vector2f pos)
{
	if (position_.x < pos.x && velocity_.x < 2)
		velocity_.x += deltaTime;
	else if (position_.x > pos.x && velocity_.x > -2)
		velocity_.x -= deltaTime;
	if (position_.y < pos.y && velocity_.y < 2)
		velocity_.y += deltaTime;
	else if (position_.y > pos.y && velocity_.y > -2)
		velocity_.y -= deltaTime;

	position_ += velocity_;
}
sf::Vector2f FamilyMember::getPosition()
{
	return position_;
}
bool FamilyMember::isAlive()
{
	return alive_;
}