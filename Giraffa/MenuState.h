#ifndef MENUSTATE_H
#define MENUSTATE_H
#include <SFML\Graphics.hpp>
#include "Button.h"
#include "State.h"
#include "Animation.h"

class Render;
class Input;
class AudioManager;
class Shape;
class Game;

class MenuState : public State
{
public:
	MenuState(Render* render, Input* input, AudioManager* audio);
	~MenuState() {};
	void enter();
	bool update(float deltaTime);
	void draw();
	float x;
	float y;
	States getState();
	State* getNextState();
	void exit();
private:
	int returnState;

	Game* game_;
	Render* render_;
	Input* input_;
	AudioManager* audio_;
	sf::Texture* menuTexture_;
	Button OptionButton_;
	Button CreditsButton_;
	Button BackToLevelSelectButton_;
	Button QuitGameButton_;
	sf::Sprite Image_;
	sf::Texture* imageTexture_;
	Animation anykey_;
	sf::Texture* anykeyTexture_;
	

};
#endif