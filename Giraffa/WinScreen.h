#pragma once
#include "State.h"

class Render;
class Input;
class AudioManager;
class WinScreen : public State
{
public:
	WinScreen(Render* pRenderer, Input* pInput, AudioManager* audio, int score);
	bool update(float pDeltaTime);
	void exit();
	void enter();
	void draw();
	State* getNextState();
	States getState();
private:
	Render* mRenderer;
	Input* mInput;
	AudioManager* audio_;
	int Score[6];
	std::string Name[6];
	float PlayerScore;
	std::string PlayerName;
	float position;
};