#ifndef PICKUP_H
#define PICKUP_H
#include <SFML\System\Vector2.hpp>
#include "PowerupStates.h"

class Pickup
{
public:
	Pickup();
	~Pickup() {};
	void init(const sf::Vector2f& pos, const sf::Vector2f& vel);
	void update(float deltaTime);
	const sf::Vector2f& getPosition() const;
	float getAngle();
	PowerupStates getPowerup();
	bool isAlive();
	void kill();
private:
	sf::Vector2f position_;
	sf::Vector2f velocity_;
	
	bool alive_;
	float wave_;
	PowerupStates powerup_;
};
#endif