#ifndef NAMESTATE_H
#define NAMESTATE_H
#include <sstream>
#include "State.h"

class Render;
class Input;
class AudioManager;

class NameState : public State
{
public:
	NameState(Render* rander, Input* input, AudioManager* audio);
	~NameState();
	void enter();
	bool update(float deltaTime);
	void draw();
	States getState();
	State* getNextState();
	void exit();
private:
	Render* render_;
	Input* input_;
	AudioManager* audio_;

	std::string userName_;
};
#endif