#include "Global.h"
#include "NewMath.h"
#include "Animation.h"
#include "EnemyThree.h"

EnemyThree::EnemyThree()
{
	alive_ = false;
	states_ = 0x0;
	enemySprite_ = nullptr;
	timer_ = 0.0;
}

EnemyThree::~EnemyThree()
{
	delete enemySprite_;
}

void EnemyThree::init(const sf::Vector2f& pos, const sf::Vector2f& vel, sf::Texture* texture)
{
	enemySprite_ = new Animation();
	enemySprite_->init(5, 2.0f, 234, 88);
	enemySprite_->getSprite()->setTexture(*texture);
	enemySprite_->getSprite()->setOrigin({ 117.0f, 44.0f });

	position_ = pos;
	velocity_ = vel;
	enemySprite_->getSprite()->setPosition(position_);
	timer_ = NewMath::randNormal() * 100.0f;

	alive_ = true;
}

void EnemyThree::update(float deltaTime)
{
	if (position_.x < -80.0f)
		alive_ = false;

	if (position_.x > -117.0f && position_.x < 117.0f + WIDTH && position_.y > -44.0f && position_.y < 44.0f + HEIGHT)
	{
		position_ += velocity_ * deltaTime;
		timer_ += 4.0f * deltaTime;
		if (timer_ > 100.0f)
			timer_ -= 100.0f;
		states_ = 0x1;

		switch ((int)timer_)
		{
		case 30:
			states_ = states_ | 0x2;
			break;
		case 50:
			states_ = states_ | 0x2;
			break;
		case 70:
			states_ = states_ | 0x2;
			break;
		default:
			break;
		}
	}
	else
	{
		position_.x -= 5.0f * deltaTime;
		states_ = 0x0;
	}

	enemySprite_->update(deltaTime);
	enemySprite_->getSprite()->setPosition(position_);
}

const sf::Vector2f& EnemyThree::getPosition() const
{
	return position_;
}


void EnemyThree::setTexture(sf::Texture* texture)
{
	enemySprite_->getSprite()->setTexture(*texture);
}

bool EnemyThree::isAlive()
{
	return alive_;
}

void EnemyThree::kill()
{
	alive_ = false;
}

sf::Sprite* EnemyThree::getSprite()
{
	return enemySprite_->getSprite();
}

bool EnemyThree::shootRequest()
{
	return states_ >> 0x1 == 0x1;
}