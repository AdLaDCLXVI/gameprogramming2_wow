#ifndef _BUTTON_H_
#define _BUTTON_H_
#include <SFML\Graphics.hpp>

class Button
{
public:
	Button() { pressed_ = false; };
	Button(const sf::Vector2f& pos, sf::Texture* texture, const sf::FloatRect& region);
	~Button() {};
	void update(const sf::Vector2f& mousePos);
	void setPressed(const sf::Vector2f& mousePos);
	bool isPressed();
	bool reset();
	void unPress();
	sf::Sprite* getSprite();
private:
	sf::Sprite buttonSprite_;

	bool pressed_;
};
#endif