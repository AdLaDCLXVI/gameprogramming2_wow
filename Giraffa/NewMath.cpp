#include "NewMath.h"

int NewMath::randRange(int min, int max) noexcept
{
	return min + rand() % (max - min);
}

float NewMath::randRange(float min, float max) noexcept
{
	float r = (float)rand() / RAND_MAX;
	return min + r * (max - min);
}

float NewMath::randNormal() noexcept
{
	return (float)rand() / RAND_MAX;
}

bool NewMath::randBool() noexcept
{
	return rand() % 2 == 1;
}

int NewMath::choose(int a, int b) noexcept
{
	return (rand() % 2 ? a : b);
}

float NewMath::choose(float a, float b) noexcept
{
	return (rand() % 2 ? a : b);
}

sf::Vector2f NewMath::randVec(float min, float max) noexcept
{
	sf::Vector2f tmp;
	tmp.x = randRange(min, max);
	tmp.y = randRange(min, max);
	return tmp;
}

sf::Vector2f NewMath::normalize(sf::Vector2f vec) noexcept
{
	float len = length(vec);

	if (len != 0)
		return vec / len;
	else
		return { 0.0f, 0.0f };
}

sf::Vector2f NewMath::inverse(const sf::Vector2f& vec) noexcept
{
	return sf::Vector2f(-vec.x, -vec.y);
}

sf::Vector2f NewMath::angle(const sf::Vector2f& vec1, const sf::Vector2f& vec2) noexcept
{
	sf::Vector2f delta = vec2 - vec1;
	return normalize(delta);
}

sf::Vector2f NewMath::rotateVec(const sf::Vector2f& vec, const sf::Vector2f& angle) noexcept
{
	sf::Vector2f tmp(vec.x * angle.x - vec.y * angle.y, vec.y * angle.x + vec.x * angle.y);
	return tmp;
}

sf::Vector2f NewMath::deflect(const sf::Vector2f & vec, sf::Vector2f normal) noexcept
{
	normal = normalize(normal);
	float dp = 2 * dot(vec, normal);
	return vec - normal * dp;
}

float NewMath::clamp(float val, float min, float max)
{
	if (val < min) return min;
	if (val > max) return max;
	return val;
}

float NewMath::dot(const sf::Vector2f & vec1, const sf::Vector2f & vec2) noexcept
{
	return vec1.x * vec2.x + vec1.y * vec2.y;
}

float NewMath::length(const sf::Vector2f& vec) noexcept
{
	return sqrtf(vec.x * vec.x + vec.y *vec.y);
}

sf::Vector2f NewMath::normal(const sf::Vector2f & vec) noexcept
{
	return sf::Vector2f(-vec.y, vec.x);
}

sf::Vector2f NewMath::align(const sf::Vector2f & vec1, const sf::Vector2f & vec2) noexcept
{
	return vec2 - vec1;
}

sf::Color NewMath::setHue(float hue)
{
	float r = clamp(fabsf(hue * 6.0f - 3.0f) - 1.0f, 0.0f, 1.0f);
	float g = clamp(2.0f - fabsf(hue * 6.0f - 2.0f), 0.0f, 1.0f);
	float b = clamp(2.0f - fabsf(hue * 6.0f - 4.0f), 0.0f, 1.0f);
	return sf::Color(r * 255.0f, g * 255.0f, b * 255.0f);
}
