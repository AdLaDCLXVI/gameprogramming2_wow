#include "Global.h"
#include "NewMath.h"
#include "TextureManager.h"
#include "Input.h"
#include "Background.h"
#include "Render.h"
#include <sstream>

Render::Render(sf::RenderWindow* window, Input* input)
{
	window_ = window;
	input_ = input;

	textureManager_ = new TextureManager();

	buttonTexture_ = textureManager_->createTexture("assets/textures/spr_buttons.png");

	inGameTexture_ = textureManager_->createTexture("assets/textures/spr_ingame.png");
	crosshairSprite_.setTexture(*inGameTexture_);
	crosshairSprite_.setTextureRect({ 0, 423, 42, 37});
	crosshairSprite_.setOrigin({ 24.0f, 24.0f });

	view_.reset(sf::FloatRect(0.0f, 0.0f, WIDTH, HEIGHT));
	view_.setViewport(sf::FloatRect(0.0f, 0.0f, 1.0f, 1.0f));
	shakeFactor_ = 0.0f;

	background_ = new Background();

	templateFont_.loadFromFile("assets/massive retaliation.ttf");
	templateText_.setFont(templateFont_);
	templateText_.setCharacterSize(30);
}

Render::~Render()
{
	delete background_;
	delete textureManager_;
}

void Render::update(float deltaTime)
{
	if (shakeFactor_ > 0.0f)
	{
		view_.reset(sf::FloatRect(
			NewMath::randRange(-shakeFactor_, shakeFactor_ * 2.0f),
			NewMath::randRange(-shakeFactor_, shakeFactor_ * 2.0f),
			WIDTH,
			HEIGHT));
		shakeFactor_ -= shakeSpeed_ * deltaTime;
	}
	else
	{
		shakeFactor_ = 0;
		view_.reset(sf::FloatRect(0.0f, 0.0f, WIDTH, HEIGHT));
	}

	
	crosshairSprite_.setPosition((sf::Vector2f)input_->getMousePos());
}

void Render::draw()
{
	window_->draw(crosshairSprite_);
}

void Render::drawBackground()
{
	window_->draw(background_->getSprite(), background_->getState());
	window_->draw(background_->getSprite2());
}

sf::RenderWindow* Render::renderToWindow()
{
	return window_;
}

sf::Texture* Render::addTexture(std::string path)
{
	return textureManager_->createTexture(path);
}

sf::Texture* Render::getInGameTexture()
{
	return inGameTexture_;
}

sf::Texture* Render::getButtonTexture()
{
	return buttonTexture_;
}

void Render::deleteTexture(sf::Texture* texture)
{
	textureManager_->deleteTexture(texture);
}

sf::View* Render::getView()
{
	return &view_;
}

void Render::shakeScreen(int factor)
{
	shakeFactor_ = (float)factor;
}
void Render::updateBackground(float deltaTime, float speed)
{
	background_->update(deltaTime, speed);
}

void Render::drawText(std::string text, float X, float Y, const sf::Color& color)
{
	templateText_.setOrigin(0.0f, 0.0f);
	templateText_.setPosition({ X, Y });
	templateText_.setString(text);
	templateText_.setColor(color);
	window_->draw(templateText_);
}

void Render::drawTextCenter(float x, float y, std::string text, const sf::Color & color)
{
	templateText_.setString(text);
	templateText_.setOrigin(templateText_.getLocalBounds().width / 2.0f, 0.0f);
	templateText_.setPosition({ x, y });
	templateText_.setColor(color);
	window_->draw(templateText_);
}
