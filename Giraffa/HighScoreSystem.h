#ifndef HIGHSCORESYSTEM_H
#define HIGHSCORESYSTEM_H
#include <vector>

struct Entry
{
	int score_;
	std::string name_;
};

struct SortEntries
{
	bool operator() (const Entry& a, const Entry& b) const
	{
		return (a.score_ > b.score_);
	}
};

class HighScoreSystem
{
public:
	HighScoreSystem();
	~HighScoreSystem();
	void addEntry(int score, std::string name);
	int getEntryScore(int place);
	const std::string& getEntryName(int place) const;
	int findEntry(std::string name);
private:
	std::vector<Entry> entries_;
};
#endif