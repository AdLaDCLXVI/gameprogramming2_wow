#ifndef STATE_H
#define STATE_H

enum States
{
	STATE_MENU,
	STATE_GAME,
	STATE_ENDSCREEN,
	STATE_TITLESCREEN,
	STATE_CREDITSTATE,
	STATE_STARTSTATE,
	STATE_OPTIONSTATE,
	STATE_HIGHSCORE,
	STATE_WINSCREEN,
	STATE_EXITSTATE
};

class State
{
public:
	virtual void enter() = 0;
	virtual bool update(float deletaTime) = 0;
	virtual void draw() = 0;
	virtual void drawFront() {};
	virtual States getState() = 0;
	virtual State* getNextState() = 0;
	virtual void exit() = 0;
};
#endif