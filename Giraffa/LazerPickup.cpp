#include "Global.h"
#include "NewMath.h"
#include "LazerPickup.h"

LazerPickup::LazerPickup()
{
	alive_ = false;
	wave_ = NewMath::randNormal() * 360.0f;
}

void LazerPickup::init(const sf::Vector2f& pos, const sf::Vector2f& vel)
{
	position_ = pos;
	velocity_ = vel;
	alive_ = true;
}

void LazerPickup::update(float deltaTime)
{
	position_ += velocity_ * deltaTime;

	if (position_.x < 64.0f)
		alive_ = false;

	if (position_.y < HEIGHT_HUD + 52.0f)
	{
		position_.y = HEIGHT_HUD + 52.0f;
		velocity_.y *= -1.0f;
	}
	if (position_.y > HEIGHT - 52.0f)
	{
		position_.y = HEIGHT - 52.0f;
		velocity_.y *= -1.0f;
	}

	wave_ += 10.0f * deltaTime;
	if (wave_ > 360.0f)
		wave_ -= 360.0f;
}

const sf::Vector2f& LazerPickup::getPosition() const
{
	return position_;
}

float LazerPickup::getAngle()
{
	return sinf(wave_);
}

bool LazerPickup::isAlive()
{
	return alive_;
}

void LazerPickup::kill()
{
	alive_ = false;
}