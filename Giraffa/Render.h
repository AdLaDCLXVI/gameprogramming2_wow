#ifndef RENDER_H
#define RENDER_H
#include <SFML\Graphics.hpp>

class TextureManager;
class Input;
class Background;

class Render
{
public:
	Render(sf::RenderWindow* window, Input* input);
	~Render();
	void update(float deltaTime);
	void draw();
	void drawBackground();
	sf::RenderWindow* renderToWindow();
	sf::Texture* addTexture(std::string path);
	sf::Texture* getInGameTexture();
	sf::Texture* getButtonTexture();
	void deleteTexture(sf::Texture* texture);
	sf::View* getView();
	void shakeScreen(int factor);
	void updateBackground(float deltaTime, float speed);
	void drawText(std::string text, float X, float Y, const sf::Color& color = sf::Color::White);
	void drawTextCenter(float x, float y, std::string text, const sf::Color& color = sf::Color::White);
private:
	sf::RenderWindow* window_;
	Input* input_;
	TextureManager* textureManager_;
	sf::View view_;
	sf::Texture* inGameTexture_;
	sf::Texture* buttonTexture_;

	Background* background_;
	sf::Sprite crosshairSprite_;

	sf::Text templateText_;
	sf::Font templateFont_;

	float shakeFactor_;
	const float shakeSpeed_ = 1.0f;
};
#endif