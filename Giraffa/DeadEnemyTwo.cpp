#include "DeadEnemyTwo.h"

DeadEnemyTwo::DeadEnemyTwo(sf::Texture* texture, sf::Vector2f pos)
{
	position_ = pos;

	enemySprite_ = new Animation;
	enemySprite_->init(9, 2.0f, 157, 157);
	enemySprite_->getSprite()->setTexture(*texture);
	enemySprite_->getSprite()->setOrigin({ 64, 64 });
	enemySprite_->getSprite()->setPosition(position_);
}

DeadEnemyTwo::~DeadEnemyTwo()
{
	delete enemySprite_;
}

void DeadEnemyTwo::Update(float deltaTime)
{
	enemySprite_->update(deltaTime);
}

sf::Sprite* DeadEnemyTwo::GetSprite()
{
	return enemySprite_->getSprite();
}

bool DeadEnemyTwo::Destroy()
{
	return enemySprite_->animationEnd();
}