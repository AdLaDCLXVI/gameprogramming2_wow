#ifndef ENEMYBULLET_H
#define ENEMYBULLET_H
#include <SFML\System\Vector2.hpp>

class EnemyBullet
{
public:
	EnemyBullet();
	~EnemyBullet() {};
	void init(const sf::Vector2f& pos, float angle, float speed);
	void update(float deltaTime);
	const sf::Vector2f& getPosition();
	float getDirection();
	bool isAlive();
	void kill();
private:
	sf::Vector2f position_;
	sf::Vector2f velocity_;

	bool alive_;
};
#endif