#pragma once
#include <SFML\Graphics.hpp>
class FamilyMember
{
public:
	FamilyMember();
	void init(sf::Vector2f pos);
	~FamilyMember() {};
	void update(float deltaTime, sf::Vector2f player);
	sf::Vector2f getPosition();
	bool isAlive();
private:
	sf::Vector2f position_;
	sf::Vector2f velocity_;
	bool alive_;
};