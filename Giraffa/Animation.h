#ifndef ANIMATION_H
#define ANIMATION_H
#include <SFML\Graphics.hpp>

class Animation
{
public:
	Animation();
	~Animation() {};
	void init(int frames, float speed, int width, int height);
	void setPositionY(int y);
	void setFrame(int frame);
	void update(float deltaTime);
	void setFrameSpeed(float speed);
	sf::Sprite* getSprite();
	bool animationEnd();

private:
	sf::Sprite sprite_;
	sf::IntRect region_;

	float speed_;
	int maxFrames_;
	float timer_;
	bool animationEnd_;
};
#endif