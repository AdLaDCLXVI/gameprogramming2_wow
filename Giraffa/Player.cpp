#include <math.h>
#include "Global.h"
#include "NewMath.h"
#include "Render.h"
#include "Input.h"
#include "Shape.h"
#include "Player.h"

Player::Player(Render* render, Input* input)
{
	render_ = render;
	input_ = input;
	angle_ = { 1.0f, 0.0f };
	alive_ = true;

	rocketTexture_ = render_->addTexture("assets/textures/spr_rocket.png");

	playerSprite_.setTexture(*render_->getInGameTexture());
	playerSprite_.setTextureRect({ 258, 0, 106, 97 });
	playerSprite_.setOrigin({ 53.0f, 70.0f });

	forelegSprite_.setTexture(*render_->getInGameTexture());
	forelegSprite_.setTextureRect({ 42, 424, 36, 53 });
	forelegSprite_.setOrigin({ 25.0f, 10.0f });

	hindlegSprite_.setTexture(*render_->getInGameTexture());
	hindlegSprite_.setTextureRect({ 470, 0, 35, 46 });
	hindlegSprite_.setOrigin({ 30.0f, 10.0f });

	collisionShape_ = new Shape();
	collisionShape_->addPoint(6.0f, -14);
	collisionShape_->addPoint(20.0f, 6);
	collisionShape_->addPoint(10.0f, 24);
	collisionShape_->addPoint(-30.0f, 24);
	collisionShape_->addPoint(-24.0f, 4);
	collisionShape_->setPosition(position_);

	rocketSprite_.init(4, 1.0f, 128, 128);
	rocketSprite_.getSprite()->setTexture(*rocketTexture_);
	rocketSprite_.getSprite()->setOrigin({ 68, 82 });

	shootRequest_ = false;
}

Player::~Player()
{
	render_->deleteTexture(rocketTexture_);
	delete collisionShape_;
}

void Player::update(float deltaTime)
{
	if (input_->getKey(sf::Keyboard::A))
		if (velocity_.x > -maxSpeed_) velocity_.x -= 1.2f;
	if (input_->getKey(sf::Keyboard::D))
		if (velocity_.x < maxSpeed_) velocity_.x += 1.2f;

	if (input_->getKey(sf::Keyboard::W))
		if (velocity_.y > -maxSpeed_) velocity_.y -= 1.2f;
	if (input_->getKey(sf::Keyboard::S))
		if (velocity_.y < maxSpeed_) velocity_.y += 1.2f;


	if (!input_->getKey(sf::Keyboard::A) && !input_->getKey(sf::Keyboard::D))
		velocity_.x *= 0.95f;
	if (!input_->getKey(sf::Keyboard::W) && !input_->getKey(sf::Keyboard::S))
		velocity_.y *= 0.95f;


	position_ += velocity_ * deltaTime;

	if (position_.x < 0)
	{
		position_.x = 0;
		velocity_.x *= -0.5f;
	}
	if (position_.x > WIDTH)
	{
		position_.x  = WIDTH;
		velocity_.x *= -0.5f;
	}
	if (position_.y < HEIGHT_HUD)
	{
		position_.y  = HEIGHT_HUD;
		velocity_.y *= -0.5f;
	}
	if (position_.y > HEIGHT)
	{
		position_.y  = HEIGHT;
		velocity_.y  *= -0.5f;
	}

	angle_ = NewMath::angle(position_, (sf::Vector2f)input_->getMousePos());
	float angle = atan2f(angle_.y, angle_.x) * RTD;
	playerSprite_.setRotation(angle);

	float rocketAngle = atan2f(velocity_.y, velocity_.x) * RTD;
	rocketSprite_.getSprite()->setRotation(rocketAngle - 90.0f);
	rocketSprite_.update(deltaTime);

	playerSprite_.setPosition(position_);

	sf::Vector2f pos = NewMath::rotateVec({ -20.0f, 10.0f }, angle_);
	float flailForce = NewMath::dot(velocity_, angle_) * 2.0f;
	hindlegSprite_.setPosition(position_ + pos);
	hindlegSprite_.setRotation(angle + flailForce);
	pos = NewMath::rotateVec({ 10.0f, 10.0f }, angle_);
	forelegSprite_.setPosition(position_ + pos);
	forelegSprite_.setRotation(angle + flailForce);


	rocketSprite_.getSprite()->setPosition(position_);

	collisionShape_->setPosition(position_);
	collisionShape_->rotate(angle_);

	if (input_->getMousePressed(sf::Mouse::Left))
		shootRequest_ = true;
	else
		shootRequest_ = false;
}

const sf::Vector2f& Player::getPosition() const
{
	return position_;
}

const sf::Vector2f& Player::getVelocity() const
{
	return velocity_;
}

void Player::setPosition(const sf::Vector2f& pos)
{
	position_ = pos;
}

void Player::setVelocity(const sf::Vector2f& vel)
{
	velocity_ = vel;
}

void Player::draw()
{
	render_->renderToWindow()->draw(playerSprite_);
	render_->renderToWindow()->draw(hindlegSprite_);
	render_->renderToWindow()->draw(forelegSprite_);
	render_->renderToWindow()->draw(*rocketSprite_.getSprite());
}

const sf::Vector2f& Player::getAngle() const
{
	return angle_;
}

Shape* Player::getShape()
{
	return collisionShape_;
}

bool Player::shootRequest()
{
	return shootRequest_;
}

bool Player::isAlive()
{
	return alive_;
}

void Player::kill()
{
	alive_ = false;
}