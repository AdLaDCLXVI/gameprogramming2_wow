#ifndef COLLISIONMANAGER_H
#define COLLISIONMANAGER_H
#include <SFML\System\Vector2.hpp>
#include <SFML\Graphics\Rect.hpp>
#include "Shape.h"
#include "Circle.h"
#include "HitInfo.h"

class CollisionManager
{
public:
	static bool collideShape(Shape* shapeA, Shape* shapeB, HitInfo& hitInfo);
	static bool collideShapeCir(Shape* shape, Circle* circle, HitInfo& hitInfo);
	static bool collideShape(Shape* shapeA, Shape* shapeB);
	static bool collideShapeCir(Shape* shape, Circle* circle);

	static bool collideRectPoint(const sf::Vector2f& pos, const sf::Vector2f& origin, float width, float height);
	static bool collideRectPoint(const sf::Vector2f& pos, const sf::FloatRect& region);
	static bool collideCirCir(Circle* circleA, Circle* circleB, HitInfo& hitInfo);
	static bool collideCirCir(Circle* circleA, Circle* circleB);
private:
	static void projectShape(const sf::Vector2f& axis, const sf::Vector2f& normal, Shape* shape, float& min, float& max);
	static float overlap(float min1, float max1, float min2, float max2);
	static float getOverlap(float min1, float max1, float min2, float max2);
};
#endif