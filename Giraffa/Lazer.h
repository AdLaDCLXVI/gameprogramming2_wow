#ifndef LAZER_H
#define LAZER_H
#include <SFML\System\Vector2.hpp>
#include "Shape.h"

class Lazer
{
public:
	Lazer();
	~Lazer() {};
	void update(const sf::Vector2f& pos, const sf::Vector2f& target);
	const sf::Vector2f& getPosition();
	const sf::Vector2f& getTarget();
	Shape* getShape();
	float getAngle();
	float getScale();
private:
	sf::Vector2f position_;
	sf::Vector2f target_;
	Shape collisionShape_;
};
#endif