#ifndef TEXTUREMANAGER_H
#define TEXTUREMANAGER_H
#include <map>
#include <SFML\Graphics.hpp>

class TextureManager
{
public:
	TextureManager() {};
	~TextureManager();
	sf::Texture* createTexture(std::string path);
	void         deleteTexture(sf::Texture* texture);
	void		 clearTextures();
private:
	std::map<std::string, sf::Texture*> textures_;
};
#endif