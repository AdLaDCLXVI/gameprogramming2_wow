#include "Circle.h"

Circle::Circle()
{
	radius_ = 0.0f;
}

Circle::Circle(const sf::Vector2f& pos, float radius)
{
	position_ = pos;
	radius_ = radius;
}

const sf::Vector2f& Circle::getPosition() const
{
	return position_;
}

void Circle::setPosition(const sf::Vector2f& pos)
{
	position_ = pos;
}

float Circle::getRadius()
{
	return radius_;
}

void Circle::setRadius(float radius)
{
	radius_ = radius;
}
