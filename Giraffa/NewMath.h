#ifndef NEWMATH_H
#define NEWMATH_H
#include <math.h>
#include <stdlib.h>
#include <SFML\System\Vector2.hpp>
#include <SFML\System\Vector3.hpp>
#include <SFML\Graphics\Color.hpp>

#define PI 3.14159265358979f
#define DTR PI / 180.0f
#define RTD 180.0f / PI

class NewMath
{
public:
	static int randRange(int min, int max) noexcept;
	static float randRange(float min, float max) noexcept;
	static float randNormal() noexcept;
	static bool randBool() noexcept;
	static int choose(int a, int b) noexcept;
	static float choose(float, float) noexcept;
	static sf::Vector2f randVec(float min, float max) noexcept;

	static sf::Vector2f normalize(sf::Vector2f vec) noexcept;
	static sf::Vector2f inverse(const sf::Vector2f& vec) noexcept;
	static sf::Vector2f angle(const sf::Vector2f& vec1, const sf::Vector2f& vec2) noexcept;
	static sf::Vector2f rotateVec(const sf::Vector2f& vec, const sf::Vector2f& angle) noexcept;
	static sf::Vector2f deflect(const sf::Vector2f& vec, sf::Vector2f normal) noexcept;
	static float clamp(float val, float min, float max);

	static float dot(const sf::Vector2f& vec1, const sf::Vector2f& vec2) noexcept;
	static float length(const sf::Vector2f& vec) noexcept;
	static sf::Vector2f normal(const sf::Vector2f& vec) noexcept;
	static sf::Vector2f align(const sf::Vector2f& vec1, const sf::Vector2f& vec2) noexcept;

	static sf::Color setHue(float hue);
};
#endif