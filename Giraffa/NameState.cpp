#include "Global.h"
#include "Render.h"
#include "Input.h"
#include "AudioManager.h"
#include "GameState.h"
#include "NameState.h"

NameState::NameState(Render * render, Input* input, AudioManager* audio)
{
	render_ = render;
	input_ = input;
	audio_ = audio;
}

NameState::~NameState()
{
}

void NameState::enter()
{
	userName_ = "";
}

bool NameState::update(float deltaTime)
{
	if (input_->getAnyKeyPressed())
	{
		if (!input_->getKey(sf::Keyboard::BackSpace))
		{
			char c = input_->getCurrentChar();
			if ((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9'))
				userName_ += c;
		}
		else
			userName_ = userName_.substr(0, userName_.length() - 1);
	}

	if (input_->getKeyPressed(sf::Keyboard::Return))
		if (userName_ != "")
			return false;

	return true;
}

void NameState::draw()
{
	render_->drawTextCenter(WIDTH / 2.0f, HEIGHT / 2.0f, userName_);
}

States NameState::getState()
{
	return STATE_NAME;
}

State * NameState::getNextState()
{
	return new GameState(render_, input_, 0, audio_);
}

void NameState::exit()
{
	// Nothing.
}