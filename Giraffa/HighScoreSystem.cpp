#include <fstream>
#include <algorithm>
#include <sstream>
#include "HighScoreSystem.h"

HighScoreSystem::HighScoreSystem()
{
	std::ifstream file("assets/highscore.hs");

	if (file.is_open())
	{
		while (!file.eof())
		{
			Entry tmp;
			file >> tmp.score_;
			std::getline(file, tmp.name_);

			entries_.push_back(tmp);
		}
	}
}

HighScoreSystem::~HighScoreSystem()
{
}

void HighScoreSystem::addEntry(int score, std::string name)
{
	entries_.push_back({ score, name });
	std::sort(entries_.begin(), entries_.end(), SortEntries());
}

int HighScoreSystem::getEntryScore(int place)
{
	if (place < 0) return -1;
	if (place > entries_.size()) return -1;

	return entries_[place].score_;
}

const std::string& HighScoreSystem::getEntryName(int place) const
{
	if (place < 0) return "";
	if (place > entries_.size()) return "";

	return entries_[place].name_;
}

int HighScoreSystem::findEntry(std::string name)
{
	for (int i = 0; i < entries_.size(); i++)
	{
		if (entries_[i].name_ == name)
			return i;
	}

	return -1;
}