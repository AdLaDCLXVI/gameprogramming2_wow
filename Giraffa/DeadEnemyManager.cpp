#include "DeadEnemy.h"
#include "DeadEnemyTwo.h"
#include "DeadEnemyThree.h"
#include "DeadEnemyManager.h"

DeadEnemyManager::DeadEnemyManager(Render* pRender)
{
	mRender = pRender;
	enemyOne_ = mRender->addTexture("assets/textures/EnemyOneDeath.png");
	enemyTwo_ = mRender->addTexture("assets/textures/EnemyTwoDeath.png");
	enemyThree_ = mRender->addTexture("assets/textures/EnemyThreeDeath.png");
	playerTexture_ = mRender->addTexture("assets/textures/giraffdeathsprite.png");
	player_ = nullptr;
	
}

DeadEnemyManager::~DeadEnemyManager()
{
	delete player_;
	mRender->deleteTexture(playerTexture_);
	mRender->deleteTexture(enemyOne_);
	mRender->deleteTexture(enemyTwo_);
	mRender->deleteTexture(enemyThree_);
}

void DeadEnemyManager::AddDeadEnemy(float type, sf::Vector2f position)
{
	if (type == 1)
	{
		DeadEnemyListOne.push_back(new DeadEnemy(enemyOne_, position));
	}
	else if (type == 2)
	{
		DeadEnemyListTwo.push_back(new DeadEnemyTwo(enemyTwo_, position));
	}
	else if (type == 3)
	{
		DeadEnemyListThree.push_back(new DeadEnemyThree(enemyThree_, position));
	}
	else if (type == 4)
	{
		if (player_ == nullptr)
		{
			player_ = new DeadPlayer(playerTexture_, position);
		}
	}

	
}
void DeadEnemyManager::Update(float pdeltaTime)
{
	{
		for (int i = 0; i < (int)DeadEnemyListOne.size(); i++)
		{
			DeadEnemyListOne[i]->Update(pdeltaTime);
			if (DeadEnemyListOne[i]->Destroy())
			{
				delete DeadEnemyListOne[i];
				DeadEnemyListOne.erase(DeadEnemyListOne.begin() + i);
			}
		}

	}
	{
		for (int i = 0; i < (int)DeadEnemyListTwo.size(); i++)
		{
			DeadEnemyListTwo[i]->Update(pdeltaTime);
			if (DeadEnemyListTwo[i]->Destroy())
			{
				delete DeadEnemyListTwo[i];
				DeadEnemyListTwo.erase(DeadEnemyListTwo.begin() + i);
			}
		}

	}
	{
		for (int i = 0; i < (int)DeadEnemyListThree.size(); i++)
		{
			DeadEnemyListThree[i]->Update(pdeltaTime);
			if (DeadEnemyListThree[i]->Destroy())
			{
				delete DeadEnemyListThree[i];
				DeadEnemyListThree.erase(DeadEnemyListThree.begin() + i);
			}
		}
	}
	if (player_ != nullptr)
	{
	player_->Update(pdeltaTime);
	
	{

	}
	}
}

void DeadEnemyManager::Draw()
{
	{
		auto it = DeadEnemyListOne.begin();
		while (it != DeadEnemyListOne.end())
		{
			mRender->renderToWindow()->draw(*(*it)->GetSprite());
			it++;
		}
	
	}
	{
		auto it = DeadEnemyListTwo.begin();
		while (it != DeadEnemyListTwo.end())
		{
			mRender->renderToWindow()->draw(*(*it)->GetSprite());
			it++;
		}

	}
	{
		auto it = DeadEnemyListThree.begin();
		while (it != DeadEnemyListThree.end())
		{
			mRender->renderToWindow()->draw(*(*it)->GetSprite());
			it++;
		}

	}
	if (player_ != nullptr)
	mRender->renderToWindow()->draw(*player_->GetSprite());
}

bool DeadEnemyManager::deadPlayer()
{
	if (player_ == nullptr)
		return true;
	else if (player_->Destroy())
		return true;
	return false;
}