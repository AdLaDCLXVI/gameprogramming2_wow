#include "NewMath.h"
#include "Shape.h"

Shape::Shape()
{
	pointCount_ = 0;
	angle_ = { 1.0f, 0.0f };
}

void Shape::setRect(int width, int height, float originX, float originY)
{
	addPoint(originX - width, originY - height);
	addPoint(originX + width, originY - height);
	addPoint(originX + width, originY + height);
	addPoint(originX - width, originY + height);
}

const sf::Vector2f & Shape::getPosition()
{
	return position_;
}

void Shape::setPosition(const sf::Vector2f& pos)
{
	position_ = pos;
}

void Shape::addPoint(float x, float y)
{
	if (pointCount_ < 10)
	{
		points_[pointCount_].x = x;
		points_[pointCount_].y = y;
		pointCount_++;
	}
}

void Shape::setPointPosition(int index, float x, float y)
{
	if (index < pointCount_)
	{
		points_[index].x = x;
		points_[index].y = y;
	}
}

sf::Vector2f Shape::getPoint(int index)
{
	index % 10;
	return NewMath::rotateVec(points_[index], angle_) + position_;
}

int Shape::pointCount()
{
	return pointCount_;
}

void Shape::rotate(float angle)
{
	angle_.x = cosf(angle * DTR);
	angle_.y = sinf(angle * DTR);
}

void Shape::rotate(sf::Vector2f angle)
{
	angle_ = angle;
}