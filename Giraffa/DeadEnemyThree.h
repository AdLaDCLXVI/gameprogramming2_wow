#pragma once
#include "SFML\Graphics.hpp"
#include "Animation.h"
class DeadEnemyThree
{
public:
	DeadEnemyThree(sf::Texture* texture, sf::Vector2f pos);
	~DeadEnemyThree();
	void Update(float deltaTime);
	bool Destroy();
	sf::Sprite* GetSprite();
private:
	bool IsAlive();
	sf::Vector2f position_;
	Animation* enemySprite_;
};