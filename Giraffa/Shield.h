#ifndef SHIELD_H
#define SHIELD_H
#include <SFML\Graphics.hpp>
#include "Circle.h"
#include "Animation.h"

class Render;
class EnemyManager;
class EnemyBulletManager;
class AsteroidManager;

class Shield
{
public:
	Shield(Render* render);
	~Shield();
	void update(float deltaTime, const sf::Vector2f& pos, 
		EnemyManager* manager1,
		AsteroidManager* manager2);
	void activate();
	void draw();
private:
	Render* render_;
	sf::Vector2f position_;
	sf::Texture* shieldTexture_;
	Animation shieldSprite_;
	Circle collisionShape_;

	bool active_;
	float timer_;
};
#endif