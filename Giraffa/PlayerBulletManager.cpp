#include "Global.h"
#include "NewMath.h"
#include "AsteroidManager.h"
#include "PlayerBulletManager.h"

PlayerBulletManager::PlayerBulletManager(Render* render)
{
	render_ = render;
	playerBulletSprite_.setTexture(*render_->getInGameTexture());
	playerBulletSprite_.setTextureRect({ 198, 223, 83, 86 });
	playerBulletSprite_.setOrigin({ 42.0f, 43.0f });
	powerupLazerTexture_ = render_->addTexture("assets/textures/spr_lazer.png");
	powerupLazerSprite_.setTexture(*powerupLazerTexture_);
	powerupLazerSprite_.setColor(sf::Color(255, 20, 25));

	collisionShape_.setRadius(12.0f);

	lastDeleted_ = 0;
	powerupTimer_ = 0.0f;
	powerupState_ = POWERUP_NONE;
	powerupLazerActive_ = false;
}

PlayerBulletManager::~PlayerBulletManager()
{
	render_->deleteTexture(powerupLazerTexture_);
}

void PlayerBulletManager::update(float deltaTime, EnemyManager* manager1, AsteroidManager* manager2)
{
	if (powerupTimer_ > 0.0f)
	{
		powerupTimer_ -= deltaTime;
	}
	else
	{
		powerupTimer_ = 0.0f;
		powerupState_ = POWERUP_NONE;
	}

	HitInfo hitInfo;
	for (int i = 0; i < 20; i++)
	{
		auto& o = playerBulletList_[i];
		if (o.isAlive())
		{
			o.update(deltaTime);

			collisionShape_.setPosition(o.getPosition());
			if (manager1->collidePlayerBullet(&collisionShape_))
				o.kill();
			else if (manager2->collideCircle(&collisionShape_, hitInfo))
				o.deflect(hitInfo.mtvVec_);

			
		}
	}

	if (powerupLazerActive_)
	{
		manager2->collideShape(powerupLazer_.getShape(), HitInfo());
		manager1->collideLazer(powerupLazer_.getShape());
	}
}

void PlayerBulletManager::addBullet(const sf::Vector2f& pos, const sf::Vector2f& vel)
{
	sf::Vector2f v;

	switch (powerupState_)
	{
	case POWERUP_NONE :
		v = NewMath::angle(pos, vel) * 80.0f;
		addBullet(pos, v, false);
		break;
	case POWERUP_LAZER :
		powerupLazer_.update(pos, vel);
		powerupLazerActive_ = true;
		break;
	case POWERUP_SHOTGUN :
		v = NewMath::angle(pos, vel);
		for (int i = 0; i < 6; i++)
			addBullet(pos, (v + NewMath::randVec(0.0f, 0.5f)) * 80.0f, true);
		render_->shakeScreen(5);
		break;
	default:
		break;
	}
}

void PlayerBulletManager::setPowerup(PowerupStates powerup)
{
	printf("Set Powerup\n");
	powerupState_ = powerup;

	switch (powerup)
	{
	case POWERUP_LAZER :
		powerupTimer_ = 600.0f;
		break;
	case POWERUP_SHOTGUN :
		powerupTimer_ = 100.0f;
		break;
	default:
		break;
	}
}

void PlayerBulletManager::draw()
{
	for (int i = 0; i < 20; i++)
	{
		auto& o = playerBulletList_[i];
		if (o.isAlive())
		{
			playerBulletSprite_.setPosition(o.getPosition());
			playerBulletSprite_.setRotation(NewMath::randNormal() * 360.0f);
			render_->renderToWindow()->draw(playerBulletSprite_, sf::BlendAdd);
		}
	}

	if (powerupLazerActive_)
	{
		powerupLazerSprite_.setRotation(powerupLazer_.getAngle());

		powerupLazerSprite_.setTextureRect({ 0, 0, 16, 32 });
		powerupLazerSprite_.setOrigin({ 16.0f, 16.0f });
		powerupLazerSprite_.setPosition(powerupLazer_.getPosition());
		render_->renderToWindow()->draw(powerupLazerSprite_, sf::BlendAdd);

		powerupLazerSprite_.setTextureRect({ 16, 0, 16, 32 });
		powerupLazerSprite_.setOrigin({ 0.0f, 16.0f });
		powerupLazerSprite_.setPosition(powerupLazer_.getTarget());
		render_->renderToWindow()->draw(powerupLazerSprite_, sf::BlendAdd);

		powerupLazerSprite_.setTextureRect({ 15, 0, 2, 32 });
		powerupLazerSprite_.setScale(powerupLazer_.getScale() / 2.0f, 1.0f);
		powerupLazerSprite_.setPosition(powerupLazer_.getPosition());
		render_->renderToWindow()->draw(powerupLazerSprite_);
		powerupLazerSprite_.setScale(1.0f, 1.0f);

		powerupLazerActive_ = false;
	}
}

void PlayerBulletManager::clean()
{
}

bool PlayerBulletManager::findEmptySlot()
{
	for (int i = lastDeleted_; i < 20; i++)
		if (!playerBulletList_[i].isAlive())
		{
			lastDeleted_ = i;
			return true;
		}
	
	for (int i = 0; i < lastDeleted_; i++)
		if (!playerBulletList_[i].isAlive())
		{
			lastDeleted_ = i;
			return true;
		}

	return false;
}

void PlayerBulletManager::addBullet(const sf::Vector2f & pos, const sf::Vector2f & vel, bool electric)
{
	if (findEmptySlot())
	{
		if (!electric)
			playerBulletList_[lastDeleted_].init(pos, vel);
		else
		{
			playerBulletList_[lastDeleted_].init(pos, vel);
			playerBulletList_[lastDeleted_].setElectric();
		}
	}
}
