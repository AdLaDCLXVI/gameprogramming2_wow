#ifndef _SLIDERCHECK_H_
#define _SLIDERCHECK_H_
#include <SFML\Graphics.hpp>
#include "Render.h"

class SliderCheck
{
public:
	SliderCheck() { grab_ = false; };
	SliderCheck(float x, float y, Render* render, float startX);
	~SliderCheck() {};
	void update(int mouseX);
	float getValue();
	void pressed(const sf::Vector2f& mousePos);
	void reset();
	void draw();

private:
	int width_;
	float startX_;

	sf::Vector2f position_;
	sf::Sprite slideButton_;

	Render* render_;
	sf::Sprite slideBar_;


	bool grab_;
};
#endif