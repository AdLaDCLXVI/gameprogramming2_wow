#ifndef ExitState_H
#define ExitState_H
#include <SFML\Graphics.hpp>
#include "State.h"

class Render;

class ExitState : public State
{
public:
	ExitState(Render* render);
	~ExitState() {};
	void enter() {};
	void exit() {};
	bool update(float deltaTime);
	void draw();
	States getState();
	State* getNextState();
private:
	float time_;

	Render* render_;
	Input* input_;
};
#endif