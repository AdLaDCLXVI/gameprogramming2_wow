#include "Render.h"
#include "EnemyManager.h"
#include "EnemyBulletManager.h"
#include "AsteroidManager.h"
#include "Shield.h"

Shield::Shield(Render* render)
{
	render_ = render;

	shieldTexture_ = render_->addTexture("assets/textures/spr_shield.png");
	shieldSprite_.init(10, 0.75f, 256, 256);
	shieldSprite_.getSprite()->setTexture(*shieldTexture_);
	shieldSprite_.getSprite()->setOrigin({ 128.0f, 128.0f });

	collisionShape_.setRadius(128.0f);

	active_ = false;
	timer_ = 0.0f;
}

Shield::~Shield()
{
	render_->deleteTexture(shieldTexture_);
}

void Shield::update(float deltaTime, const sf::Vector2f& pos, 
	EnemyManager* manager1,
	AsteroidManager* manager2)
{
	if (active_)
	{
		if (timer_ > 0.0f)
			timer_ -= deltaTime;
		else
		{
			timer_ = 0.0f;
			active_ = false;
		}

		collisionShape_.setPosition(pos);

		manager1->collideShield(&collisionShape_);
		manager2->collideCircle(&collisionShape_, HitInfo());

		shieldSprite_.update(deltaTime);
		shieldSprite_.getSprite()->setPosition(pos);
	}
}

void Shield::activate()
{
	active_ = true;
	timer_ = 80.0f;
}

void Shield::draw()
{
	if (active_)
		render_->renderToWindow()->draw(*shieldSprite_.getSprite());
}