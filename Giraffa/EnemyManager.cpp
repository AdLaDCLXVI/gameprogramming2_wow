#include <SFML\Graphics.hpp>
#include "CollisionManager.h"
#include "NewMath.h"
#include "EnemyBulletManager.h"
#include "DeadEnemyManager.h"
#include "Global.h"
#include "EnemyManager.h"

#include "PickupManager.h"

EnemyManager::EnemyManager(Render* render, PickupManager* pickupManager, AudioManager* audio)
{
	render_ = render;
	pickupManager_ = pickupManager;
	audioManager_ = audio;

	active_ = 0;
	empty_ = false;
	enemyOneList_ = nullptr;
	enemyTwoList_ = nullptr;
	enemyThreeList_ = nullptr;

	enemyBulletManager_ = new EnemyBulletManager(render);
	deadEnemyManager_ = new DeadEnemyManager(render);
	enemyOneTexture_ = render_->addTexture("assets/textures/spr_enemyOne_flying.png");
	enemyTwoTexture_ = render_->addTexture("assets/textures/spr_enemyTwo_flying.png");
	enemyThreeTexture_ = render_->addTexture("assets/textures/spr_enemyThree_flying.png");
	collisionShapeOne_.setRect(64, 64, 0.0f, 0.0f);
	collisionShapeTwo_.setRect(64, 64, 0.0f, 0.0f);
	collisionShapeThree_.setRect(64, 64, 0.0f, 0.0f);

	enemyOneNumber_ = 0;
	enemyTwoNumber_ = 0;
	enemyThreeNumber_ = 0;
	score_ = 0;
	playerHit_ = false;
	Boss = new Khan();
	BossDying = false;

	audioManager_->SetGlobalMusic("assets/music/BossTheme.ogg");
	Shooting = audioManager_->AddSound("assets/music/BossRing.wav");
	soundPlayer_.setBuffer(Shooting->mClip);
	Laser = audioManager_->AddSound("assets/music/Bosslaser.wav");
	Spraying = audioManager_->AddSound("assets/music/BossSpray.wav");
	EnemyDeath1 = audioManager_->AddSound("assets/sound/DeathSound1.wav");
	EnemyDeath2 = audioManager_->AddSound("assets/sound/DeathSound2.wav");
	EnemyShoot = audioManager_->AddSound("assets/sound/EnemyAttack1.wav");
	GiraffDead = audioManager_->AddSound("assets/sound/GiraffDead.wav");
	soundPlayer_.setVolume(audioManager_->sfxVolume_);
}

EnemyManager::~EnemyManager()
{
	delete enemyBulletManager_;
	delete deadEnemyManager_;
	delete[] enemyOneList_;
	delete[] enemyTwoList_;
	delete[] enemyThreeList_;
	render_->deleteTexture(enemyOneTexture_);
	render_->deleteTexture(enemyTwoTexture_);
	render_->deleteTexture(enemyThreeTexture_);

	audioManager_->DeleteSound(Laser);
	audioManager_->DeleteSound(Spraying);
	audioManager_->DeleteSound(Shooting);
	audioManager_->DeleteSound(EnemyDeath1);
	audioManager_->DeleteSound(EnemyDeath2);
	audioManager_->DeleteSound(EnemyShoot);
}

void EnemyManager::init(std::string path)
{
	if (enemyOneList_ != nullptr)
	{
		delete[] enemyOneList_;
		delete[] enemyTwoList_;
		delete[] enemyThreeList_;
	}

	std::ifstream file(path);

	if (file.is_open())
	{
		file >> enemyOneNumber_;
		file >> enemyTwoNumber_;
		file >> enemyThreeNumber_;
		enemyOneList_ = new EnemyOne[enemyOneNumber_];
		enemyTwoList_ = new EnemyTwo[enemyTwoNumber_];
		enemyThreeList_ = new EnemyThree[enemyThreeNumber_];

		// x, y, xVel, yVel
		float x, y, xv, yv;
		int type, i1 = 0, i2 = 0, i3 = 0;
		while (!file.eof())
		{
			file >> type;
			file >> x;
			file >> y;

			switch (type)
			{
				case 1 :
					file >> xv;
					file >> yv;
					enemyOneList_[i1++].init({ x, y }, { xv, yv }, enemyOneTexture_);
					break;
				case 2 :
					file >> xv;
					file >> yv;
					enemyTwoList_[i2++].init({ x, y }, { xv, yv }, enemyTwoTexture_);
					break;
				case 3:
					file >> xv;
					file >> yv;
					enemyThreeList_[i3++].init({ x, y }, { xv, yv }, enemyThreeTexture_);
					break;
			default:
				break;
			}
		}

		file.close();
	}
}

void EnemyManager::update(float deltaTime)
{
	empty_ = true;
	for (int i = 0; i < enemyOneNumber_; i++)
	{
		auto& o = enemyOneList_[i];
		if (o.isAlive())
		{
			empty_ = false;
			o.update(deltaTime);
			if (o.shootRequest()){
				soundPlayer_.setBuffer(Spraying->mClip);
				audioManager_->PlaySoundRand(&soundPlayer_);
				enemyBulletManager_->addBullet(o.getPosition() + sf::Vector2f(-11.0f, 13.0f), 180.0f, 70.0f);
			}
		}
	}

	for (int i = 0; i < enemyTwoNumber_; i++)
	{
		auto& o = enemyTwoList_[i];
		if (o.isAlive())
		{
			empty_ = false;
			o.update(deltaTime);
			if (o.shootRequest()){
				soundPlayer_.setBuffer(EnemyShoot->mClip);
				audioManager_->PlaySoundRand(&soundPlayer_);
				enemyBulletManager_->addBullet(o.getPosition() + sf::Vector2f(-11.0f, 13.0f), 180.0f, 70.0f);
			}
		}
	}

	for (int i = 0; i < enemyThreeNumber_; i++)
	{
		auto& o = enemyThreeList_[i];
		if (o.isAlive())
		{
			empty_ = false;
			o.update(deltaTime);
			if (o.shootRequest())
			{
				soundPlayer_.setBuffer(EnemyShoot->mClip);
				audioManager_->PlaySoundRand(&soundPlayer_);
				enemyBulletManager_->addBullet(o.getPosition() + sf::Vector2f(-11.0f, 13.0f), 140.0f, 70.0f);
				enemyBulletManager_->addBullet(o.getPosition() + sf::Vector2f(-11.0f, 13.0f), 180.0f, 70.0f);
				enemyBulletManager_->addBullet(o.getPosition() + sf::Vector2f(-11.0f, 13.0f), 220.0f, 70.0f);
			}
		}
	}

	if (Boss->GetState() != notReady)
	{
		if(Boss->isAlive())
		{

			
			if (Boss->Ready())
			{
				audioManager_->SetGlobalMusic("assets/music/Theme.ogg");
			}
			Boss->Update(deltaTime);
			if (Boss->GetState() == Passive) soundPlayer_.pause();
			if (Boss->shootRequest())
			{
				if (Boss->GetState() == Nr1)
				{
					for (int i = 0; i < 360; i += 360 / 12)
					{
						enemyBulletManager_->addBullet(Boss->getPosition() + sf::Vector2f( 120, 120) , i + Boss->GetTargetOne(), 30.0f);
					}
					render_->shakeScreen(10);
					soundPlayer_.setBuffer(Shooting->mClip);
					soundPlayer_.play();
					
				}
				else if (Boss->GetState() == Nr2)
				{
					enemyBulletManager_->addBullet(Boss->getPosition() + sf::Vector2f( 50.0f, 70.0f), Boss->GetTargetTwo(), 200);
					enemyBulletManager_->addBullet(Boss->getPosition() + sf::Vector2f( 195.0f, 70.0f), Boss->GetTargetOne(), 200);
					if (soundPlayer_.getBuffer() != &Laser->mClip)
					{
						soundPlayer_.setBuffer(Laser->mClip);
						soundPlayer_.play();
					}
				}
				else if(Boss->GetState() == Pre3)
				{
					float tmp = rand() % 30;
					if (rand() % 2 == 0)
						tmp *= -1;
					enemyBulletManager_->addBullet(Boss->getPosition() + sf::Vector2f(100.0f, -40.0f), 270 + tmp, 50);
					render_->shakeScreen(10); 
					soundPlayer_.setBuffer(Laser->mClip);
					soundPlayer_.play();
				}
				
				else{
					enemyBulletManager_->addBullet(sf::Vector2f( rand() % WIDTH , 0), 90, 20);
					soundPlayer_.stop();
				}
			}
			else if (Boss->GetState() == Dead)
			{
				score_ += 10;
				if (!BossDying)
				{
					audioManager_->SetGlobalMusic("assets/music/MenuMusik.wav");
					BossDying = true;
				}
			}
		}
	}

	enemyBulletManager_->update(deltaTime);
	deadEnemyManager_->Update(deltaTime);
	/*if (enemyBulletManager_->GameOver() || collidePlayerBullet(player->getShape()))
	{
		if (playerHit_ != true)
		{
			soundPlayer_.setBuffer(GiraffDead->mClip);
			soundPlayer_.play();
			playerHit_ = true;
			deadEnemyManager_->AddDeadEnemy(4, player->getPosition());
			audioManager_->SetGlobalMusic("assets/sound/GameOver.wav");
		}
	}*/
}

void EnemyManager::activate(bool state)
{
	active_ = state;
}

bool EnemyManager::isActivated()
{
	return active_;
}

void EnemyManager::draw()
{
	for (int i = 0; i < enemyOneNumber_; i++)
	{
		auto& o = enemyOneList_[i];
		if (o.isAlive())
			render_->renderToWindow()->draw(*o.getSprite()->getSprite());
	}

	for (int i = 0; i < enemyTwoNumber_; i++)
	{
		auto& o = enemyTwoList_[i];
		if (o.isAlive())
			render_->renderToWindow()->draw(*o.getSprite()->getSprite());
	}

	for (int i = 0; i < enemyThreeNumber_; i++)
	{
		auto& o = enemyThreeList_[i];
		if (o.isAlive())
			render_->renderToWindow()->draw(*o.getSprite());
	}

	if (Boss->GetState() != notReady)
	{
		if (Boss->isAlive())

		render_->renderToWindow()->draw(*Boss->getTentacleSprite());
		render_->renderToWindow()->draw(Boss->getEyesSprite());
		render_->renderToWindow()->draw(*Boss->getSprite());
		
	}

	enemyBulletManager_->draw();
	deadEnemyManager_->Draw();
}


bool EnemyManager::collidePlayerBullet(Circle * circle)
{
	sf::Vector2f coord;
	if (collideCircle(circle, coord))
	{
		pickupManager_->addPickup(coord);
		return true;
	}


	if (Boss != nullptr)
	{
		if (Boss->isAlive())
			if (CollisionManager::collideShapeCir(Boss->GetShape(), circle))
			{
				if (Boss->GetState() != Start)
				{
					Boss->HitBoss(1);
					render_->shakeScreen(4);
				}
				return true;
			}
	}

	return false;
}

bool EnemyManager::collidePlayer(Shape* shape)
{
	if (collideShape(shape))
		return true;
	if (enemyBulletManager_->collideShape(shape))
		return true;

	if (Boss->GetState() != notReady )
	{
		if (CollisionManager::collideShape(Boss->GetShape(), shape))
			return true;
	}

	return false;
}


void EnemyManager::collideLazer(Shape* shape)
{
	collideShape(shape);
	enemyBulletManager_->collideShape(shape);
}
bool EnemyManager::isEmpty()
{
	return empty_;
}

bool EnemyManager::collideCircle(Circle* circle, sf::Vector2f& coord)
{
	for (int i = 0; i < enemyOneNumber_; i++)
	{
		auto& o = enemyOneList_[i];
		if (o.isAlive() && o.isInView())
		{
			collisionShapeOne_.setPosition(o.getPosition());
			if(CollisionManager::collideShapeCir(&collisionShapeOne_, circle))
			{
				score_ += 100;
				o.kill();
				coord = o.getPosition();
				render_->shakeScreen(5);
				deadEnemyManager_->AddDeadEnemy(1, o.getPosition());
				if (rand() % 2 == 0)
					soundPlayer_.setBuffer(EnemyDeath1->mClip);
				else
					soundPlayer_.setBuffer(EnemyDeath2->mClip);

				soundPlayer_.play();
				return true;
			}
		}
	}

	for (int i = 0; i < enemyTwoNumber_; i++)
	{
		auto& o = enemyTwoList_[i];
		if (o.isAlive())
		{
			collisionShapeTwo_.setPosition(o.getPosition());
			if (CollisionManager::collideShapeCir(&collisionShapeTwo_, circle))
			{
				score_ += 200;
				deadEnemyManager_->AddDeadEnemy(2, o.getPosition());
				o.kill();
				render_->shakeScreen(6);

				coord = o.getPosition();
				if (rand() % 2 == 0)
					soundPlayer_.setBuffer(EnemyDeath1->mClip);
				else
					soundPlayer_.setBuffer(EnemyDeath2->mClip);

				soundPlayer_.play();
				return true;
			}
		}
	}

	for (int i = 0; i < enemyThreeNumber_; i++)
	{
		auto& o = enemyThreeList_[i];
		if (o.isAlive())
		{
			collisionShapeThree_.setPosition(o.getPosition());
			if (CollisionManager::collideShapeCir(&collisionShapeThree_, circle))
			{
				score_ += 300; 
				coord = o.getPosition();
				deadEnemyManager_->AddDeadEnemy(3, o.getPosition());
				o.kill();
				render_->shakeScreen(8);
				if (rand() % 2 == 0)
					soundPlayer_.setBuffer(EnemyDeath1->mClip);
				else
					soundPlayer_.setBuffer(EnemyDeath2->mClip);

				soundPlayer_.play();
				return true;
			}
		}
	}

	if (Boss->GetState() != notReady)
	{
		if (Boss->isAlive())
		{
			if (CollisionManager::collideShapeCir(Boss->GetShape(), circle))
			{
				if (Boss->GetState() != Start)
				{
					Boss->HitBoss(1);
					render_->shakeScreen(4);
				}
				return true;
			}
		}
	}

	return false;
}

bool EnemyManager::GameOver()
{
	if ( playerHit_)
	{

		if (deadEnemyManager_->deadPlayer())
			return true;

		return false;
	}
	else if (!Boss->isAlive() && Boss->GetState() == Dead && Boss->GetState() != notReady)
	{

		return true;
	}
	return false;
}

int EnemyManager::GetScore()
{
	return score_;
}

bool EnemyManager::PlayerLose()
{
	return playerHit_;
}
bool EnemyManager::collideShape(Shape* shape)
{
	for (int i = 0; i < enemyOneNumber_; i++)
	{
		auto& o = enemyOneList_[i];
		if (o.isAlive() && o.isInView())
		{
			collisionShapeOne_.setPosition(o.getPosition());
			if (CollisionManager::collideShape(&collisionShapeOne_, shape))
			{
				//score_ += 100;
				o.kill();
				render_->shakeScreen(5);
				deadEnemyManager_->AddDeadEnemy(1, o.getPosition());
				if (rand() % 2 == 0)
					soundPlayer_.setBuffer(EnemyDeath1->mClip);
				else
					soundPlayer_.setBuffer(EnemyDeath2->mClip);

				soundPlayer_.play();
				return true;
			}
		}
	}

	for (int i = 0; i < enemyTwoNumber_; i++)
	{
		auto& o = enemyTwoList_[i];
		if (o.isAlive())
		{
			collisionShapeTwo_.setPosition(o.getPosition());
			if (CollisionManager::collideShape(&collisionShapeTwo_, shape))
			{
				//score_ += 200;
				deadEnemyManager_->AddDeadEnemy(2, o.getPosition());
				o.kill();
				render_->shakeScreen(6);
				if (rand() % 2 == 0)
					soundPlayer_.setBuffer(EnemyDeath1->mClip);
				else
					soundPlayer_.setBuffer(EnemyDeath2->mClip);

				soundPlayer_.play();
				return true;
			}
		}
	}

	for (int i = 0; i < enemyThreeNumber_; i++)
	{
		auto& o = enemyThreeList_[i];
		if (o.isAlive())
		{
			collisionShapeTwo_.setPosition(o.getPosition());
			if (CollisionManager::collideShape(&collisionShapeTwo_, shape))
			{
				//score_ += 300;
				deadEnemyManager_->AddDeadEnemy(3, o.getPosition());
				o.kill();
				render_->shakeScreen(8);
				if (rand() % 2 == 0)
					soundPlayer_.setBuffer(EnemyDeath1->mClip);
				else
					soundPlayer_.setBuffer(EnemyDeath2->mClip);

				soundPlayer_.play();
				return true;
			}
		}
	}

	return false;
}
void EnemyManager::collideShield(Circle* circle)
{
	enemyBulletManager_->collideCircle(circle);
}
void EnemyManager::clean()
{
	{
		for (int i = 0; i < enemyOneNumber_; i++)
		{
			auto& o = enemyOneList_[i];
			if (o.isAlive() && o.getPosition().x < WIDTH)
			{
				{
					score_ += 50;
					deadEnemyManager_->AddDeadEnemy(1, o.getPosition());
					o.kill();
					if (rand() % 2 == 0)
						soundPlayer_.setBuffer(EnemyDeath1->mClip);
					else
						soundPlayer_.setBuffer(EnemyDeath2->mClip);

					soundPlayer_.play();
				}
			}
		}
	}
	{
		for (int i = 0; i < enemyTwoNumber_; i++)
		{
			auto& o = enemyTwoList_[i];
			if (o.isAlive())
			{
				{
					score_ += 100;
					deadEnemyManager_->AddDeadEnemy(2, o.getPosition());
					o.kill();
					if (rand() % 2 == 0)
						soundPlayer_.setBuffer(EnemyDeath1->mClip);
					else
						soundPlayer_.setBuffer(EnemyDeath2->mClip);

					soundPlayer_.play();
				}
			}
		}
	}
	{
	for (int i = 0; i < enemyThreeNumber_; i++)
	{
		auto& o = enemyThreeList_[i];
		if (o.isAlive())
		{
			{
				score_ += 150;
				deadEnemyManager_->AddDeadEnemy(3, o.getPosition());
				o.kill();
				if (rand() % 2 == 0)
					soundPlayer_.setBuffer(EnemyDeath1->mClip);
				else
					soundPlayer_.setBuffer(EnemyDeath2->mClip);

				soundPlayer_.play();
			}
		}
	}
	}
	enemyBulletManager_->clean();
}

void EnemyManager::activateBoss()
{
	Boss->init({ WIDTH + 700, 250 }, { -5, 3 });
	Boss->CreateAnimation(render_);
}

void EnemyManager::KillPlayer(sf::Vector2f pos)
{
	deadEnemyManager_->AddDeadEnemy(4, pos);
}

//if (CollisionManager::collideShape(Boss->GetShape(), player->getShape()))
//{
//	if (playerHit_ != true)
//	{
//		soundPlayer_.setBuffer(GiraffDead->mClip);
//		soundPlayer_.play();
//		playerHit_ = true;
//		deadEnemyManager_->AddDeadEnemy(4, player->getPosition());
//		audioManager_->SetGlobalMusic("assets/sound/GameOver.wav");
//	}
//
//
//}