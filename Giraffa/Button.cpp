#include "CollisionManager.h"
#include "Button.h"

Button::Button(const sf::Vector2f& pos, sf::Texture* texture, const sf::FloatRect& region)
{
	buttonSprite_.setPosition(pos);
	buttonSprite_.setTexture(*texture);
	buttonSprite_.setTextureRect((sf::IntRect)region);
	buttonSprite_.setOrigin( region.width / 2.0f, region.height / 2.0f);

	pressed_ = false;
}

void Button::update(const sf::Vector2f& mousePos)
{
	if (CollisionManager::collideRectPoint(mousePos, buttonSprite_.getGlobalBounds()))
		buttonSprite_.setScale({ 1.2f, 1.2f});
	else
		buttonSprite_.setScale({ 1.0f, 1.0f });
}

void Button::setPressed(const sf::Vector2f& mousePos)
{
	if (CollisionManager::collideRectPoint(mousePos, buttonSprite_.getGlobalBounds()))
		pressed_ = true;
}

bool Button::isPressed()
{
	return pressed_;
}

bool Button::reset()
{
	if (pressed_)
		return true;
	return false;
}

sf::Sprite* Button::getSprite()
{
	return &buttonSprite_;
}
void Button::unPress()
{
	pressed_ = false;
}