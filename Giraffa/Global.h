#ifndef GLOBAL_H
#define GLOBAL_H

#define WIDTH 1280
#define HEIGHT 720
#define HEIGHT_HUD 100
#define MS_PER_UPDATE (1.0 / 80.0)
#endif