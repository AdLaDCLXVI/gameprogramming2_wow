#include "NewMath.h"
#include "CollisionManager.h"

bool CollisionManager::collideShape(Shape* shapeA, Shape* shapeB, HitInfo& hitInfo)
{
	sf::Vector2f edge;
	sf::Vector2f normal;
	hitInfo.mtvMag_ = 99999.0f;

	for (int i = 0; i < shapeA->pointCount(); i++)
	{
		edge = shapeA->getPoint(i);
		normal = NewMath::normalize(NewMath::normal(shapeA->getPoint(i + 1) - edge));

		float minA, maxA, minB, maxB;
		projectShape(edge, normal, shapeA, minA, maxA);
		projectShape(edge, normal, shapeB, minB, maxB);

		if (overlap(minA, maxA, minB, maxB) > 0)
			return false;
		else 
		{
			float interval = getOverlap(minA, maxA, minB, maxB);

			if (interval < hitInfo.mtvMag_) {
				hitInfo.mtvMag_ = interval;
				hitInfo.mtvVec_ = normal;
			}
		}
	}

	for (int i = 0; i < shapeB->pointCount(); i++)
	{
		edge = shapeB->getPoint(i);
		normal = NewMath::normalize(NewMath::normal(shapeB->getPoint(i + 1) - edge));
		float mtv = 99999.0f;

		float minA, maxA, minB, maxB;
		projectShape(edge, normal, shapeA, minA, maxA);
		projectShape(edge, normal, shapeB, minB, maxB);

		if (overlap(minA, maxA, minB, maxB) > 0)
			return false;
		else
		{
			float interval = getOverlap(minA, maxA, minB, maxB);

			if (interval < hitInfo.mtvMag_) {
				hitInfo.mtvMag_ = interval;
				hitInfo.mtvVec_ = normal;
			}
		}
	}

	float dir = NewMath::dot(NewMath::align(shapeA->getPosition(), shapeB->getPosition()), hitInfo.mtvVec_);
	if (dir > 0)
		hitInfo.mtvVec_ *= -1.0f;

	return true;
}

bool CollisionManager::collideShapeCir(Shape* shape, Circle* circle, HitInfo& hitInfo)
{
	int edgeCountA = shape->pointCount();
	sf::Vector2f edge;
	sf::Vector2f normal;
	hitInfo.mtvMag_ = 99999.0f;

	for (int i = 0; i < edgeCountA; i++) {
		edge = shape->getPoint(i);
		normal = NewMath::normalize(NewMath::normal(shape->getPoint(i + 1) - edge));

		float minA, maxA;
		projectShape(edge, normal, shape, minA, maxA);
		sf::Vector2f d = normal * circle->getRadius() - circle->getPosition();
		float minB = NewMath::dot(NewMath::normalize(d - edge), normal);
		d = normal * circle->getRadius() + circle->getPosition();
		float maxB = NewMath::dot(NewMath::normalize(d - edge), normal);

		if (overlap(minA, maxA, minB, maxB) > 0)
			return false;
		else
		{
			float interval = getOverlap(minA, maxA, minB, maxB);

			if (interval < hitInfo.mtvMag_) {
				hitInfo.mtvMag_ = interval;
				hitInfo.mtvVec_ = normal;
			}
		}
	}

	for (int i = 0; i < edgeCountA; i++) {
		edge = NewMath::align(circle->getPosition(), shape->getPoint(i));
		normal = NewMath::normalize(edge);

		float minA, maxA, minB, maxB;
		projectShape(circle->getPosition(), normal, shape, minA, maxA);
		minB = -circle->getRadius();
		maxB = circle->getRadius();

		if (overlap(minA, maxA, minB, maxB) > 0)
			return false;
		else
		{
			float interval = getOverlap(minA, maxA, minB, maxB);

			if (interval < hitInfo.mtvMag_) {
				hitInfo.mtvMag_ = interval;
				hitInfo.mtvVec_ = normal;
			}
		}
	}

	float dir = NewMath::dot(NewMath::align(shape->getPosition(), circle->getPosition()), hitInfo.mtvVec_);
	if (dir > 0)
		hitInfo.mtvVec_ *= -1.0f;

	return true;
}

bool CollisionManager::collideShape(Shape* shapeA, Shape* shapeB)
{
	return collideShape(shapeA, shapeB, HitInfo());
}

bool CollisionManager::collideShapeCir(Shape* shape, Circle* circle)
{
	return collideShapeCir(shape, circle, HitInfo());
}

void CollisionManager::projectShape(const sf::Vector2f& axis, const sf::Vector2f& normal, Shape* shape, float& min, float& max)
{
	float proj = NewMath::dot(normal, shape->getPoint(0) - axis);
	min = proj;
	max = proj;

	for (int i = 0; i < shape->pointCount(); i++)
	{
		proj = NewMath::dot(normal, shape->getPoint(i) - axis);
		if (proj < min)
			min = proj;
		else if (proj > max)
			max = proj;
	}
}

float CollisionManager::overlap(float min1, float max1, float min2, float max2)
{
	if (min1 < max2)
		return min2 - max1;
	else
		return min1 - max2;
}

float CollisionManager::getOverlap(float min1, float max1, float min2, float max2)
{
	float min = (float)fmax(min1, min2);
	float max = (float)fmin(max1, max2);

	if (max > min)
		return max - min;
	else
		return min - max;
}

bool CollisionManager::collideRectPoint(const sf::Vector2f& pos, const sf::Vector2f& origin, float width, float height)
{
	if (pos.x > origin.x && pos.y > origin.y && pos.x < origin.x + width && pos.y < origin.y + height)
		return true;

	return false;
}

bool CollisionManager::collideRectPoint(const sf::Vector2f& pos, const sf::FloatRect& region)
{
	if (pos.x > region.left && pos.y > region.top
		&& pos.x < region.left + region.width
		&& pos.y < region.top + region.height)
		return true;

	return false;
}

bool CollisionManager::collideCirCir(Circle * circleA, Circle * circleB, HitInfo& hitInfo)
{
	float bias = circleA->getRadius() + circleB->getRadius();
	sf::Vector2f magnitude = circleB->getPosition() - circleA->getPosition();
	if (NewMath::length(magnitude) < bias)
	{
		hitInfo.mtvVec_ = NewMath::normalize(magnitude);
		hitInfo.mtvMag_ = NewMath::length(magnitude) - bias;
		return true;
	}
	return false;
}

bool CollisionManager::collideCirCir(Circle* circleA, Circle* circleB)
{
	return collideCirCir(circleA, circleB, HitInfo());
}
