#pragma once
#include <SFML\System\Vector2.hpp>
#include "Animation.h"

class RainbowAsteroid
{
public:
	RainbowAsteroid();
	~RainbowAsteroid() {};
	void init(const sf::Vector2f& pos, const sf::Vector2f& vel);
	void update(float deltaTime);
	const sf::Vector2f& getPosition() const;
	float getAngle();
	bool isAlive();
	void kill();
	bool activated();
private:
	sf::Vector2f posistion_;
	sf::Vector2f velocity_;

	char alive_;
	float angle_;
	bool active_;
};