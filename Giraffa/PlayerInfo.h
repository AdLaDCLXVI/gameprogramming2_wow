#pragma once
#include <sstream>

class PlayerInfo
{
public:
	PlayerInfo();
	~PlayerInfo();
	void setName(std::string name);
	std::string getName();
	void addScore(int value);
	int getScore();
	bool level1();
	bool level2();
	bool level3();
private:
	std::string name_;
	int score_;
	bool level1_;
	bool level2_;
	bool level3_;
};

