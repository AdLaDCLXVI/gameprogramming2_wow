#include "DeadAsteroid.h"

DeadAsteroid::DeadAsteroid()
{
	enemySprite_ = nullptr;

	alive_ = 0x2;
}

DeadAsteroid::~DeadAsteroid()
{
	delete enemySprite_;
	enemySprite_ = nullptr;
}

void DeadAsteroid::init(sf::Texture* texture, sf::Vector2f pos)
{
	position_ = pos;
	alive_ = 0x0;

	enemySprite_ = new Animation();
	enemySprite_->init(6, 2.0f, 128, 128);
	enemySprite_->getSprite()->setTexture(*texture);
	enemySprite_->getSprite()->setOrigin({ 64, 64 });
	enemySprite_->getSprite()->setPosition(position_);

}

void DeadAsteroid::Update(float deltaTime)
{
	enemySprite_->update(deltaTime);
	if (enemySprite_->animationEnd())
		alive_ = 0x2;
}

sf::Sprite* DeadAsteroid::GetSprite()
{
	return enemySprite_->getSprite();
}
void DeadAsteroid::kill()
{
	alive_ = 0x2;
}

bool DeadAsteroid::isAlive()
{
	return alive_ != 0x2;
}