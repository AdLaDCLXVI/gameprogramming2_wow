#ifndef PICKUPMANAGER_H
#define PICKUPMANAGER_H
#include "SFML\Graphics.hpp"
#include "Render.h"
#include "PowerupStates.h"
#include "Pickup.h"
#include "Circle.h"

class Player;

class PickupManager
{
public:
	PickupManager(Render* render);
	~PickupManager() {};
	void update(float deltaTime);
	void addPickup(const sf::Vector2f& pos);
	void draw();
	void collidePlayer(Shape* shape);
	PowerupStates getCurrentPickup();
	void empty();
	void drawHudPickup();
private:
	void addPickup(const sf::Vector2f& pos, const sf::Vector2f& vel);
	bool findEmptySlot();

	Render* render_;
	Pickup pickupList_[4];
	sf::Texture* pickupTexture_;
	sf::Sprite pickupLazerSprite_;
	sf::Sprite pickupShotgunSprite_;
	sf::Sprite pickupShieldSprite_;
	Circle collisionShape_;

	short lastDeleted_;
	PowerupStates currentPowerup_;
};
#endif