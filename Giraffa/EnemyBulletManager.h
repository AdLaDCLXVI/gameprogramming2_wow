#ifndef ENEMYBULLETMANAGER_H
#define ENEMYBULLETMANAGER_H
#include <SFML\Graphics.hpp>
#include "Render.h"
#include "EnemyBullet.h"
#include "Shape.h"
#include "Circle.h"

class EnemyBulletManager
{
public:
	EnemyBulletManager(Render* render);
	~EnemyBulletManager();
	void update(float deltaTime);
	void addBullet(const sf::Vector2f& pos, float angle, float speed);
	void draw();
	void collideCircle(Circle* circle);
	bool collideShape(Shape* shape);
	void clean();
private:
	bool findEmptySlot();

	Render* render_;
	EnemyBullet enemyBulletList_[40];
	sf::Texture* enemyBulletTexture_;
	sf::Sprite enemyBulletSprite_;
	Shape collisionShape_;
	int lastDeleted_;
};
#endif