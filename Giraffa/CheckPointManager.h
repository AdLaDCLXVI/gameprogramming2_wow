#ifndef CHECKPOINTMANAGER_H
#define CHECKPOINTMANAGER_H
#include <SFML\Graphics.hpp>
#include "CheckPoint.h"
#include "Animation.h"

class Render;
class Player;

class CheckPointManager
{
public:
	CheckPointManager(Render* render, int level);
	~CheckPointManager();
	void update(float deltaTime, Player* player);
	void draw();
	bool nextCheckPoint();
	int getCheckPoint();
	bool passedCheckPoint();
private:
	Render* render_;
	CheckPoint checkPoints_[3];
	sf::Texture* checkPointTexture_;
	Animation checkPointSprite_;

	int level_;
	bool switchLevel_;
	bool checkPointExist_;
};
#endif