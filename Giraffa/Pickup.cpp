#include "Global.h"
#include "NewMath.h"
#include "Pickup.h"

Pickup::Pickup()
{
	alive_ = false;
	wave_ = NewMath::randNormal() * 360.0f;
	powerup_ = POWERUP_NONE;
}

void Pickup::init(const sf::Vector2f& pos, const sf::Vector2f& vel)
{
	position_ = pos;
	velocity_ = vel;
	alive_ = true;

	switch (rand() % 3)
	{
	case 0 :
		powerup_ = POWERUP_LAZER;
		break;
	case 1:
		powerup_ = POWERUP_SHOTGUN;
		break;
	case 2:
		powerup_ = POWERUP_SHIELD;
		break;
	default :
		powerup_ = POWERUP_NONE;
		break;
	}
}

void Pickup::update(float deltaTime)
{
	position_ += velocity_ * deltaTime;

	if (position_.x < -48.0f)
		alive_ = false;

	if (position_.y < HEIGHT_HUD + 52.0f)
	{
		position_.y = HEIGHT_HUD + 52.0f;
		velocity_.y *= -1.0f;
	}
	if (position_.y > HEIGHT - 52.0f)
	{
		position_.y = HEIGHT - 52.0f;
		velocity_.y *= -1.0f;
	}

	wave_ += 10.0f * deltaTime;
	if (wave_ > 360.0f)
		wave_ -= 360.0f;
}

const sf::Vector2f& Pickup::getPosition() const
{
	return position_;
}

float Pickup::getAngle()
{
	return sinf(wave_);
}

PowerupStates Pickup::getPowerup()
{
	return powerup_;
}

bool Pickup::isAlive()
{
	return alive_;
}

void Pickup::kill()
{
	alive_ = false;
}