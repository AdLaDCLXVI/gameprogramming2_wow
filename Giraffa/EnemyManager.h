#ifndef ENEMYMANAGER_H
#define ENEMYMANAGER_H
#include <fstream>
#include "Render.h"
#include "EnemyOne.h"
#include "EnemyTwo.h"
#include "EnemyThree.h"
#include "Khan.h"
#include "Shape.h"
#include "AudioManager.h"
#include "DeadEnemyManager.h"
#include "Circle.h"
#include "HitInfo.h"

class EnemyBulletManager;
class Player;
class DeadEnemyManager;

class PickupManager;
class EnemyManager
{
public:
	EnemyManager(Render* render, PickupManager* pickupManager, AudioManager* audio);
	~EnemyManager();
	void init(std::string path);
	void update(float deltaTime);
	void activate(bool state);
	bool isActivated();
	void draw();
	bool collidePlayerBullet(Circle * circle);
	bool collidePlayer(Shape* shape);
	void collideShield(Circle* circle);
	void collideLazer(Shape* shape);
	bool isEmpty();
	bool GameOver();
	int GetScore();
	bool PlayerLose();
	void clean();
	void activateBoss();
	void KillPlayer(sf::Vector2f);
private:

	bool collideShape(Shape* shape);
	bool collideCircle(Circle* circle, sf::Vector2f& coord);

	Render* render_;
	EnemyBulletManager* enemyBulletManager_;
	DeadEnemyManager* deadEnemyManager_;

	PickupManager* pickupManager_;
	EnemyOne* enemyOneList_;
	sf::Texture* enemyOneTexture_;
	Shape collisionShapeOne_;

	EnemyTwo* enemyTwoList_;
	sf::Texture* enemyTwoTexture_;
	Shape collisionShapeTwo_;

	EnemyThree* enemyThreeList_;
	sf::Texture* enemyThreeTexture_;
	Shape collisionShapeThree_;

	Khan* Boss;

	
	AudioManager* audioManager_;
	soundClip* Shooting;
	sf::Sound soundPlayer_;
	soundClip* Laser;
	soundClip* Spraying;
	soundClip* EnemyShoot;
	soundClip* EnemyDeath1;
	soundClip* EnemyDeath2;
	soundClip* GiraffDead;

	int score_;
	int enemyOneNumber_;
	int enemyTwoNumber_;
	int enemyThreeNumber_;
	bool active_;
	bool playerHit_;
	bool empty_;
	bool BossDying;
};
#endif