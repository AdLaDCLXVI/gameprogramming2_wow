#include <stdio.h>
#include "NewMath.h"
#include "Lazer.h"

Lazer::Lazer()
{
	collisionShape_.setRect(8, 8, -8.0f, 0.0f);
}

void Lazer::update(const sf::Vector2f& pos, const sf::Vector2f& target)
{
	position_ = pos;
	target_ = target;
	sf::Vector2f angle = NewMath::angle(position_, target_);
	sf::Vector2f regionalPos = NewMath::align(position_, target_);

	collisionShape_.setPosition(position_);
	collisionShape_.setPointPosition(3, -angle.y * 8,  angle.x * 8);
	collisionShape_.setPointPosition(0,  angle.y * 8, -angle.x * 8);
	collisionShape_.setPointPosition(1, regionalPos.x - angle.y * 8, regionalPos.y + angle.x * 8);
	collisionShape_.setPointPosition(2, regionalPos.x + angle.y * 8, regionalPos.y - angle.x * 8);
}

const sf::Vector2f& Lazer::getPosition()
{
	return position_;
}

const sf::Vector2f& Lazer::getTarget()
{
	return target_;
}

Shape* Lazer::getShape()
{
	printf("Get collider\n");
	return &collisionShape_;
}

float Lazer::getAngle()
{
	return atan2f(target_.y - position_.y, target_.x - position_.x) * RTD;
}

float Lazer::getScale()
{
	return NewMath::length(target_ - position_);
}