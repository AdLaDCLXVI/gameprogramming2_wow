#pragma once
#include "State.h"
#include "Button.h"

class Render;
class Input;
class AudioManager;
class Button;
class HighScore : public State
{
public:
	HighScore(Render* pRenderer, Input* pInput, AudioManager* audio, int playerScore);
	bool update(float pDeltaTime);
	void exit();
	void enter();
	void draw();
	State* getNextState();
	States getState();
private:
	Render* mRenderer;
	Input* mInput;
	AudioManager* audio_;
	int Score[5];
	std::string Name[5];
	Button BackToMenuButton_;
	Button retryButton_;

	int returnState_;
};