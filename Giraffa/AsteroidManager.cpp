#include "Global.h"
#include "NewMath.h"
#include "CollisionManager.h"
#include "Render.h"
#include "Player.h"
#include "AsteroidManager.h"

AsteroidManager::AsteroidManager(Render* render, AudioManager* pAudio)
{
	render_ = render;
	audio_ = pAudio;
	clean_ = false;
	asteroidSprite_.setTexture(*render_->getInGameTexture());
	asteroidSprite_.setTextureRect({ 0, 323, 101, 100 });
	asteroidSprite_.setOrigin(50.0f, 50.0f);

	texture = render_->addTexture("assets/textures/rainbow.png");
	stop_ = false;

	rainbowAsteroidSprite_.init(6, 0.5f, 128, 128);
	rainbowAsteroidSprite_.getSprite()->setTexture(*texture);
	rainbowAsteroidSprite_.getSprite()->setOrigin(64.0f, 64.0f);

	texture = render_->addTexture("assets/textures/rainbowExplosion.png");

	blowRainbow_.init(4, 0.5f, 456, 456);
	blowRainbow_.getSprite()->setTexture(*texture);
	blowRainbow_.getSprite()->setOrigin(228.0f, 228.0f);

	texture = render_->addTexture("assets/textures/asteroidExplosion.png");

	collisionShape_.setRadius(50.0f);
	rainbowAsteroid_.kill();

	lastDeleted_ = 0;
	lastDeadDeleted_ = 0;
	scale_ = 1;

	asteroid = *audio_->AddSound("assets/sound/asteroidExplosion.ogg");
	rainbow = *audio_->AddSound("assets/sound/clearScreen.ogg");
	soundPlayer_.setVolume(audio_->sfxVolume_);
	rainbowHue_ = 0;
}

void AsteroidManager::update(float deltaTime, Player* player)
{
	if (clean_)
	{
		for (int i = 0; i < 6; i++)
		{
			auto& o = asteroidList_[i];
			if (o.isAlive())
				o.kill();
		}
	}
	if (rand() % 200 == 1)
	{
		sf::Vector2f edge;
		sf::Vector2f dir;
		float d;
		if (NewMath::randBool())
		{
			edge.x = NewMath::choose(-5.0f, WIDTH + 50.0f);
			edge.y = (float)NewMath::randRange(0, HEIGHT);
			
			if (edge.x < 0)
				d = NewMath::randRange(-45.0f, 45.0f);
			else
				d = NewMath::randRange(135.0f, 255.0f);
		}
		else
		{
			edge.x = (float)NewMath::randRange(0, WIDTH);
			edge.y = NewMath::choose(-55.0f, HEIGHT + 55.0f);

			if (edge.y < 0)
				d = NewMath::randRange(225.0f, 315.0f);
			else
				d = NewMath::randRange(135.0f, 45.0f);
		}

		dir.x = cosf(d * DTR) * 9.0f;
		dir.y = sinf(d * DTR) * 9.0f;

		if (rand() % 10 == 1){
			if (!rainbowAsteroid_.isAlive())
				rainbowAsteroid_.init(edge, dir);
		}
		else
			int id = addAsteroid(edge, dir);
	}
	{
		for (int i = 0; i < 6; i++)
		{
			auto& o = asteroidList_[i];
			if (o.isAlive())
				o.update(deltaTime);
		}
	}
	for (int i = 0; i < 8; i++)
	{
		auto& o = deadAsteroidList_[i];
		if (o.isAlive())
			o.Update(deltaTime);
	}
	if (rainbowAsteroid_.isAlive())
		if (!rainbowAsteroid_.activated())
		{
			rainbowAsteroid_.update(deltaTime);
			rainbowAsteroidSprite_.update(deltaTime);

		}
		else
		{
			if (blowRainbow_.getSprite()->getScale().x < 10)
			{
				scale_+=1.0f;
				blowRainbow_.update(deltaTime);
				blowRainbow_.getSprite()->setScale(scale_, scale_);
				rainbowHue_ += 0.2f * deltaTime;
				if (rainbowHue_ > 1.0f)
					rainbowHue_ -= 1.0f;

				
				blowRainbow_.getSprite()->setColor(NewMath::setHue(rainbowHue_));
			}
			else
			{			
				rainbowAsteroid_.kill();
				clean_ = false;
				scale_ = 1;
				blowRainbow_.getSprite()->setScale(1, 1);
			}
		}
	if (!rainbowAsteroid_.isAlive())
		clean_ = false;
}

void AsteroidManager::draw()
{
	for (int i = 0; i < 6; i++)
	{
		auto& o = asteroidList_[i];
		if (o.isAlive())
		{
			asteroidSprite_.setPosition(o.getPosition());
			asteroidSprite_.setRotation(o.getAngle());
			render_->renderToWindow()->draw(asteroidSprite_);
		}
	}
	for (int i = 0; i < 6; i++)
	{
		auto& o = deadAsteroidList_[i];
		if (o.isAlive())
		{
			render_->renderToWindow()->draw(*o.GetSprite());
		}
	}
	if (rainbowAsteroid_.isAlive())
	{
		if (!rainbowAsteroid_.activated())
		{

			rainbowAsteroidSprite_.getSprite()->setPosition(rainbowAsteroid_.getPosition());
			rainbowAsteroidSprite_.getSprite()->setRotation(rainbowAsteroid_.getAngle());

			render_->renderToWindow()->draw(*rainbowAsteroidSprite_.getSprite());
		}
		else
		{
			blowRainbow_.getSprite()->setPosition(rainbowAsteroid_.getPosition());
			blowRainbow_.getSprite()->setRotation(rainbowAsteroid_.getAngle());

			render_->renderToWindow()->draw(*blowRainbow_.getSprite());
		}
	}
}

bool AsteroidManager::collideShape(Shape* shape, HitInfo& hitInfo)
{
	for (int i = 0; i < 6; i++)
	{
		auto& o = asteroidList_[i];
		if (o.isAlive())
		{
			collisionShape_.setPosition(o.getPosition());
			if (CollisionManager::collideShapeCir(shape, &collisionShape_, hitInfo))
			{
				o.kill();
				render_->shakeScreen(5);
				soundPlayer_.setBuffer(asteroid.mClip);
				soundPlayer_.play();
				if (findEmptyDeadSlot())
				{
					deadAsteroidList_[lastDeadDeleted_].init(texture, o.getPosition());
				}
				return true;
			}
		}
	}
	if (rainbowAsteroid_.isAlive())
	{
		collisionShape_.setPosition(rainbowAsteroid_.getPosition());
		if (CollisionManager::collideShapeCir(shape, &collisionShape_, hitInfo))
		{
			rainbowAsteroid_.kill();
			render_->shakeScreen(5);
			soundPlayer_.setBuffer(rainbow.mClip);
			soundPlayer_.play();
			clean_ = true;
			return true;
		}
	}
	return false;
}

bool AsteroidManager::collideCircle(Circle* circle, HitInfo& hitInfo)
{
	for (int i = 0; i < 6; i++)
	{
		auto& o = asteroidList_[i];
		if (o.isAlive())
		{
			collisionShape_.setPosition(o.getPosition());
			if (CollisionManager::collideCirCir(circle, &collisionShape_, hitInfo))
			{
				o.kill();
				render_->shakeScreen(5);
				soundPlayer_.setBuffer(asteroid.mClip);
				soundPlayer_.play();
				if (findEmptyDeadSlot())
				{
					deadAsteroidList_[lastDeadDeleted_].init(texture, o.getPosition());
				}
				return true;
			}
		}
	}
	if (rainbowAsteroid_.isAlive())
	{
		collisionShape_.setPosition(rainbowAsteroid_.getPosition());
		if (CollisionManager::collideCirCir(circle, &collisionShape_, hitInfo))
		{
			rainbowAsteroid_.kill();
			render_->shakeScreen(5);
			soundPlayer_.setBuffer(rainbow.mClip);
			soundPlayer_.play();
			clean_ = true;
			return true;
		}
	}

	return false;
}

int AsteroidManager::addAsteroid(const sf::Vector2f & pos, const sf::Vector2f & vel)
{
	if (findEmptySlot())
	{
		asteroidList_[lastDeleted_].init(pos, vel);
		return lastDeleted_;
	}
	
	return -1;
}

bool AsteroidManager::findEmptySlot()
{
	for (int i = lastDeleted_; i < 6; i++)
		if (!asteroidList_[i].isAlive())
		{
			lastDeleted_ = i;
			return true;
		}

	for (int i = 0; i < lastDeleted_; i++)
		if (!asteroidList_[i].isAlive())
		{
			lastDeleted_ = i;
			return true;
		}

	return false;
}

bool AsteroidManager::getClean()
{
	return clean_;
}

void AsteroidManager::stopClean()
{
	clean_ = false;
}

void AsteroidManager::stop()
{
	for (int i = 0; i < 6; i++)
		asteroidList_[i].kill();

	stop_ = true;
}

bool AsteroidManager::getStop()
{
	return stop_;
}

bool AsteroidManager::findEmptyDeadSlot()
{
	for (int i = lastDeadDeleted_; i < 8; i++)
		if (!deadAsteroidList_[i].isAlive())
		{
			lastDeadDeleted_ = i;
			return true;
		}

	for (int i = 0; i < lastDeadDeleted_; i++)
		if (!deadAsteroidList_[i].isAlive())
		{
			lastDeadDeleted_ = i;
			return true;
		}

	return false;
}