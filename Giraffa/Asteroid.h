#ifndef ASTEROID_H
#define ASTEROID_H
#include <SFML\System\Vector2.hpp>

class Asteroid
{
public:
	Asteroid();
	~Asteroid() {};
	void init(const sf::Vector2f& pos, const sf::Vector2f& vel);
	void update(float deltaTime);
	const sf::Vector2f& getPosition() const;
	float getAngle();
	bool isAlive();
	void kill();

private:
	sf::Vector2f posistion_;
	sf::Vector2f velocity_;

	char alive_;
	float angle_;
};
#endif