#ifndef OptionState_H
#define OptionState_H
#include <SFML\Graphics.hpp>
#include "Button.h"
#include "State.h"
#include "SliderCheck.h"

class Render;
class Input;
class AudioManager;

class OptionState : public State
{
public:
	OptionState(Render* render, Input* input, AudioManager* audio);
	~OptionState() {};
	void enter();
	bool update(float deletaTime);
	void draw();
	float x;
	float y;
	States getState();
	State* getNextState();
	void exit();
private:
	Render* render_;
	Input* input_;
	AudioManager* audio_;
	SliderCheck volumeSlider_;
	SliderCheck sfxSlider_;


	sf::Sprite RightKeys_;
	sf::Sprite LeftKeys_;
	sf::Sprite FireRightKeys_;
	sf::Sprite FireLeftKeys_;
	sf::Sprite PowerUpRightKeys_;
	sf::Sprite PowerUpLeftKeys_;
	sf::Sprite MovementRightKeys_;
	sf::Sprite MovementLeftKeys_;
	sf::Sprite ControlsKeys_;
	sf::Sprite MusicKeys_;
	// sf::Sprite MusicBarKeys_;
	sf::Sprite SFXKeys_;
	// sf::Sprite SFXBarKeys_;
	sf::Sprite OptionsKeys_;
	sf::Sprite SoundSettingsKeys_;


	sf::Texture* menuTexture_;
	sf::Texture* buttonTexture_;
	sf::Texture* imageTexture_;

	Button BackToMenuButton_;
	Button RightMenuButton_;
	Button LeftMenuButton_;
};
#endif