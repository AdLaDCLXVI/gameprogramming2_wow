#ifndef ENEMYTWO_H
#define ENEMYTWO_H
#include <SFML\Graphics\Texture.hpp>
#include <SFML\System\Vector2.hpp>
#include "Animation.h"

class EnemyTwo
{
public:
	EnemyTwo();
	~EnemyTwo();
	void init(sf::Vector2f pos, sf::Vector2f vel, sf::Texture* texture);
	void update(float deltaTime);
	const sf::Vector2f& getPosition() const;
	void setPosition(float x, float y);
	void setVelocity(float x, float y);
	void setTexture(sf::Texture* texture);
	bool isAlive();
	void kill();
	Animation* getSprite();
	bool shootRequest();
private:
	Animation* enemySprite_;
	sf::Vector2f position_;
	sf::Vector2f velocity_;
	bool alive_;
	char states_;
};
#endif