#include "Input.h"
#include "Global.h"
#include "FoG.h"
#include "Render.h"
#include "PowerLaser.h"
#include "Entity.h"
#include "Giraffe.h"

PowerLaser::PowerLaser(Render* pRenderer, float pX, float pY)
{
	mX = pX;
	mY = pY;
	mIsDead = false;
	mRenderer = pRenderer;
	mTargetPos = vec2(pX, pY);
	mUVcoord = { 0.0f, 0.359375f, 0.0625f, 0.375f };
}

void PowerLaser::Draw()
{
	mRenderer->DrawRectangleStretch(vec2(mX, mY), mTargetPos, mUVcoord, 32.0f);
}

ENTITY_TYPES PowerLaser::GetType()
{
	return ENTITY_POWERLASER;
}


void PowerLaser::Update(float DeltaTime, vec2 pTargetPos, bool setState)
{
	mTargetPos = pTargetPos;

	isActive = setState;

}

bool PowerLaser::getActive()
{
	return isActive;
}

Shape PowerLaser::GetShape()
{
	vec2 delta = mTargetPos - vec2(mX, mY);
	vec2 rot = ExtendedMath::Normalize(delta);
	vec2 posxx = ExtendedMath::Normal(rot * 32.0f);
	vec2 posxy = ExtendedMath::Normal(rot * 32.0f);
	vec2 posyy = delta + ExtendedMath::Normal(rot * 32.0f);
	vec2 posyx = delta - ExtendedMath::Normal(rot * 32.0f);

	Shape mCollisionShape = Shape();
	mCollisionShape.AddPoint(posxx.x, posxx.y);
	mCollisionShape.AddPoint(posyy.x, posyy.y);
	mCollisionShape.AddPoint(posyx.x, posyx.y);
	mCollisionShape.AddPoint(posxy.x, posxy.y);

	mCollisionShape.SetPosition(mX, mY);

	return mCollisionShape;
}