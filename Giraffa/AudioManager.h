#ifndef _AUDIOMANAGER_H_
#define _AUDIOMANAGER_H_
#include <SFML\Audio.hpp>
#include <vector>

typedef const char* string;

struct soundClip
{
	sf::SoundBuffer mClip;
	const char* mPath;
};

struct musicClip
{
	sf::Music mClip;
	const char* mPath;
};

class AudioManager
{
public:
	AudioManager();
	~AudioManager();
	soundClip* AddSound(string pPath); // Return null when false
	musicClip* AddMusic(string pPath);
	void DeleteSound(soundClip* pSound);
	void DeleteMusic(musicClip* pMusic);
	void SetGlobalMusic(std::string path);
	void StopGlobalMusic();
	void SetSound();
	void PlayMenuSound();
	void PlaySoundRand(sf::Sound*);
	void Update();

	void setVolume(float newVolume);
	void setSfxVolume(float newVolume);
	float sfxVolume_;

private:
	float musicVolume_;

	std::vector<soundClip*> mSoundClips;
	std::vector<musicClip*> mMusicClips;
	sf::Music GlobalMusic;
	soundClip* MenuClick;
	sf::Sound soundPlayer_;
	std::string incMusic_;
	float volume;
	bool needChange;
};
#endif