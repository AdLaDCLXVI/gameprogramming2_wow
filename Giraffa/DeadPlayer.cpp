#include "DeadPlayer.h"

DeadPlayer::DeadPlayer(sf::Texture* texture, sf::Vector2f pos)
{
	position_ = pos;

	enemySprite_ = new Animation;
	enemySprite_->init(7, 0.5f, 128, 128);
	enemySprite_->getSprite()->setTexture(*texture);
	enemySprite_->getSprite()->setOrigin({ 64, 64 });
	enemySprite_->getSprite()->setPosition(position_);
}

DeadPlayer::~DeadPlayer()
{
	delete enemySprite_;
}

void DeadPlayer::Update(float pDeltaTime)
{
	enemySprite_->update(pDeltaTime);
	if (enemySprite_->animationEnd())
	{
		enemySprite_->setFrame(7);
		enemySprite_->setFrameSpeed(0.0f);
	}
}

sf::Sprite* DeadPlayer::GetSprite()
{
	return enemySprite_->getSprite();
}
bool DeadPlayer::Destroy()
{
	return enemySprite_->animationEnd();
}