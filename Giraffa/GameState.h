#ifndef GAMESTATE_H
#define GAMESTATE_H
#include <SFML\Graphics.hpp>
#include "Button.h"
#include "Player.h"
#include "PlayerBulletManager.h"
#include "EnemyManager.h"
#include "Pause.h"
#include "FamilyMember.h"
#include "State.h"

class Input;
class Render;
class AsteroidManager;
class PickupManager;
class Shield;
class AudioManager;
class CheckPointManager;

class GameState : public State
{
public:
	GameState(Render* render, Input* input, int level, AudioManager* audio);
	~GameState() {};
	void enter();
	bool update(float deletaTime);
	void draw();
	void drawFront();
	States getState();
	State* getNextState();
	void exit();

private:
	void setLevel(int level);

	AudioManager* audio_;
	Render* render_;
	Input* input_;
	Player* player_;
	Shield* shield_;
	Pause pauseScreen_;
	EnemyManager* enemyManager_;
	AsteroidManager* asteroidManager_;
	PlayerBulletManager* playerBulletManager_;
	PickupManager* pickupManager_;
	CheckPointManager* checkPointManager_;
	sf::Texture* hudTexture_;
	sf::Sprite hudSprite_;

	sf::Sound soundPlayer_;
	soundClip* playerShoot_;
	soundClip* powerUp_;
	soundClip* powerLight_;

	Button OptionButton_;
	Button BackToLevelSelectButton_;
	Button QuitGameButton_;
	Button ResumeButton_;

	FamilyMember family_;
	FamilyMember family2_;
	FamilyMember family3_;
	sf::Sprite familySprite_;

	bool pause_;
	int returnState;
	int currentLevel_;
};
#endif