#pragma once
#include "DeadEnemy.h"
#include "DeadEnemyTwo.h"
#include "DeadEnemyThree.h"
#include "DeadPlayer.h"
#include <vector>
#include "Render.h"

class DeadEnemyManager
{
public:
	DeadEnemyManager(Render* pRender);
	~DeadEnemyManager();
	void AddDeadEnemy(float type, sf::Vector2f position);
	void Update(float deltaTime);
	void Draw();
	bool deadPlayer();
private:
	std::vector<DeadEnemy*> DeadEnemyListOne;
	std::vector<DeadEnemyTwo*> DeadEnemyListTwo;
	std::vector<DeadEnemyThree*> DeadEnemyListThree;
	DeadPlayer* player_;
	Render* mRender;
	sf::Texture* enemyOne_;
	sf::Texture* enemyTwo_;
	sf::Texture* enemyThree_;
	sf::Texture* playerTexture_;
};