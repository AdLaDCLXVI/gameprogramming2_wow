#include "Global.h"
#include "Render.h"
#include "Input.h"
#include "GameState.h"
#include "StartState.h"
#include "OptionState.h"
#include "MenuState.h"
#include "NewMath.h"

StartState::StartState(Render* render, Input* input, AudioManager* audio)
{
	render_ = render;
	input_ = input;
	audio_ = audio;
	returnState = 0;

	wave_ = 0.0f;
}

void StartState::enter()
{
	menuTexture_ = render_->addTexture("assets/textures/spr_buttons.png");
	anykeyTexture_ = render_->addTexture("assets/textures/spr_anykey.png");
	Level= audio_->AddSound("assets/sound/selectLevel.wav");
	soundPlayer_.setBuffer(Level->mClip);
	soundPlayer_.setVolume(audio_->sfxVolume_);

	levelOneButton_ = Button({ 400, 600 }, menuTexture_, { 1634.0f, 93.0f, 199.0f, 200.0f });
	levelTwoButton_ = Button({ 700, 500 }, menuTexture_, { 1358.0f, 334.0f, 199.0f, 200.0f });
	levelThreeButton_ = Button({ 800, 200 }, menuTexture_, { 1358.0f, 334.0f, 199.0f, 200.0f });
	levelFourButton_ = Button({ 1150, 100 }, menuTexture_, { 1358.0f, 334.0f, 199.0f, 200.0f });
	menuButton_ = Button({ 1150, 600 }, menuTexture_, { 1002.0f, 641.0f, 210.0f, 211.0f });
}

bool StartState::update(float deltaTime)
{

	menuButton_.update((sf::Vector2f)input_->getMousePos());
	if (input_->getMousePressed(sf::Mouse::Left))
		menuButton_.setPressed((sf::Vector2f)input_->getMousePos());

	if (menuButton_.reset())
	{
		audio_->PlayMenuSound();
		returnState = 2;
		return false;
	}

	levelOneButton_.update((sf::Vector2f)input_->getMousePos());
	if (input_->getMousePressed(sf::Mouse::Left))
		levelOneButton_.setPressed((sf::Vector2f)input_->getMousePos());

	if (levelOneButton_.reset())
	{
		soundPlayer_.play();
		returnState = 1;
		return false;
	}

	wave_ += 10.0f * deltaTime;
	soundPlayer_.setBuffer(Level->mClip);
	float pulse = sinf(wave_ * DTR);
	levelOneButton_.getSprite()->setScale(1 + pulse * 0.1, 1 - pulse * 0.1);

	/*levelTwoButton_.update((sf::Vector2f)input_->getMousePos());
	if (input_->getMousePressed(sf::Mouse::Left))
		levelTwoButton_.setPressed((sf::Vector2f)input_->getMousePos());

	if (levelTwoButton_.reset())
		return false;

	levelThreeButton_.update((sf::Vector2f)input_->getMousePos());
	if (input_->getMousePressed(sf::Mouse::Left))
		levelThreeButton_.setPressed((sf::Vector2f)input_->getMousePos());

	if (levelThreeButton_.reset())
		return false;

	levelFourButton_.update((sf::Vector2f)input_->getMousePos());
	if (input_->getMousePressed(sf::Mouse::Left))
		levelFourButton_.setPressed((sf::Vector2f)input_->getMousePos());

	if (levelFourButton_.reset())
		return false;*/

	return true;
}

void StartState::draw()
{
	render_->renderToWindow()->draw(*menuButton_.getSprite());
	render_->renderToWindow()->draw(*levelOneButton_.getSprite());
	render_->renderToWindow()->draw(*levelTwoButton_.getSprite());
	render_->renderToWindow()->draw(*levelThreeButton_.getSprite());
	render_->renderToWindow()->draw(*levelFourButton_.getSprite());
}

States StartState::getState()
{
	return STATE_STARTSTATE;
}

State* StartState::getNextState()
{
	switch (returnState)
	{
	case 2:
		return new MenuState(render_, input_, audio_);
	default:
		return new GameState(render_, input_, 0, audio_);
	}
}

void StartState::exit()
{
}