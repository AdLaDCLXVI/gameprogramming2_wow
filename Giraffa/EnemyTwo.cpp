#include "Global.h"
#include "EnemyTwo.h"

EnemyTwo::EnemyTwo()
{
	alive_ = false;
	states_ = 0x0;
	velocity_ = { 0.0f, 0.0f };
	position_ = { 0.0f, 0.0f };
	enemySprite_ = nullptr;
}


EnemyTwo::~EnemyTwo()
{
	delete enemySprite_;
}

void EnemyTwo::init(sf::Vector2f pos, sf::Vector2f vel, sf::Texture* texture)
{
	enemySprite_ = new Animation();
	enemySprite_->init(5, 2.0f, 155, 88);
	enemySprite_->getSprite()->setTexture(*texture);
	enemySprite_->getSprite()->setOrigin({ 77.0f, 44.0f });

	position_ = pos;
	velocity_ = vel;
	enemySprite_->getSprite()->setPosition(position_);

	alive_ = true;
}

void EnemyTwo::update(float deltaTime)
{
	if (position_.x < -80.0f)
		alive_ = false;

	if (position_.x > -77.0f && position_.x < 77.0f + WIDTH && position_.y > -44.0f && position_.y < 44.0f + HEIGHT)
	{
		position_ += velocity_ * deltaTime;
		states_ = 0x1;
	}
	else
	{
		position_.x -= 5.0f * deltaTime;
		states_ = 0x0;
	}

	if (position_.y < HEIGHT_HUD + 44.0f)
	{
		position_.y = HEIGHT_HUD + 44.0f;
		velocity_.y *= -1.0f;
		states_ = states_ | 0x2;
	}
	if (position_.y > HEIGHT - 44.0f)
	{
		position_.y = HEIGHT - 44.0f;
		velocity_.y *= -1.0f;
		states_ = states_ | 0x2;
	}

	enemySprite_->update(deltaTime);
	enemySprite_->getSprite()->setPosition(position_);
}

const sf::Vector2f& EnemyTwo::getPosition() const
{
	return position_;
}

void EnemyTwo::setPosition(float x, float y)
{
	position_.x = x;
	position_.y = y;
}

void EnemyTwo::setVelocity(float x, float y)
{
	velocity_.x = x;
	velocity_.y = y;
}

void EnemyTwo::setTexture(sf::Texture* texture)
{
	enemySprite_->getSprite()->setTexture(*texture);
}

bool EnemyTwo::isAlive()
{
	return alive_;
}

void EnemyTwo::kill()
{
	alive_ = false;
}

Animation* EnemyTwo::getSprite()
{
	return enemySprite_;
}

bool EnemyTwo::shootRequest()
{
	return states_ >> 0x1 == 0x1;
}