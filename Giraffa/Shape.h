#ifndef SHAPE_H
#define SHAPE_H
#include <SFML\System\Vector2.hpp>

class Shape
{
public:
	Shape();
	~Shape() {};
	void setRect(int width, int height, float originX, float originY);
	const sf::Vector2f& getPosition();
	void setPosition(const sf::Vector2f& pos);
	void addPoint(float x, float y);
	void setPointPosition(int index, float x, float y);
	sf::Vector2f getPoint(int index);
	int pointCount();
	void rotate(float angle);
	void rotate(sf::Vector2f angle);
private:
	sf::Vector2f points_[10];
	sf::Vector2f position_;
	sf::Vector2f angle_;
	int pointCount_;
};
#endif