#include <math.h>
#include "Global.h"
#include "NewMath.h"
#include "PlayerBullet.h"

PlayerBullet::PlayerBullet()
{
	state_ = 0x0;
}

void PlayerBullet::init(const sf::Vector2f& pos, const sf::Vector2f& vel)
{
	position_ = pos;
	velocity_ = vel;
	state_ = 0x1;
}

void PlayerBullet::update(float deltaTime)
{
	position_ += velocity_ * deltaTime;

	if (position_.x < 0 || position_.x > WIDTH || position_.y < 0 || position_.y > HEIGHT)
		state_ = 0x0;

	if ((state_ & 0x2) == 0x2)
	{
		velocity_ *= 0.94f;
		if (NewMath::length(velocity_) < 0.5f)
			state_ = 0x0;
	}
}

const sf::Vector2f& PlayerBullet::getPosition() const
{
	return position_;
}

const sf::Vector2f& PlayerBullet::getVelocity() const
{
	return velocity_;
}

bool PlayerBullet::isAlive()
{
	return state_ != 0x0;
}

void PlayerBullet::kill()
{
	state_ = 0x0;
}

void PlayerBullet::deflect(const sf::Vector2f& normal)
{
	//state_ = state_ | 0x2;
	velocity_ = NewMath::deflect(velocity_, normal);
}

void PlayerBullet::setElectric()
{
	state_ = state_ | 0x6;
}

void PlayerBullet::clean()
{
}

bool PlayerBullet::isElectric()
{
	return (state_ & 0x4) == 0x4;
}
