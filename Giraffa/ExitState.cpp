#include "Global.h"
#include "Render.h"
#include "ExitState.h"
#include "MenuState.h"
#include "AudioManager.h"

ExitState::ExitState(Render* render)
{
	render_ = render;

	time_ = 0;
}

bool ExitState::update(float deltaTime)
{
	time_ += deltaTime;

	if (time_ >= 20)
	{
		return nullptr;
	}


	return true;
}

void ExitState::draw()
{
	render_->drawText("David Isaksson: Producer", 500, 90);
	render_->drawText("Malin Runsten Fredriksson: Designer", 500, 150);
	render_->drawText("Adrian Lavrell: Lead Coder", 500, 210);
	render_->drawText("Madeleine Fj�ll: Lead Art", 500, 270);
	render_->drawText("Axel Eriksson: Lead Sound", 500, 330);
	render_->drawText("Lucas Palsson: Artist", 500, 390);
	render_->drawText("Johan Persson: QA", 500, 450);
}

States ExitState::getState()
{
	return STATE_EXITSTATE;
}

State* ExitState::getNextState()
{
	return nullptr;
}