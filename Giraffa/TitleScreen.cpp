#include "Global.h"
#include "NewMath.h"
#include "Render.h"
#include "Input.h"
#include "GameState.h"
#include "TitleScreen.h"
#include "OptionState.h"
#include "MenuState.h"
#include "StartState.h"
#include "AudioManager.h"

TitleScreen::TitleScreen(Render* render, Input* input, AudioManager* audio)
{
	render_ = render;
	input_ = input;
	audio_ = audio;
	active = false;

	wave_ = 0.0f;
}

void TitleScreen::enter()
{
	imageTexture_ = render_->addTexture("assets/textures/spr_title.png");
	Image_.setTexture(*imageTexture_);
	Image_.setOrigin(imageTexture_->getSize().x / 2.0f, imageTexture_->getSize().y / 2.0f);
	Image_.setPosition(WIDTH / 2.0f, 300.0f);

	anykeyTexture_ = render_->addTexture("assets/textures/spr_anykey.png");
	anykey_.init(11, 1.5f, 644, 181);
	anykey_.getSprite()->setTexture(*anykeyTexture_);
	anykey_.getSprite()->setScale(0.8f, 0.8f);
	anykey_.getSprite()->setOrigin({322, 181});
	anykey_.getSprite()->setPosition(WIDTH / 2.0f, 620);
}

bool TitleScreen::update(float deltaTime)
{
	if (active)
	{
		anykey_.update(deltaTime);
		if(anykey_.animationEnd())
		return false;
	}
	else
	{
		if (input_->getAnyKeyPressed())
		{
			active = true;
			audio_->PlayMenuSound();
		}
	}

	wave_ += 5.0f * deltaTime;
	if (wave_ > 360.0f)
		wave_ -= 360.0f;

	float pulse = sinf(wave_ * DTR);
	Image_.setScale(1 + pulse * 0.1, 1 - pulse * 0.1);
	Image_.setRotation(sinf(wave_ * 2.0f * DTR) * 12.0f);
	return true;
}

void TitleScreen::draw()
{
	render_->renderToWindow()->draw(*anykey_.getSprite());
	render_->renderToWindow()->draw(Image_);
}

States TitleScreen::getState()
{
	return STATE_TITLESCREEN;
}

State* TitleScreen::getNextState()
{
	return new StartState(render_, input_, audio_);
}

void TitleScreen::exit()
{
	render_->deleteTexture(anykeyTexture_);
}