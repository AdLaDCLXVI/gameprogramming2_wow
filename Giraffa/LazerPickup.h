#ifndef LAZERPICKUP_H
#define LAZERPICKUP_H
#include <SFML\System\Vector2.hpp>

class LazerPickup
{
public:
	LazerPickup();
	~LazerPickup() {};
	void init(const sf::Vector2f& pos, const sf::Vector2f& vel);
	void update(float deltaTime);
	const sf::Vector2f& getPosition() const;
	float getAngle();
	bool isAlive();
	void kill();
private:
	sf::Vector2f position_;
	sf::Vector2f velocity_;
	
	bool alive_;
	float wave_;
};
#endif