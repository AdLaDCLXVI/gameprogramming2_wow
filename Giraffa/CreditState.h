#ifndef CreditState_H
#define CreditState_H
#include <SFML\Graphics.hpp>
#include "Button.h"
#include "State.h"
#include "Animation.h"

class Render;
class Input;
class AudioManager;
class Shape;

class CreditState : public State
{
public:
	CreditState(Render* render, Input* input, AudioManager* audio);
	~CreditState() {};
	void enter();
	bool update(float deltaTime);
	void draw();
	float x;
	float y;
	States getState();
	State* getNextState();
	void exit();
private:
	int returnState;

	Render* render_;
	Input* input_;
	AudioManager* audio_;
	sf::Texture* menuTexture_;
	sf::Texture* creditTexture_;
	Button OptionButton_;
	Button CreditsButton_;
	Button BackToMenuButton_;
	sf::Sprite Image_;
	sf::Sprite credImage_;
	sf::Texture* imageTexture_;
	Animation anykey_;
	sf::Texture* anykeyTexture_;
};
#endif