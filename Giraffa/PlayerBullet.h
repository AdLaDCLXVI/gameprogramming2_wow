#ifndef PLAYERBULLET_H
#define PLAYERBULLET_H
#include <SFML\System\Vector2.hpp>

class PlayerBullet
{
public:
	PlayerBullet();
	~PlayerBullet() {};
	void init(const sf::Vector2f& pos, const sf::Vector2f& vel);
	void update(float deltaTime);
	const sf::Vector2f& getPosition() const;
	const sf::Vector2f& getVelocity() const;
	bool isAlive();
	void kill();
	void deflect(const sf::Vector2f& normal);
	void setElectric();
	void clean();
	bool isElectric();
private:
	sf::Vector2f position_;
	sf::Vector2f velocity_;

	short state_;
};
#endif