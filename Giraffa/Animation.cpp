#include "Animation.h"

Animation::Animation()
{
	timer_ = 0;
	animationEnd_ = false;
}

void Animation::init(int frames, float speed, int width, int height)
{
	maxFrames_ = frames;
	speed_ = speed;

	region_.left = 0;
	region_.top = 0;
	region_.width = width;
	region_.height = height;

	sprite_.setTextureRect(region_);
}

void Animation::setPositionY(int y)
{
	region_.top = y;
}

void Animation::setFrame(int frame)
{
	timer_ = frame;
	if (timer_ > maxFrames_)
		timer_ -= maxFrames_;
}

void Animation::update(float deltaTime)
{
	animationEnd_ = false;

	timer_ += speed_ * deltaTime;
	if (timer_ > maxFrames_)
	{
		timer_ -= maxFrames_;
		animationEnd_ = true;
	}

	region_.left = (int)floorf(timer_) * region_.width;
	sprite_.setTextureRect(region_);
}

void Animation::setFrameSpeed(float speed)
{
	speed_ = speed;
}

sf::Sprite* Animation::getSprite()
{
	return &sprite_;
}

bool Animation::animationEnd()
{
	return animationEnd_;
}
