#include "NewMath.h"
#include "AudioManager.h"

AudioManager::AudioManager()
{
	sfxVolume_ = 50.0f;
	musicVolume_ = 50.0f;
}

AudioManager::~AudioManager()
{
	{
		auto it = mSoundClips.begin();
		while (it != mSoundClips.end())
		{
			delete (*it);
			it++;
		}
		mSoundClips.clear();
	}

	auto it = mMusicClips.begin();
	while (it != mMusicClips.end())
	{
		delete (*it);
		it++;
	}
	mMusicClips.clear();
}
void AudioManager::Update()
{
	if (needChange)
	{
		if (GlobalMusic.getVolume() > 10)
			GlobalMusic.setVolume(GlobalMusic.getVolume() - 1);
		else
		{
			GlobalMusic.openFromFile(incMusic_);
			GlobalMusic.play();
			GlobalMusic.setLoop(true);
			needChange = false;
		}
	}

	else
		if (GlobalMusic.getVolume() < musicVolume_)
			GlobalMusic.setVolume(GlobalMusic.getVolume() + 1);
}

void AudioManager::setVolume(float newVolume)
{
	musicVolume_ = newVolume;
	GlobalMusic.setVolume(musicVolume_);
}
void AudioManager::setSfxVolume(float newVolume)
{
	sfxVolume_ = newVolume;
	soundPlayer_.setVolume(sfxVolume_);
}

void AudioManager::SetGlobalMusic(std::string path)
{

	incMusic_ = path;
	needChange = true;
	GlobalMusic.setVolume(musicVolume_);
}

soundClip* AudioManager::AddSound(string pPath)
{
	int i = 0;
	for (i; i < (int)mSoundClips.size(); i++)
		if (mSoundClips[i]->mPath = pPath)
			return mSoundClips[i];

	soundClip* tmp = new soundClip;
	if (!tmp->mClip.loadFromFile(pPath))
	{
		delete tmp;
		return nullptr;
	}
	tmp->mPath = pPath;

	return tmp;
}

musicClip* AudioManager::AddMusic(string pPath)
{
	int i = 0;
	for (i; i < (int)mMusicClips.size(); i++)
		if (mMusicClips[i]->mPath = pPath)
			return mMusicClips[i];

	musicClip* tmp = new musicClip;
	if (!tmp->mClip.openFromFile(pPath))
	{
		delete tmp;
		return nullptr;
	}
	tmp->mPath = pPath;

	return tmp;
}

void AudioManager::DeleteSound(soundClip* pSound)
{
	auto it = mSoundClips.begin();
	while (it != mSoundClips.end())
	{
		if ((*it) == pSound)
		{
			delete (*it);
			mSoundClips.erase(it);
			return;
		}
		it++;
	}
}

void AudioManager::DeleteMusic(musicClip * pMusic)
{
	auto it = mMusicClips.begin();
	while (it != mMusicClips.end())
	{
		if ((*it) == pMusic)
		{
			delete (*it);
			mMusicClips.erase(it);
			return;
		}
		it++;
	}
}


void AudioManager::StopGlobalMusic()
{
	GlobalMusic.stop();
}

void AudioManager::SetSound()
{
	MenuClick = AddSound("assets/sound/select.wav");
	soundPlayer_.setBuffer(MenuClick->mClip);
	soundPlayer_.setVolume(sfxVolume_);
}

void AudioManager::PlayMenuSound()
{
	soundPlayer_.play();
}

void AudioManager::PlaySoundRand(sf::Sound* Player)
{
	Player->setPitch(NewMath::randRange(0.8f, 1.2f));
	Player->play();
	//Player->setPitch(1);
}