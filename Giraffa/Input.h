#ifndef INPUT_H
#define INPUT_H
#include <SFML\Window.hpp>
#include <SFML\Graphics.hpp>

class Input
{
public:
	Input();
	~Input() {};
	void update(sf::RenderWindow* window);
	bool getKey(sf::Keyboard::Key key);
	bool getKeyPressed(sf::Keyboard::Key key);
	bool getAnyKeyPressed();
	char getCurrentChar();

	int getMouseX();
	int getMouseY();
	const sf::Vector2i& getMousePos() const;
	bool getMouse(sf::Mouse::Button button);
	bool getMousePressed(sf::Mouse::Button button);
private:
	sf::Event events_;
	sf::Vector2i mousePosition_;
	bool mouseHold_[sf::Mouse::Button::ButtonCount];
	bool mousePressed_[sf::Mouse::Button::ButtonCount];
	char currentChar_;

	bool keyHold_[sf::Keyboard::KeyCount];
	bool keyPressed_[sf::Keyboard::KeyCount];
};
#endif