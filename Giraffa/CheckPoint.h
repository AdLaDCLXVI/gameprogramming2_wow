#ifndef CHECKPOINT_H
#define CHECKPOINT_H
#include <SFML\System\Vector2.hpp>

class CheckPoint
{
public:
	CheckPoint();
	~CheckPoint() {};
	void init(const sf::Vector2f& pos, int level);
	void update(float deltaTime);
	void activate();
	bool isAlive();
	void kill();
	int getLevel();
	const sf::Vector2f& getPosition();
private:
	sf::Vector2f position_;
	sf::Vector2f velocity_;

	bool alive_;
	int level_;
};
#endif