#include "Game.h"

int main(int argC, char** argV)
{
	Game giraffaGame;
	if (giraffaGame.begin())
	{
		giraffaGame.run();
		giraffaGame.end();
	}

	return 0;
}