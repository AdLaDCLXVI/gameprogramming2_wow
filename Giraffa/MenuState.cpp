#include "Global.h"
#include "Render.h"
#include "Input.h"
#include "GameState.h"
#include "MenuState.h"
#include "OptionState.h"
#include "CreditState.h"
#include "StartState.h"
#include "Game.h"
#include "ExitState.h"

MenuState::MenuState(Render* render, Input* input, AudioManager* audio)
{
	render_ = render;
	input_ = input;
	audio_ = audio;

	returnState = 0;

	imageTexture_ = render_->addTexture("assets/textures/spr_menu.png");
	Image_.setTexture(*imageTexture_);
	Image_.setScale(1.0f, 1.0f);
	Image_.setPosition(500.0f, 100.0f);
}

void MenuState::enter()
{
	Image_.setTexture(*render_->getButtonTexture());
	Image_.setScale(1.0f, 1.0f);
	Image_.setTextureRect({ 1215, 224, 291, 109 });
	Image_.setPosition(500.0f, 100.0f);

	OptionButton_ = Button({ 640, 320 }, render_->getButtonTexture(), { 1114.0f, 395.0f, 243.0f, 63.0f });
	CreditsButton_ = Button({ 640, 400 }, render_->getButtonTexture(), { 1065.0f, 464.0f, 241.0f, 63.0f });
	BackToLevelSelectButton_ = Button({ 660, 250 }, render_->getButtonTexture(), { 0.0f, 577.0f, 628.0f, 61.0f });
	QuitGameButton_ = Button({ 640, 480 }, render_->getButtonTexture(), { 596.0f, 829.0f, 299.0f, 76.0f });
}

bool MenuState::update(float deltaTime)
{

	OptionButton_.update((sf::Vector2f)input_->getMousePos());
	if (input_->getMousePressed(sf::Mouse::Left))
		OptionButton_.setPressed((sf::Vector2f)input_->getMousePos());

	if (OptionButton_.reset())
	{
		audio_->PlayMenuSound();
		returnState = 2;
		return false;
	}

	CreditsButton_.update((sf::Vector2f)input_->getMousePos());
	if (input_->getMousePressed(sf::Mouse::Left))
		CreditsButton_.setPressed((sf::Vector2f)input_->getMousePos());

	if (CreditsButton_.reset())
	{
		audio_->PlayMenuSound();
		returnState = 3;
		return false;
	}

	QuitGameButton_.update((sf::Vector2f)input_->getMousePos());
	if (input_->getMousePressed(sf::Mouse::Left))
		QuitGameButton_.setPressed((sf::Vector2f)input_->getMousePos());

	if (QuitGameButton_.reset())
	{
		audio_->PlayMenuSound();
		returnState = 4;
		return false;
	}

	BackToLevelSelectButton_.update((sf::Vector2f)input_->getMousePos());
	if (input_->getMousePressed(sf::Mouse::Left))
		BackToLevelSelectButton_.setPressed((sf::Vector2f)input_->getMousePos());

	if (BackToLevelSelectButton_.reset())
	{
		audio_->PlayMenuSound();
		returnState = 1;
		return false;
	}

	return true;
}

void MenuState::draw()
{
	render_->renderToWindow()->draw(*OptionButton_.getSprite());
	render_->renderToWindow()->draw(*CreditsButton_.getSprite());
	render_->renderToWindow()->draw(*BackToLevelSelectButton_.getSprite());
	render_->renderToWindow()->draw(*QuitGameButton_.getSprite());
	render_->renderToWindow()->draw(Image_);
}

States MenuState::getState()
{
	return STATE_MENU;
}

State* MenuState::getNextState()
{
	switch (returnState)
	{
	case 4:
		return new ExitState(render_);
	case 3:
		return new CreditState(render_, input_, audio_);
	case 2:
		return new OptionState(render_, input_, audio_);
	default:
		return new StartState(render_, input_, audio_);
	}
}

void MenuState::exit()
{
}