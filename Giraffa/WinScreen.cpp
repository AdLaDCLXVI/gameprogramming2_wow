#include "MenuState.h"
#include "GameState.h"
#include "WinScreen.h"
#include "Render.h"
#include "Input.h"
#include "AudioManager.h"
#include <fstream>
#include <sstream>

WinScreen::WinScreen(Render* pRenderer, Input* pInput, AudioManager* audio, int HighScore)
{
	mRenderer = pRenderer;
	mInput = pInput;
	PlayerScore = HighScore;
	position = 5;
	PlayerName = "";
}

void WinScreen::enter()
{
	std::fstream file;
	file.open("assets/HighScore.txt");
	for (int i = 0; i < 5; i++)
	{
		file >> Name[i];
		file >> Score[i];
	}
	file.close();
	Score[5] = PlayerScore;
	Name[5] = PlayerName;

	for (int i = 4; i >= 0; i--)
	{
		if (Score[i + 1] > Score[i])
		{
			float tmp = Score[i];
			Score[i] = Score[i + 1];
			Score[i + 1] = tmp;

			std::string temp = Name[i];
			Name[i] = Name[i + 1];
			Name[i + 1] = temp;

			position = i;
		}
	}
}

void WinScreen::exit()
{
	Name[(int)position] = PlayerName;


}

bool WinScreen::update(float DeltaTime)
{
	
	//// Write name for highscore if there is a highscore
	//if (position != 5)
	//{
	//	sf::Event event;
	//	if (event.type == sf::Event::TextEntered)
	//		if (event.text.unicode < 128)
	//			PlayerName += static_cast<char>(event.text.unicode);
	//		else if (mInput->GetKey(sf::Keyboard::BackSpace) && PlayerName.size() > 0)
	//			PlayerName.pop_back();

	//}
	//if (mInput->GetKey(sf::Keyboard::Return))
	//{
	//	return false;
	//}
	return true;
}

void WinScreen::draw()
{
	std::string text;
	for (int i = 0; i < 5; i++)
	{
		std::stringstream strs;
		if (position != i)
		{
			mRenderer->drawText( Name[i].c_str(), 500, 50 * i + 100);
		}
		else
		{
			mRenderer->drawText( PlayerName.c_str(), 500, 50 * i + 100);
		}
		
		strs << Score[i];
		text = strs.str();
		mRenderer->drawText( text.c_str(), 1100, 50 * i + 100);

	}
}

State* WinScreen::getNextState()
{
	return new MenuState( mRenderer, mInput, audio_);
}

States WinScreen::getState()
{
	return STATE_WINSCREEN;
}