#include "CollisionManager.h"
#include "SliderCheck.h"
#include "OptionState.h"


SliderCheck::SliderCheck(float x, float y, Render * render, float startX)
{
	render_ = render;
	position_ = { x + startX, y };
	slideBar_.setTexture(*render->getButtonTexture());
	slideBar_.setTextureRect({ 742, 201, 575, 22 });
	slideBar_.setPosition(x, y);

	width_ = slideBar_.getLocalBounds().width;

	startX_ = x;
	grab_ = false;

	slideButton_.setTexture(*render->getButtonTexture());
	slideButton_.setTextureRect({ 1213, 648, 69, 89 });
	slideButton_.setOrigin(34.5f, 26.5f);
	slideButton_.setPosition(position_);
	slideButton_.setScale(0.8f, 0.8f);
}

void SliderCheck::update(int mouseX)
{
	slideButton_.setPosition(position_);

	if (grab_)
	{
		position_.x = (float)mouseX;
	}

	if (position_.x < startX_)
	{
		position_.x = startX_;
	}

	if (position_.x > startX_ + width_)
	{
		position_.x = startX_ + width_;
	}


}

float SliderCheck::getValue()
{
	return (position_.x - startX_) / width_ * 200.0f;
}

void SliderCheck::pressed(const sf::Vector2f & mousePos)
{
	if (CollisionManager::collideRectPoint(mousePos, slideBar_.getGlobalBounds()))
	{
		grab_ = true;
	}
}

void SliderCheck::reset()
{
	grab_ = false;
}

void SliderCheck::draw()
{
	render_->renderToWindow()->draw(slideBar_);
	render_->renderToWindow()->draw(slideButton_);
}

