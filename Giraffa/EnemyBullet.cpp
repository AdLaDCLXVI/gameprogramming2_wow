#include "NewMath.h"
#include "Global.h"
#include "EnemyBullet.h"

EnemyBullet::EnemyBullet()
{
	alive_ = false;
}

void EnemyBullet::init(const sf::Vector2f& pos, float angle, float speed)
{
	position_ = pos;
	velocity_.x = cosf(angle * DTR) * speed;
	velocity_.y = sinf(angle * DTR) * speed;
	alive_ = true;
}

void EnemyBullet::update(float deltaTime)
{
	position_ += velocity_ * deltaTime;

	if (position_.x < 0 || position_.x > WIDTH || position_.y < 0 || position_.y > HEIGHT)
		alive_ = false;
}

const sf::Vector2f& EnemyBullet::getPosition()
{
	return position_;
}

float EnemyBullet::getDirection()
{
	return atan2f(velocity_.y, velocity_.x) * RTD;
}

bool EnemyBullet::isAlive()
{
	return alive_;
}

void EnemyBullet::kill()
{
	alive_ = false;
}