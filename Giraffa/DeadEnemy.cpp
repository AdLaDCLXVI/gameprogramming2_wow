#include "DeadEnemy.h"

DeadEnemy::DeadEnemy(sf::Texture* texture, sf::Vector2f pos)
{
	position_ = pos;

	enemySprite_ = new Animation;
	enemySprite_->init(9, 2.0f, 175, 175);
	enemySprite_->getSprite()->setTexture(*texture);
	enemySprite_->getSprite()->setOrigin({ 64, 64 });
	enemySprite_->getSprite()->setPosition(position_);
	
}

DeadEnemy::~DeadEnemy()
{
	delete enemySprite_;
}

void DeadEnemy::Update(float deltaTime)
{
	enemySprite_->update(deltaTime);
	
}

sf::Sprite* DeadEnemy::GetSprite()
{
	return enemySprite_->getSprite();
}

bool DeadEnemy::Destroy()
{
	return enemySprite_->animationEnd();
}