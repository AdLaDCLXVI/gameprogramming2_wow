#include "Global.h"
#include "Render.h"
#include "Player.h"
#include "CheckPointManager.h"

CheckPointManager::CheckPointManager(Render* render, int level)
{
	render_ = render;
	level_ = level;
	switchLevel_ = false;
	checkPointExist_ = false;
	checkPointTexture_ = render_->addTexture("assets/textures/spr_checkpoints.png");

	checkPointSprite_.init(5, 0.0f, 96, 89);
	checkPointSprite_.getSprite()->setTexture(*checkPointTexture_);
	checkPointSprite_.getSprite()->setOrigin({ 48, 44 });

	checkPoints_[0].init({ 32.0f + WIDTH, HEIGHT / 2.0f }, 0);
	checkPoints_[1].init({ 32.0f + WIDTH, HEIGHT / 2.0f }, 1);
	checkPoints_[2].init({ 32.0f + WIDTH, HEIGHT / 2.0f }, 2);
}

CheckPointManager::~CheckPointManager()
{
	render_->deleteTexture(checkPointTexture_);
}

void CheckPointManager::update(float deltaTime, Player* player)
{
	switchLevel_ = false;
	
	auto& o = checkPoints_[level_];
	if (o.isAlive())
	{
		o.update(deltaTime);
		if (o.getPosition().x < player->getPosition().x)
		{
			o.kill();
			checkPointExist_ = false;
			switchLevel_ = true;
		}
		checkPointSprite_.getSprite()->setPosition(o.getPosition());
	}
}

void CheckPointManager::draw()
{
	auto& o = checkPoints_[level_];
	if (o.isAlive())
		render_->renderToWindow()->draw(*checkPointSprite_.getSprite());
}

bool CheckPointManager::nextCheckPoint()
{
	
	if (level_ < 3)
	{
		if (!checkPointExist_)
		{
			level_++;
			checkPoints_[level_].activate();
			checkPointSprite_.setFrame(level_);
			checkPointExist_ = true;
		}
		return true;
	}

	return false;
}

int CheckPointManager::getCheckPoint()
{
	return level_;
}

bool CheckPointManager::passedCheckPoint()
{
	return switchLevel_;
}

