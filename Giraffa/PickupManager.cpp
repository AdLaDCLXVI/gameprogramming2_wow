#include "CollisionManager.h"
#include "Global.h"
#include "NewMath.h"
#include "Player.h"
#include "PickupManager.h"

PickupManager::PickupManager(Render* render)
{
	render_ = render;
	currentPowerup_ = POWERUP_NONE;

	pickupTexture_ = render_->getInGameTexture();
	pickupLazerSprite_.setTexture(*pickupTexture_);
	pickupLazerSprite_.setTextureRect({ 365, 0, 104, 105 });
	pickupLazerSprite_.setOrigin(52.0f, 52.0f);
	pickupShotgunSprite_.setTexture(*pickupTexture_);
	pickupShotgunSprite_.setTextureRect({ 258, 98, 105, 105 });
	pickupShotgunSprite_.setOrigin(52.0f, 52.0f);
	pickupShieldSprite_.setTexture(*pickupTexture_);
	pickupShieldSprite_.setTextureRect({ 364, 106, 104, 105 });
	pickupShieldSprite_.setOrigin(52.0f, 52.0f);

	collisionShape_.setRadius(48.0f);

	lastDeleted_ = 0;
}

void PickupManager::update(float deltaTime)
{
	for (int i = 0; i < 4; i++)
	{
		auto& o = pickupList_[i];
		if (o.isAlive())
			o.update(deltaTime);
	}
}

void PickupManager::addPickup(const sf::Vector2f & pos)
{
	if (rand() % 4 == 1)
		addPickup(pos, NewMath::angle(pos, { 0.0f, HEIGHT / 2.0f }) * 8.0f);
}

void PickupManager::addPickup(const sf::Vector2f& pos, const sf::Vector2f& vel)
{
	if (findEmptySlot())
		pickupList_[lastDeleted_].init(pos, vel);
}

void PickupManager::draw()
{
	for (int i = 0; i < 4; i++)
	{
		auto& o = pickupList_[i];
		if (o.isAlive())
		{
			switch (o.getPowerup())
			{
			case POWERUP_LAZER :
				pickupLazerSprite_.setPosition(o.getPosition());
				pickupLazerSprite_.setRotation(o.getAngle());
				render_->renderToWindow()->draw(pickupLazerSprite_);
				break;
			case POWERUP_SHOTGUN:
				pickupShotgunSprite_.setPosition(o.getPosition());
				pickupShotgunSprite_.setRotation(o.getAngle());
				render_->renderToWindow()->draw(pickupShotgunSprite_);
				break;
			case POWERUP_SHIELD:
				pickupShieldSprite_.setPosition(o.getPosition());
				pickupShieldSprite_.setRotation(o.getAngle());
				render_->renderToWindow()->draw(pickupShieldSprite_);
				break;
			default:
				break;
			}
		}
	}
}

void PickupManager::collidePlayer(Shape* shape)
{
	for (int i = 0; i < 4; i++)
	{
		auto& o = pickupList_[i];
		if (o.isAlive())
		{
			collisionShape_.setPosition(o.getPosition());
			if (CollisionManager::collideShapeCir(shape, &collisionShape_))
			{
				currentPowerup_ = o.getPowerup();
				o.kill();
				lastDeleted_ = i;
				return;
			}
		}
	}
}

PowerupStates PickupManager::getCurrentPickup()
{
	return currentPowerup_;
}

void PickupManager::empty()
{
	currentPowerup_ = POWERUP_NONE;
}

void PickupManager::drawHudPickup()
{
	switch (currentPowerup_)
	{
	case POWERUP_LAZER :
		pickupLazerSprite_.setPosition(WIDTH - 40.0f, 40.0f);
		render_->renderToWindow()->draw(pickupLazerSprite_);
		break;
	case POWERUP_SHOTGUN :
		pickupShotgunSprite_.setPosition(WIDTH - 40.0f, 40.0f);
		render_->renderToWindow()->draw(pickupShotgunSprite_);
		break;
	case POWERUP_SHIELD :
		pickupShieldSprite_.setPosition(WIDTH - 40.0f, 40.0f);
		render_->renderToWindow()->draw(pickupShieldSprite_);
		break;
	default:
		break;
	}
}

bool PickupManager::findEmptySlot()
{
	for (int i = lastDeleted_; i < 4; i++)
		if (!pickupList_[i].isAlive())
		{
			lastDeleted_ = i;
			return true;
		}

	for (int i = 0; i < lastDeleted_; i++)
		if (!pickupList_[i].isAlive())
		{
			lastDeleted_ = i;
			return true;
		}

	return false;
}
