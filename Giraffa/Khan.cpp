#include "Shape.h"
#include "NewMath.h"
#include "Global.h"
#include "Animation.h"
#include <sfml/Audio.hpp>
#include <time.h>
#include <stdlib.h>
#include "Render.h"
#include "Khan.h"
Khan::Khan()
{	
	alive_ = false;
	HP = 200;
	State = notReady;
	Time = 0;
	EyeTime = 0;
	target1 = 0;
	target2 = 10;

}

Khan::~Khan()
{
	delete enemySprite_;
}

void Khan::init(const sf::Vector2f& pos, const sf::Vector2f& vel)
{
	Time = 100;
	DeltaTime = 0;
	State = Start;
	alive_ = true;
	position_ = pos;
	velocity_ = vel;
	requestShoot = false;
	collisionShape_ = new Shape();
	collisionShape_->addPoint(80, -44);
	collisionShape_->addPoint(-140.0f, 90);
	collisionShape_->addPoint(320.0f, 71);
	collisionShape_->addPoint(0.0f, 256);
	collisionShape_->addPoint(232.0f, 256);
	collisionShape_->setPosition(position_);
}

void Khan::Update(float pTime)
{

	enemySprite_->getSprite()->setPosition(position_);
	Tentacles->getSprite()->setPosition(sf::Vector2f(position_.x - 20, position_.y + 120));
	Eyes.setPosition(position_);

	DeltaTime = pTime;
	if (HP > 0)
	{

		Tentacles->update(pTime);
		
		if (State == Start)
		{
			Walking();
		}
		else if (State == Nr1)
		{
			NrOne();
		}
		else if (State == Nr2)
		{
			NrTwo();
		}
		else if (State == Nr3 || State == Pre3)
			NrThree();
		else 
			Idle();
	}
	else
	{ 
		Death();
	}
	Time -= DeltaTime;
	collisionShape_->setPosition(position_);
	if (position_.y > HEIGHT + 120)
	{
		alive_ = false;
	}
	if (Eyes.getColor() == sf::Color::Red && EyeTime == 0)
	{
		Eyes.setColor(sf::Color::White);
	}
	if (EyeTime > 0)
	{
		EyeTime--; 
		enemySprite_->getSprite()->setPosition(position_.x , position_.y + 10);
		Tentacles->getSprite()->setPosition(sf::Vector2f(position_.x - 20, position_.y + 10 + 120));
		Eyes.setPosition(position_.x, position_.y + 10);
		Eyes.setColor(sf::Color::Red);
	}
	else
	{
	}
}



BossState Khan::GetState()
{
	return State;
}

void Khan::Walking()
{
	if (position_.x > -117.0f && position_.x < 117.0f + WIDTH && position_.y > -44.0f && position_.y < 44.0f + HEIGHT)
	{
		if (position_.x> WIDTH / 2 -100)
		{
			position_.x += velocity_.x;
			enemySprite_->update(DeltaTime);
		}
		else
		{
			State = Passive;
			Time = 100;

		}
	}
	else
	{
		position_.x -= 5.0f * DeltaTime;
	}
}

void Khan::Idle()
{
	if (Time > 1)
	{
		velocity_.y += 5.0f;
		if (velocity_.y > 360.0f) velocity_.y -= 360.0f;
		position_.y = 250 + sinf(velocity_.y* DTR) * 32.0f;
		enemySprite_->update(DeltaTime);
	}
	else
		NewState();
		
	
}

void Khan::NrOne()
{
	requestShoot = false;
	velocity_.y += 5.0f;
	if (velocity_.y> 360.0f) velocity_.y -= 360.0f;
	position_.y = 250 + sinf(velocity_.y * DTR) * 32.0f;
	if (Time > 1 && Time < 275)
	{
		
		

		if (Time % 50 == 0){
			requestShoot = true;
			target1 += 20.0f;
			if (target1 > 360) target1 -= -1;
		}
		else
			enemySprite_->update(DeltaTime);
		
	}
	else if (Time >= 275){	}

	
	else
	{
		enemySprite_->animationEnd();

		enemySprite_->update(DeltaTime);
		target1 = 0;
		Time = 100;
		State = Passive;
		enemySprite_ = IdleSprite;
	}
}

void Khan::NrTwo()
{


	if (Time > 800)
	{
		enemySprite_->update(DeltaTime);

		int tmpX = rand() % ((1140 - Time) / 10 + 1);
		int tmpY = rand() % ((1140 - Time) / 10 + 1);

		if (rand() % 2 == 0)
			tmpX *= -1;
		if (rand() % 2 == 0)
			tmpY *= -1;
			
		enemySprite_->getSprite()->setPosition(position_.x + tmpX, position_.y + 10 + tmpY);
		Tentacles->getSprite()->setPosition(sf::Vector2f(position_.x - 20 + tmpX, position_.y + 130 + tmpY));
		Eyes.setPosition(position_.x + tmpX, position_.y + 10 + tmpY);
	}

	else if (Time > 1)
	{
		if (Eyes.getColor() != sf::Color::Green)	
			Eyes.setColor(sf::Color::Green);
		while (!enemySprite_->animationEnd())
		{
			enemySprite_->update(DeltaTime);
		}
		
		target1 += 1.1;
		if (target1 > 360) target1 = 0;
		target2 += 1.1;
		if (target2 > 360) target2 = 0;
		requestShoot = true;
	}

	else
	{
		enemySprite_->animationEnd();
		Time = 100;
		State = Passive;
		requestShoot = false;
		target1 = 0;
		target2 = 10;
		Eyes.setColor(sf::Color::White);
		enemySprite_ = IdleSprite;
	}
}

void Khan::NrThree()
{
	requestShoot = false;
	if (State == Pre3)
	{
		if (Time > 140)
		{
			enemySprite_->update(DeltaTime);
		}
		else if (Time > 0)
		{
			if (Time % 2 == 0)
				requestShoot = true;
		}
		else
		{
			State = Nr3;
			Time = 300;

			enemySprite_->animationEnd();

			enemySprite_->update(DeltaTime);
		}
	}

	else
	{
		if (Time > 0)
		{
			if (!enemySprite_->animationEnd())
			{
				enemySprite_->update(DeltaTime);
			}
			velocity_.x += 5.0f;
			if (velocity_.x > 360.0f) velocity_.x -= 360.0f;
				position_.x += sinf(velocity_.x * DTR) * 3.0f;
			if (Time % 5 == 0)
				requestShoot = true;

			
		}

		else
		{
			velocity_.y += 1;
			Time = 250;
			State = Pre3;
		}
	}

	if (velocity_.y > 1)
	{
		State = Passive;
		Time = 100;
		velocity_.y = 5;

		enemySprite_->animationEnd();
		enemySprite_ = IdleSprite;

	}
}
void Khan::Death()
{
	position_.y += 1;
	
		enemySprite_->update(DeltaTime);
	
	
		int tmpX = rand() %  10 ;
		int tmpY = rand() %   10 ;

		if (rand() % 2 == 0)
			tmpX *= -1;
		if (rand() % 2 == 0)
			tmpY *= -1;

		enemySprite_->getSprite()->setPosition(position_.x + tmpX, position_.y + 10 + tmpY);
		Tentacles->getSprite()->setPosition(sf::Vector2f(position_.x - 20 + tmpX, position_.y + 130 + tmpY));
		Eyes.setPosition(position_.x + tmpX, position_.y + 10 + tmpY);
	
}

void Khan::NewState() {
	int tmp = rand() % 3;

	if (tmp == 0)
	{
		State = Nr1;
		Time = 400;
		enemySprite_ = attackOne;
	}
	else if (tmp == 1)
	{
		State = Nr2;
		Time = 968;
		enemySprite_ = attackTwo;
	}
	else
	{
		State = Pre3;
		Time = 250;
		velocity_.y = 0;
		enemySprite_ = attackThree;
	}
}

void Khan::HitBoss(float Dmg)
{
	HP -= Dmg;
	if (HP < 1)
	{
		State = Dead;
		enemySprite_ = death;
		requestShoot = false;
		Time = 26;
	}
	EyeTime = 4;
	
	float tmp = 100;
	enemySprite_->getSprite()->setPosition(position_.x + tmp, position_.y);
	Tentacles->getSprite()->setPosition(sf::Vector2f(position_.x + tmp - 20, position_.y + 120));
	Eyes.setPosition(position_.x + tmp, position_.y);
}

const sf::Vector2f& Khan::getPosition() const
{
	return position_;
}

void Khan::setTexture(sf::Texture* texture)
{
	enemySprite_->getSprite()->setTexture(*texture);
}

sf::Sprite* Khan::getSprite()
{
	return enemySprite_->getSprite();
}
sf::Sprite* Khan::getTentacleSprite()
{
	return Tentacles->getSprite();
}
sf::Sprite Khan::getEyesSprite()
{
	return Eyes;
}
void Khan::kill()
{
	alive_ = false;
}

bool Khan::shootRequest()
{
	return requestShoot;
}

bool Khan::isAlive()
{
	return alive_;
}
Shape* Khan::GetShape()
{
	return collisionShape_;
}

void Khan::CreateAnimation(Render* pRender)
{
	attackOne = new Animation();
	attackOne->init(13, 0.9, 458, 250);
	texture = pRender->addTexture("assets/textures/Boss/bossAttack_1.png");
	attackOne->getSprite()->setTexture(*texture);
	attackOne->getSprite()->setOrigin({ 117.0f, 44.0f });
	attackOne->getSprite()->setPosition(position_);

	attackTwo = new Animation();
	attackTwo->init(9, 0.2, 468, 330);
	texture = pRender->addTexture("assets/textures/Boss/bossAttack_2.png");
	attackTwo->getSprite()->setTexture(*texture);
	attackTwo->getSprite()->setOrigin({ 117.0f, 44.0f });
	attackTwo->getSprite()->setPosition(position_);

	attackThree = new Animation();
	attackThree->init(26, 0.5, 512, 342);
	texture = pRender->addTexture("assets/textures/Boss/bossAttack_3.png");
	attackThree->getSprite()->setTexture(*texture);
	attackThree->getSprite()->setOrigin({ 137.0f, 150.0f });
	attackThree->getSprite()->setPosition(position_);

	death = new Animation();
	death->init(20, 1, 512, 512);
	texture = pRender->addTexture("assets/textures/Boss/bossDying.png");
	death->getSprite()->setTexture(*texture);
	death->getSprite()->setOrigin({ 140.0f, 160.0f });
	death->getSprite()->setPosition(position_);

	Tentacles = new Animation();
	Tentacles->init(8, 0.5, 512, 252);
	texture = pRender->addTexture("assets/textures/Boss/tentaclesprite.png");
	Tentacles->getSprite()->setTexture(*texture);
	Tentacles->getSprite()->setOrigin({ 112.0f, 20.0f });
	Tentacles->getSprite()->setPosition(sf::Vector2f(position_.x- 80, position_.y + 120));

	texture = pRender->addTexture("assets/textures/Boss/eyes.png");
	Eyes.setTexture(*texture);
	Eyes.setOrigin({ 58.0f, 6.0f });
	Eyes.setPosition(position_);

	IdleSprite = new Animation();
	IdleSprite->init(1, 0, 465, 252);
	texture = pRender->addTexture("assets/textures/Boss/bossIdle.png");
	IdleSprite->getSprite()->setTexture(*texture);
	IdleSprite->getSprite()->setOrigin({ 117.0f, 44.0f });

	enemySprite_ = IdleSprite;
	enemySprite_->getSprite()->setPosition(position_);
	enemySprite_->getSprite()->setOrigin({ 117.0f, 44.0f });
}
bool Khan::Ready()
{
	if (position_.x < WIDTH + 600 && position_.x > WIDTH + 588)
		return true;
	else
		return false;
}