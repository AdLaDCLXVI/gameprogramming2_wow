#include "CheckPoint.h"

CheckPoint::CheckPoint()
{
	velocity_ = { -9.0f, 0.0f };
	alive_ = false;
	level_ = -1;
}

void CheckPoint::init(const sf::Vector2f& pos, int level)
{
	position_ = pos;
	level_ = level;
}

void CheckPoint::update(float deltaTime)
{
	position_ += velocity_ * deltaTime;

	if (position_.x < 0)
		alive_ = false;
}

void CheckPoint::activate()
{
	alive_ = true;
}

bool CheckPoint::isAlive()
{
	return alive_;
}

void CheckPoint::kill()
{
	alive_ = false;
}

int CheckPoint::getLevel()
{
	return level_;
}

const sf::Vector2f & CheckPoint::getPosition()
{
	return position_;
}
